#include <core.h>
#include <gui.h>
#include <log.h>

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ma::Core::init();
    ma::Gui::init();

    return a.exec();
}
