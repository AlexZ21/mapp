#ifndef TESTPLUGIN_GLOBAL_H
#define TESTPLUGIN_GLOBAL_H

#if defined(_MSC_VER)
#  define DECL_EXPORT     __declspec(dllexport)
#  define DECL_IMPORT     __declspec(dllimport)
#elif defined(__GNUC__)
#  if  defined(__WIN32__)
#    define DECL_EXPORT     __declspec(dllexport)
#    define DECL_IMPORT     __declspec(dllimport)
#  elif defined(__linux__)
#    define DECL_EXPORT     __attribute__((visibility("default")))
#    define DECL_IMPORT     __attribute__((visibility("default")))
#    define DECL_HIDDEN     __attribute__((visibility("hidden")))
#  endif
#endif

#if defined(TESTPLUGIN_LIBRARY)
#  define TESTPLUGINSHARED_EXPORT DECL_EXPORT
#else
#  define TESTPLUGINSHARED_EXPORT DECL_IMPORT
#endif

#define EXTERN_TESTPLUGIN extern "C"

#endif // TESTPLUGIN_GLOBAL_H
