#ifndef TESTPLUGIN_H
#define TESTPLUGIN_H

#include "testplugin_global.h"

#include <plugin.h>

class TestPlugin : public ma::Plugin
{
public:
    TestPlugin();
    ~TestPlugin();

    void init();
};

MA_PLUGIN(TestPlugin, "TestPlugin", "Description for TestPlugin", "1.0")

#endif // TESTPLUGIN_H
