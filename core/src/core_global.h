#ifndef CORE_GLOBAL_H
#define CORE_GLOBAL_H

#if defined(_MSC_VER)
#  define DECL_EXPORT     __declspec(dllexport)
#  define DECL_IMPORT     __declspec(dllimport)
#elif defined(__GNUC__)
#  if  defined(__WIN32__)
#    define DECL_EXPORT     __declspec(dllexport)
#    define DECL_IMPORT     __declspec(dllimport)
#  elif defined(__linux__)
#    define DECL_EXPORT     __attribute__((visibility("default")))
#    define DECL_IMPORT     __attribute__((visibility("default")))
#    define DECL_HIDDEN     __attribute__((visibility("hidden")))
#  endif
#endif

#define MACORE_EXTERN extern "C"

#if defined(MACORE_LIBRARY)
#  define MACORESHARED_EXPORT DECL_EXPORT
#else
#  define MACORESHARED_EXPORT DECL_IMPORT
#endif

#define NS_MA_BEGIN namespace ma {
#define NS_MA_END }
#define USING_MA_NS using namespace ma;

#endif // CORE_GLOBAL_H
