#ifndef CORE_UTILS_H
#define CORE_UTILS_H

#include "core_global.h"
#include "log.h"

#define MA_ASSERT(_expr_, _err_text_) \
    if (!_expr_) { \
        LERROR() << _err_text_; \
        exit(1); \
    }

NS_MA_BEGIN

struct Vector3f {
    float x;
    float y;
    float z;
};

NS_MA_END

#endif // CORE_UTILS_H
