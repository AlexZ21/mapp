#include "core.h"
#include "core_utils.h"
#include "plugin.h"

#include "utils/fs.h"
#include "utils/library.h"

#include "audio/audiodevice.h"
#include "audio/audioplayer.h"

NS_MA_BEGIN

Core &Core::instance()
{
    static Core core;
    return core;
}

void Core::init()
{
    instance()._init();
}

Core::Core() :
    m_audioPlayer(nullptr)
{
    AudioDevice::init();
    m_audioPlayer = new AudioPlayer();
}

Core::~Core()
{
    LDEBUG() << "Delete core";
    delete m_audioPlayer;
    m_plugins.erase(m_plugins.begin(), m_plugins.end());
}

void Core::_init()
{
    LINFO() << "Init core";

    // Load plugins
    loadPlugins();

    // Init plugins
    initPlugins();
}

void Core::loadPlugins()
{
    std::string pluginsPath = fs::currentPath()+ "/plugins";
    LINFO() << "Plugins directory: " << pluginsPath;

#if defined(SYSTEM_WIN)
    std::vector<std::string> filters = {"*.dll"};
#else
    std::vector<std::string> filters = {".*\\.so"};
#endif

    std::vector<std::string> pluginsList = fs::filesList(pluginsPath, filters);

    for (const std::string &pluginFileName : pluginsList) {
        typedef Plugin *(*LoadPluginType)();
        typedef const char *(*TextInfoType)();

        Library *lib = new Library();
        if (!lib->load(pluginsPath + "/" + pluginFileName)) {
            LERROR() << "Can't load plugin <" << pluginFileName << ">";
            delete lib;
            continue;
        }

        LoadPluginType LoadPlugin = (LoadPluginType) lib->resolve("LoadPlugin");
        TextInfoType PluginName = (TextInfoType) lib->resolve("PluginName");
        TextInfoType PluginDescription = (TextInfoType) lib->resolve("PluginDescription");
        TextInfoType PluginVersion = (TextInfoType) lib->resolve("PluginVersion");

        if (LoadPlugin) {
            Plugin *plugin = LoadPlugin();
            if (plugin) {
                LINFO() << "Loading plugin: " << PluginName() << " v."
                        << PluginVersion()
                        << " (" << PluginDescription() << ")";
                m_plugins.emplace_back(plugin);
                m_pluginLibs.emplace_back(lib);
                continue;
            }
        }

        delete lib;
    }
}

void Core::initPlugins()
{
    for (const std::shared_ptr<Plugin> &plugin : m_plugins)
        plugin->init();
}

AudioPlayer *Core::audioPlayer() const
{
    return m_audioPlayer;
}

NS_MA_END
