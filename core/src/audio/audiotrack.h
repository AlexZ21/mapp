#ifndef AUDIOTRACK_H
#define AUDIOTRACK_H

#include "../core_global.h"
#include "../sigslot.h"

#include <string>
#include <chrono>

NS_MA_BEGIN

class MACORESHARED_EXPORT AudioTrack
{
public:
    AudioTrack();

    std::string source() const;
    void setSource(const std::string &source);

    std::string loader() const;
    void setLoader(const std::string &loader);

    std::string title() const;
    void setTitle(const std::string &title);

    std::string artist() const;
    void setArtist(const std::string &artist);

    std::string album() const;
    void setAlbum(const std::string &album);

    std::chrono::milliseconds duration() const;
    void setDuration(const std::chrono::milliseconds &duration);

    Signal<> metadataChanged;

private:
    std::string m_source; ///< Audio data url
    std::string m_loader; ///< Audio loader name

    std::string m_title;
    std::string m_artist;
    std::string m_album;
    std::chrono::milliseconds m_duration;

};

NS_MA_END

#endif // AUDIOTRACK_H
