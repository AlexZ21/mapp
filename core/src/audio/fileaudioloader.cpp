#include "fileaudioloader.h"
#include "../log.h"

#include <fstream>
#include <string.h>

NS_MA_BEGIN

FileAudioLoader::FileAudioLoader(const std::shared_ptr<AudioTrack> &audioTrack) :
    AudioLoader(audioTrack)
{
    type = FixedLength;
}

FileAudioLoader::~FileAudioLoader()
{

}

bool FileAudioLoader::load()
{
    std::ifstream ifs(audioTrack->source());

    if (!ifs.eof() && !ifs.fail()) {
        ifs.seekg(0, std::ios_base::end);
        std::streampos fileSize = ifs.tellg();
        m_buffer.resize(fileSize);
        ifs.seekg(0, std::ios_base::beg);
        ifs.read(&m_buffer[0], fileSize);
    }

    m_maxSize = m_buffer.size();
    maxSizeChanged(m_maxSize);
    sizeChanged(m_buffer.size());

    std::string title = audioTrack->source();
    int pos = title.find_last_of('/');
    if (pos != std::string::npos)
        title = title.substr(pos + 1);
    pos = title.find_last_of('.');
    if (pos != std::string::npos)
        title = title.substr(0, pos);
    audioTrack->setTitle(title);

    return true;
}

bool FileAudioLoader::stop()
{
    return true;
}

int64_t FileAudioLoader::read(uint8_t *data, int size, int pos)
{
    const char *data_ = m_buffer.data();
    int size_ = size;
    if (size + pos > m_buffer.size())
        size_ = m_buffer.size() - pos;
    memcpy(data, &data_[pos], size_);
    return size_;
}

int64_t FileAudioLoader::seek(int64_t pos, int64_t offset, int whence)
{
    int64_t newPos = -1;
    switch (whence) {
    case MA_AL_SEEK_SET: return offset; break;
    case MA_AL_SEEK_CUR: return pos + offset; break;
    case MA_AL_SEEK_END: return m_maxSize + offset; break;
    }
    return newPos;
}

int64_t FileAudioLoader::size()
{
    return m_buffer.size();
}

int64_t FileAudioLoader::maxSize()
{
    return m_maxSize;
}

int FileAudioLoader::seekType()
{
    return MA_AL_ANY;
}

NS_MA_END
