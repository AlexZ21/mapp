#include "httpaudioloader.h"
#include "../log.h"

#include <string.h>

#include <mutex>
#include <openssl/err.h>
#include <vector>

NS_MA_BEGIN

HttpAudioLoader::HttpAudioLoader(const std::shared_ptr<AudioTrack> &audioTrack) :
    AudioLoader(audioTrack),
    m_maxSize(0),
    m_loading(false),
    m_stopped(false),
    m_icyMetaint(0),
    m_currentIcyMetadataSize(0)
{
}

HttpAudioLoader::~HttpAudioLoader()
{
    LDEBUG() << "Destroing HttpAudioLoader";
    if (!m_stopped) {
        LDEBUG() << "Serega";
        m_http.stop();
        m_loading = false;
        endBuffering();
        m_readingCv.notify_one();
    }
}

bool HttpAudioLoader::load()
{
    // Detect buffer size
    m_http.setFollowLocation(true);
    m_http.setStoreBody(false);
    m_http.setConnectionTimeout(std::chrono::milliseconds(1000));

    Http::Request req(Url(audioTrack->source())/*, Http::Request::Head*/);

    m_http.setOnHeaderReady([this](const Http::Response &response){
        m_maxSize = std::atoll(response.field("content-length").c_str());
        type = m_maxSize == 0 ? Stream : FixedLength;
        m_maxSize = m_maxSize == 0 ? 204800 : m_maxSize;
        maxSizeChanged(m_maxSize);
        m_headerReady = true;
        m_readingCv.notify_one();
    });

    m_http.setOnError([this](){
        m_loading = false;
        m_readingCv.notify_one();
    });

    m_http.sendRequestAsync(req, std::chrono::milliseconds(1000));

    {
        std::unique_lock<std::mutex> readingLock(m_readingMx);
        m_readingCv.wait(readingLock);
    }

    m_http.stop();

    if (!m_headerReady)
        return false;

    // Prepare callbacks
    if (type == FixedLength) {
        m_http.setOnHeaderReady(nullptr);
        m_http.setOnReady(nullptr);
        m_http.setOnBodyDataRecieved([this](const char *data, size_t size){
            if (size > 0) {
                m_bufferMx.lock();
                int bufferSize = m_buffer.size();
                m_buffer.resize(m_buffer.size() + size);
                memcpy(&m_buffer.data()[bufferSize], data, size);
                m_chunk.current += size;
                m_bufferMx.unlock();
                if (m_chunk.current >= m_chunk.end - 1)
                    endBuffering();
            }
            m_readingCv.notify_one();
        });
        m_http.setOnError([this](){
            m_bufferMx.lock();
            m_chunk = {0, 0, 0};
            m_loading = false;
            m_bufferMx.unlock();
        });
    }

    return true;
}

bool HttpAudioLoader::stop()
{
    m_stopped = true;
    m_http.stop();
    m_loading = false;
    endBuffering();
    m_readingCv.notify_one();
    return true;
}

int64_t HttpAudioLoader::read(uint8_t *data, int size, int pos)
{
    if (m_stopped)
        return 0;

    int size_ = size;

    if (type == Stream) {
        if (!m_loading)
            loadStream();

        if (m_buffer.size() == 0) {
            startBuffering();
            std::unique_lock<std::mutex> readingLock(m_readingMx);
            m_readingCv.wait(readingLock,
                             [this, size](){
                if (!m_loading)
                    return true;
                return m_buffer.size() != 0;
            });
            endBuffering();
        }

        if (m_stopped) {
            memset(data, 0, 0);
            return size;
        }

        m_bufferMx.lock();

        if (size > m_buffer.size())
            size_ = m_buffer.size();

        if (size_ > 0) {
            const char *data_ = m_buffer.data();
            memcpy(data, &data_[0], size_);
            m_buffer.erase(m_buffer.begin(), m_buffer.begin() + size_);
        }

        m_bufferMx.unlock();

    } else {
        if (pos < m_maxSize) {
            if (!canReadFromChunk(pos, size)) {
                m_http.stop();
                m_buffer.clear();
                int to = pos + size * 200;
                to = to > m_maxSize ? m_maxSize : to;
                loadChunk(pos, to);
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }

            int waitTo = pos + size_;
            if (waitTo > m_maxSize)
                waitTo = m_maxSize;

            if (m_chunk.current < waitTo) {
                std::unique_lock<std::mutex> readingLock(m_readingMx);
                m_readingCv.wait_for(readingLock,
                                     std::chrono::milliseconds(2000),
                                     [this, waitTo](){
                    if (!m_loading)
                        return true;
                    return m_chunk.current >= waitTo;
                });
            }
        }

        m_bufferMx.lock();

        if (size_ + (pos - m_chunk.start) > m_buffer.size())
            size_ = m_buffer.size() - (pos - m_chunk.start);

        if (size_ > 0) {
            const char *data_ = m_buffer.data();
            memcpy(data, &data_[pos - m_chunk.start], size_);
        }

        m_bufferMx.unlock();
    }

    return size_;
}

int64_t HttpAudioLoader::seek(int64_t pos, int64_t offset, int whence)
{
    int64_t newPos = -1;
    switch (whence) {
    case MA_AL_SEEK_SET: newPos = offset; break;
    case MA_AL_SEEK_CUR: newPos = pos + offset; break;
    case MA_AL_SEEK_END: newPos = m_maxSize + offset; break;
    }

    if (type == Stream)
        return -1;

    return newPos;
}

int64_t HttpAudioLoader::size()
{
    return m_buffer.size();
}

int64_t HttpAudioLoader::maxSize()
{
    return m_maxSize;
}

int HttpAudioLoader::seekType()
{
    return MA_AL_BYTE;
}

void HttpAudioLoader::loadStream()
{
    m_http.setOnReady(nullptr);

    // icyMetaint
    m_http.setOnHeaderReady([this](const Http::Response &response){
        if (!response.field("icy-metaint").empty())
            m_icyMetaint = std::stoul(response.field("icy-metaint").c_str());
    });

    m_http.setOnBodyDataRecieved([this](const char *data, size_t size){
        if (size > 0) {
            m_bufferMx.lock();

            if (m_icyMetaint > 0) {
                m_tmpBuffer.resize(m_tmpBuffer.size() + size);
                memcpy(&m_tmpBuffer.data()[m_tmpBuffer.size() - size], data, size);

                if (m_currentIcyMetadataSize != 0) {
                    if (m_tmpBuffer.size() > m_currentIcyMetadataSize) {
                        m_currentIcyMetadata.clear();
                        m_currentIcyMetadata.append(m_tmpBuffer.data(), m_currentIcyMetadataSize);
                        m_tmpBuffer.erase(m_tmpBuffer.begin(),
                                          m_tmpBuffer.begin() + m_currentIcyMetadataSize);
                        m_currentIcyMetadataSize = 0;

                        int pos = m_currentIcyMetadata.find('\'');
                        if (pos != std::string::npos)
                            m_currentIcyMetadata.erase(0, pos + 1);
                        pos = m_currentIcyMetadata.find('\'');
                        if (pos != std::string::npos)
                            m_currentIcyMetadata.erase(pos);

                        pos = m_currentIcyMetadata.find(" - ");
                        if (pos != std::string::npos) {
                            audioTrack->setTitle(m_currentIcyMetadata.substr(pos + 3));
                            audioTrack->setArtist(m_currentIcyMetadata.substr(0, pos));
                        } else {
                            audioTrack->setTitle(m_currentIcyMetadata);
                            audioTrack->setArtist(std::string());
                        }

                    }
                } else {
                    if (m_tmpBuffer.size() > m_icyMetaint + 1) {
                        writeStreamDataToBuffer(m_tmpBuffer.data(), m_icyMetaint);
                        m_tmpBuffer.erase(m_tmpBuffer.begin(), m_tmpBuffer.begin() + m_icyMetaint);

                        // Get metadata size
                        m_currentIcyMetadataSize = m_tmpBuffer.front() * 16;
                        m_tmpBuffer.erase(m_tmpBuffer.begin(), m_tmpBuffer.begin() + 1);
                    }
                }

            } else {
                writeStreamDataToBuffer(data, size);
            }

            m_bufferMx.unlock();
        }
        m_readingCv.notify_one();
    });

    m_http.setOnError([this](){
        m_loading = false;
        m_readingCv.notify_one();
    });

    m_icyMetaint = 0;

    Http::Request request(audioTrack->source());
    request.setField("Icy-MetaData", "1");
    m_http.sendRequestAsync(request, std::chrono::milliseconds(1000));

    m_loading = true;
    startBuffering();
}

void HttpAudioLoader::writeStreamDataToBuffer(const char *data, size_t size)
{
    if (m_buffer.size() > m_maxSize)
        m_buffer.clear();

    m_buffer.resize(m_buffer.size() + size);
    memcpy(&m_buffer.data()[m_buffer.size() - size], data, size);
}

void HttpAudioLoader::loadChunk(int64_t from, int64_t to)
{
    m_chunk.start = from;
    m_chunk.current = from;
    m_chunk.end = to;

    Http::Request request(audioTrack->source());
    request.setField("Range", "bytes=" + std::to_string(from) + "-" + std::to_string(to));
    m_http.sendRequestAsync(request, std::chrono::milliseconds(1000));

    m_loading = true;
    startBuffering();
}

bool HttpAudioLoader::canReadFromChunk(int64_t pos, int64_t size)
{
    return (pos >= m_chunk.start && pos <= m_chunk.end) &&
            ((pos + size) >= m_chunk.start && (pos + size) <= m_chunk.end);
}

NS_MA_END
