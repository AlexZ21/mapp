#include "audiosource.h"

NS_MA_BEGIN

AudioSource::AudioSource(const AudioSource &copy)
{
    alCheck(alGenSources(1, &m_source));
    alCheck(alSourcei(m_source, AL_BUFFER, 0));

    setPitch(copy.pitch());
    setVolume(copy.volume());
    setPosition(copy.position());
    setRelativeToListener(copy.isRelativeToListener());
    setMinDistance(copy.minDistance());
    setAttenuation(copy.attenuation());
}

AudioSource::~AudioSource()
{
    alCheck(alSourcei(m_source, AL_BUFFER, 0));
    alCheck(alDeleteSources(1, &m_source));
}

void AudioSource::setPitch(float pitch)
{
    alCheck(alSourcef(m_source, AL_PITCH, pitch));
}

float AudioSource::pitch() const
{
    ALfloat pitch;
    alCheck(alGetSourcef(m_source, AL_PITCH, &pitch));
    return pitch;
}

void AudioSource::setVolume(float volume)
{
    alCheck(alSourcef(m_source, AL_GAIN, volume * 0.01f));
}

float AudioSource::volume() const
{
    ALfloat gain;
    alCheck(alGetSourcef(m_source, AL_GAIN, &gain));
    return gain * 100.f;
}

void AudioSource::setPosition(float x, float y, float z)
{
    alCheck(alSource3f(m_source, AL_POSITION, x, y, z));
}

void AudioSource::setPosition(const Vector3f &position)
{
    setPosition(position.x, position.y, position.z);
}

Vector3f AudioSource::position() const
{
    Vector3f position;
    alCheck(alGetSource3f(m_source, AL_POSITION, &position.x, &position.y, &position.z));
    return position;
}

void AudioSource::setRelativeToListener(bool relative)
{
    alCheck(alSourcei(m_source, AL_SOURCE_RELATIVE, relative));
}

bool AudioSource::isRelativeToListener() const
{
    ALint relative;
    alCheck(alGetSourcei(m_source, AL_SOURCE_RELATIVE, &relative));
    return relative != 0;
}

void AudioSource::setMinDistance(float distance)
{
    alCheck(alSourcef(m_source, AL_REFERENCE_DISTANCE, distance));
}

float AudioSource::minDistance() const
{
    ALfloat distance;
    alCheck(alGetSourcef(m_source, AL_REFERENCE_DISTANCE, &distance));
    return distance;
}

void AudioSource::setAttenuation(float attenuation)
{
    alCheck(alSourcef(m_source, AL_ROLLOFF_FACTOR, attenuation));
}

float AudioSource::attenuation() const
{
    ALfloat attenuation;
    alCheck(alGetSourcef(m_source, AL_ROLLOFF_FACTOR, &attenuation));
    return attenuation;
}

AudioSource &AudioSource::operator =(const AudioSource &right)
{
    setPitch(right.pitch());
    setVolume(right.volume());
    setPosition(right.position());
    setRelativeToListener(right.isRelativeToListener());
    setMinDistance(right.minDistance());
    setAttenuation(right.attenuation());
    return *this;
}

AudioSource::AudioSource()
{
    alCheck(alGenSources(1, &m_source));
    alCheck(alSourcei(m_source, AL_BUFFER, 0));
}

AudioSource::Status AudioSource::status() const
{
    ALint status;
    alCheck(alGetSourcei(m_source, AL_SOURCE_STATE, &status));
    switch (status) {
        case AL_INITIAL:
        case AL_STOPPED: return Stopped;
        case AL_PAUSED:  return Paused;
        case AL_PLAYING: return Playing;
    }
    return Stopped;
}

NS_MA_END
