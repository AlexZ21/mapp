#ifndef AUDIOLOADER_H
#define AUDIOLOADER_H

#include "../core_global.h"
#include "../sigslot.h"
#include "audiotrack.h"

#include <memory>

//! Defines for ffmpeg
#define MA_AL_SEEK_SET 0
#define MA_AL_SEEK_CUR 1
#define MA_AL_SEEK_END 2
#define MA_AL_AVSEEK_SIZE 0x10000
#define MA_AL_ANY 4
#define MA_AL_BYTE 2

NS_MA_BEGIN

class MACORESHARED_EXPORT AudioLoader
{
public:
    enum Type {
        Stream,
        FixedLength
    };

    AudioLoader(const std::shared_ptr<AudioTrack> &audioTrack);
    virtual ~AudioLoader();

    //! Start loading audio data
    virtual bool load() = 0;
    //! Stop loading audio data
    virtual bool stop() = 0;
    //! Read audio data from buffer
    virtual int64_t read(uint8_t *data, int size, int pos) = 0;
    //! Seek in buffer
    virtual int64_t seek(int64_t pos, int64_t offset, int whence) = 0;
    //! Current buffer size
    virtual int64_t size() = 0;
    //! Max buffer size
    virtual int64_t maxSize() = 0;
    //! Seek type for ffmpeg
    virtual int seekType() = 0;

    //! Loading audio data type:
    //! Stream - if data not have fixed size
    //! FixedLength - data has fixed size
    Type getType() const;

    //! Emit when buffer size changed
    Signal<int64_t> sizeChanged;
    //! Emit when max buffer size changed
    Signal<int64_t> maxSizeChanged;
    //! Emit when start loading audio data
    Signal<> startBuffering;
    //! Emit when stop loading audio data
    Signal<> endBuffering;

protected:
    std::shared_ptr<AudioTrack> audioTrack; ///< Audio track
    Type type; ///< Loading type

};

NS_MA_END

#endif // AUDIOLOADER_H
