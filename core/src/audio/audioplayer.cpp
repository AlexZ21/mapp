#include "audioplayer.h"
#include "audioplaylist.h"
#include "audiotrack.h"
#include "ffmpegaudiostream.h"
#include "audioloader.h"
#include "audioloaderfactory.h"
#include "../core.h"
#include "../core_utils.h"
#include "../log.h"

#include <string.h>
#include <functional>
#include <algorithm>

NS_MA_BEGIN

AudioPlayer::AudioPlayer() :
    m_audioStream(nullptr),
    m_currentPlaylist(nullptr),
    m_currentTrackIndex(-1),
    m_currentAudioTrack(nullptr),
    m_status(Stopped),
    m_newTrack(false),
    m_repeat(false),
    m_needRepeat(false),
    m_shuffle(false),
    m_running(true)
{
    srand(time(nullptr));
    m_thread = std::thread(&AudioPlayer::threadFunc, this);
}

AudioPlayer::~AudioPlayer()
{
    // Stop audio loader
    if (m_audioLoader)
        m_audioLoader->stop();

    m_running.store(false);
    m_cv.notify_one();
    if (m_thread.joinable())
        m_thread.join();
}

std::shared_ptr<AudioPlaylist> AudioPlayer::playlist() const
{
    return m_currentPlaylist;
}

void AudioPlayer::setpPlaylist(AudioPlaylist *playlist)
{
    m_currentPlaylist.reset(playlist);
    m_currentPlaylist->changed.connect([this](){
        m_playlistIndexes.push_back(m_playlistIndexes.size());
        if (m_shuffle)
            std::random_shuffle(m_playlistIndexes.begin(), m_playlistIndexes.end());
    });
    m_playlistIndexes.clear();
    for (int i = 0; i < m_currentPlaylist->size(); ++i)
        m_playlistIndexes.push_back(i);
    if (m_shuffle)
        std::random_shuffle(m_playlistIndexes.begin(), m_playlistIndexes.end());
    m_currentTrackIndex = 0;
    m_newTrack = true;
}

void AudioPlayer::play(AudioTrack *audioTrack)
{
    m_currentPlaylist.reset();
    m_currentAudioTrack.reset(audioTrack);
    m_newTrack = true;
    play();
}

void AudioPlayer::play()
{    
    if (m_newTrack) {
        // If audio loader is active
        // Audio loader can block audio stream by condition variables and mutexes
        // when audio loader read data
        if (m_audioLoader)
            m_audioLoader->stop();
        m_cv.notify_one();
    } else {
        if (m_audioStream) {
            // If end of stream playing from start
            if (m_audioStream->endOfStream())
                m_audioStream->setPlayingOffset(std::chrono::milliseconds(0));
            m_audioStream->play();
        }
    }
}

void AudioPlayer::pause()
{
    if (m_audioStream)
        m_audioStream->pause();
}

void AudioPlayer::stop()
{
    if (m_audioStream)
        m_audioStream->stop();
}

void AudioPlayer::seek(const std::chrono::milliseconds &duration)
{
    if (m_audioStream)
        m_audioStream->setPlayingOffset(duration);
}

void AudioPlayer::prev()
{
    if (!m_currentPlaylist)
        return;

    --m_currentTrackIndex;
    if (m_currentTrackIndex < 0) {
        m_currentTrackIndex = m_playlistIndexes.size() - 1;
    }

    //    m_currentAudioTrack = m_currentPlaylist->audioTrack(m_playlistIndexes[m_currentTrackIndex]);
    m_newTrack = true;
    play();
}

void AudioPlayer::next()
{
    if (!m_currentPlaylist)
        return;

    ++m_currentTrackIndex;
    if (m_currentTrackIndex == m_playlistIndexes.size()) {
        m_currentTrackIndex = 0;
    }

    //    m_currentAudioTrack = m_currentPlaylist->audioTrack(m_playlistIndexes[m_currentTrackIndex]);
    m_newTrack = true;
    play();
}

AudioPlayer::Status AudioPlayer::status() const
{
    return m_status;
}

bool AudioPlayer::isRepeat() const
{
    return m_repeat;
}

void AudioPlayer::setRepeat(bool repeat)
{
    m_repeat = repeat;
    if (m_audioStream)
        m_audioStream->setLoop(m_repeat);
}

bool AudioPlayer::isShuffle() const
{
    return m_shuffle;
}

void AudioPlayer::setShuffle(bool shuffle)
{
    m_shuffle = shuffle;
    if (m_shuffle)
        std::random_shuffle(m_playlistIndexes.begin(), m_playlistIndexes.end());
    else
        std::sort(m_playlistIndexes.begin(), m_playlistIndexes.end());
}

std::chrono::milliseconds AudioPlayer::playingOffset() const
{
    if (m_audioStream)
        return m_audioStream->playingOffset();
    return std::chrono::milliseconds(0);
}

void AudioPlayer::setStatus(AudioPlayer::Status status)
{
    m_status = status;
    statusChanged(m_status);
}

void AudioPlayer::threadFunc()
{
    while(m_running.load()) {
        // If not new track
        if (!m_newTrack) {
            std::unique_lock<std::mutex> lock(m_threadmx);
            m_cv.wait(lock, [this](){
                // If tread stopped return
                if (!m_running.load())
                    return true;
                // If setted new track return
                if (m_newTrack)
                    return true;
                return false;
            });
            // If thread stopped return from thread
            if (!m_running.load())
                return;
        }

        m_newTrack = false;

        if (m_needRepeat && m_audioStream && m_audioStream->loop() && m_audioStream->endOfStream()) {
            m_audioStream->setPlayingOffset(std::chrono::milliseconds(0));
            m_audioStream->play();
            m_needRepeat = false;
            continue;
        }

        // If set playlist
        if (m_currentPlaylist)
            m_currentAudioTrack = m_currentPlaylist->audioTrack(m_playlistIndexes[m_currentTrackIndex]);

        // Destroy current audio stream and set new
        if (m_audioStream) {
            m_audioStream->stop();
            m_audioStream.reset();
        }

        if (m_audioLoader)
            m_audioLoader.reset();

        startBuffering();

        // Creating audioloader
        m_audioLoader = std::shared_ptr<AudioLoader>(AudioLoaderFactory::create(m_currentAudioTrack));
        if (!m_audioLoader) {
            LERROR() << "Can't find audio loader: " << m_currentAudioTrack->loader();
            endBuffering();
            continue;
        }

        m_audioLoader->startBuffering.connect([this](){
            startBuffering();
        });

        m_audioLoader->endBuffering.connect([this](){
            endBuffering();
        });

        m_currentAudioTrack->metadataChanged.connect([this](){
            metadataChanged(*m_currentAudioTrack.get());
        });

        // Load audio data
        if (!m_audioLoader->load()) {
            m_audioLoader.reset();
            LERROR() << "Can't load audio data";
            endBuffering();
            continue;
        }

        // Create ffmpeg decoder
        m_audioStream = std::shared_ptr<FFMpegAudioStream>(new FFMpegAudioStream(m_audioLoader.get()));

        m_audioStream->statusChanged.connect([this](int status){
            setStatus((Status) status);
            if (!m_audioStream)
                return;
            // If repeat
            if (m_audioStream->status() == AudioStream::Stopped &&
                    m_audioStream->loop() && m_audioStream->endOfStream()) {
                m_newTrack = true;
                m_needRepeat = true;
                m_cv.notify_one();
            }
            // If set playlist
            if (m_audioStream->status() == AudioStream::Stopped &&
                    m_audioStream->endOfStream() && m_currentPlaylist) {
                next();
            }
        });

        m_audioStream->
                metadataChanged.connect([this](const std::string &key, const std::string &value){
            if (key == "duration")
                m_currentAudioTrack->setDuration(std::chrono::milliseconds(std::stoll(value.c_str())));
            if (key == "title")
                m_currentAudioTrack->setTitle(value);
            if (key == "artist")
                m_currentAudioTrack->setArtist(value);
        });

        // If repeat
        m_audioStream->setLoop(m_repeat);

        if (!m_audioStream->init()) {
            m_audioStream.reset();
            m_audioLoader.reset();
            endBuffering();
            continue;
        }

        metadataChanged(*m_currentAudioTrack.get());

        endBuffering();

        m_audioStream->play();
    }
}

NS_MA_END
