#ifndef FILEAUDIOLOADER_H
#define FILEAUDIOLOADER_H

#include "../core_global.h"
#include "audioloader.h"
#include "audiotrack.h"

#include <vector>

NS_MA_BEGIN

class MACORESHARED_EXPORT FileAudioLoader : public AudioLoader
{
public:
    FileAudioLoader(const std::shared_ptr<AudioTrack> &audioTrack);
    ~FileAudioLoader();

    bool load();
    bool stop();
    int64_t read(uint8_t *data, int size, int pos);
    int64_t seek(int64_t pos, int64_t offset, int whence);
    int64_t size();
    int64_t maxSize();
    int seekType();

private:
    std::vector<char> m_buffer;
    int64_t m_maxSize;

};

NS_MA_END

#endif // FILEAUDIOLOADER_H
