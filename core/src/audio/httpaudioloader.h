#ifndef HTTPAUDIOLOADER_H
#define HTTPAUDIOLOADER_H

#include "../core_global.h"
#include "audioloader.h"
#include "audiotrack.h"
#include "../network/http.h"

#include <vector>
#include <mutex>
#include <condition_variable>

NS_MA_BEGIN

class HttpAudioLoader : public AudioLoader
{
public:
    //! Loading chunk
    struct Chunk {
        int64_t start = 0;
        int64_t current = 0;
        int64_t end = 0;
    };

    HttpAudioLoader(const std::shared_ptr<AudioTrack> &audioTrack);
    ~HttpAudioLoader();

    bool load();
    bool stop();
    int64_t read(uint8_t *data, int size, int pos);
    int64_t seek(int64_t pos, int64_t offset, int whence);
    int64_t size();
    int64_t maxSize();
    int seekType();

private:
    //! Load stream
    void loadStream();
    void writeStreamDataToBuffer(const char *data, size_t size);
    //! Load data chunk
    void loadChunk(int64_t from, int64_t to);
    //! Check data available by position
    bool canReadFromChunk(int64_t pos, int64_t size);

private:
    std::vector<char> m_buffer;
    std::mutex m_bufferMx;
    int64_t m_maxSize;

    bool m_headerReady; ///< Http header status
    bool m_loading; ///< Http status
    bool m_stopped;

    Http m_http; ///< Http client

    std::mutex m_readingMx;
    std::condition_variable m_readingCv;

    //! Stream
    uint32_t m_icyMetaint; ///< Bytes between metadata blocks
    std::string m_currentIcyMetadata;
    uint32_t m_currentIcyMetadataSize;
    std::vector<char> m_tmpBuffer;

    //! FixedLength
    Chunk m_chunk; ///< Current loading chunk
};

NS_MA_END

#endif // HTTPAUDIOLOADER_H
