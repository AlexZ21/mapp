#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include "../core_global.h"
#include "../sigslot.h"

#include <chrono>
#include <memory>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

NS_MA_BEGIN

class FFMpegAudioStream;
class AudioTrack;
class AudioPlaylist;
class AudioLoader;

class MACORESHARED_EXPORT AudioPlayer
{
public:
    enum Status {
        Stopped,
        Paused,
        Playing
    };

    AudioPlayer();
    ~AudioPlayer();

    std::shared_ptr<AudioPlaylist> playlist() const;
    void setpPlaylist(AudioPlaylist *playlist);

    void play(AudioTrack *audioTrack);
    void play();
    void pause();
    void stop();
    void seek(const std::chrono::milliseconds &duration);
    void prev();
    void next();

    Status status() const;

    bool isRepeat() const;
    void setRepeat(bool repeat);

    bool isShuffle() const;
    void setShuffle(bool shuffle);

    std::chrono::milliseconds playingOffset() const;

    Signal<Status> statusChanged;
    Signal<> startBuffering;
    Signal<> endBuffering;
    Signal<const AudioTrack &> metadataChanged;

private:
    void setStatus(Status status);
    void threadFunc();

private:
    std::shared_ptr<AudioLoader> m_audioLoader;
    std::mutex m_audioLoaderMX;
    std::shared_ptr<FFMpegAudioStream> m_audioStream;

    //! Playlist
    std::shared_ptr<AudioPlaylist> m_currentPlaylist;
    int m_currentTrackIndex;
    std::vector<int> m_playlistIndexes;

    //! Current track
    std::shared_ptr<AudioTrack> m_currentAudioTrack;
    bool m_newTrack;

    bool m_repeat;
    bool m_needRepeat;
    bool m_shuffle;

    Status m_status;

    std::thread m_thread;
    std::atomic_bool m_running;
    std::mutex m_threadmx;
    std::condition_variable m_cv;
    std::mutex m_initmx;

};

NS_MA_END

#endif // AUDIOPLAYER_H
