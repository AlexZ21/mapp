#include "audioloaderfactory.h"
#include "audioloader.h"
#include "fileaudioloader.h"
#include "httpaudioloader.h"
#include "../log.h"

NS_MA_BEGIN

std::shared_ptr<AudioLoader> AudioLoaderFactory::create(const std::shared_ptr<AudioTrack> &audioTrack)
{
    if (instance().m_factories.find(audioTrack->loader()) == instance().m_factories.end())
        return nullptr;
    return std::shared_ptr<AudioLoader>(instance().m_factories.at(audioTrack->loader())->create(audioTrack));
}

AudioLoaderFactory &AudioLoaderFactory::instance()
{
    static AudioLoaderFactory factory;
    return factory;
}

AudioLoaderFactory::AudioLoaderFactory()
{
    m_factories.emplace("local_file", new Factory<FileAudioLoader>());
    LINFO() << "Register audioloader <local_file>";

    m_factories.emplace("http_file", new Factory<HttpAudioLoader>());
    LINFO() << "Register audioloader <http_file>";
}

AudioLoaderFactory::~AudioLoaderFactory()
{
    while (m_factories.size() > 0) {
        delete m_factories.begin()->second;
        m_factories.erase(m_factories.begin());
    }
}

NS_MA_END
