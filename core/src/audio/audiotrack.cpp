#include "audiotrack.h"

NS_MA_BEGIN

AudioTrack::AudioTrack() :
    m_title("Unknow title"),
    m_artist("Unknow artist"),
    m_album("Unknow album"),
    m_duration(std::chrono::milliseconds(0))
{

}

std::string AudioTrack::source() const
{
    return m_source;
}

void AudioTrack::setSource(const std::string &source)
{
    m_source = source;
}

std::string AudioTrack::loader() const
{
    return m_loader;
}

void AudioTrack::setLoader(const std::string &loader)
{
    m_loader = loader;
}

std::string AudioTrack::title() const
{
    return m_title;
}

void AudioTrack::setTitle(const std::string &title)
{
    if (m_title != title) {
        m_title = title;
        if (m_title.empty())
            m_title = "Unknow title";
        metadataChanged();
    }
}

std::string AudioTrack::artist() const
{
    return m_artist;
}

void AudioTrack::setArtist(const std::string &artist)
{
    if (m_artist != artist) {
        m_artist = artist;
        if (m_artist.empty())
            m_artist = "Unknow artist";
        metadataChanged();
    }
}

std::string AudioTrack::album() const
{
    return m_album;
}

void AudioTrack::setAlbum(const std::string &album)
{
    if (m_album != album) {
        m_album = album;
        if (m_album.empty())
            m_album = "Unknow album";
        metadataChanged();
    }
}

std::chrono::milliseconds AudioTrack::duration() const
{
    return m_duration;
}

void AudioTrack::setDuration(const std::chrono::milliseconds &duration)
{
    if (m_duration != duration) {
        m_duration = duration;
        metadataChanged();
    }
}

NS_MA_END
