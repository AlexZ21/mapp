#ifndef FFMPEGAUDIOSTREAM_H
#define FFMPEGAUDIOSTREAM_H

#include "../core_global.h"
#include "../sigslot.h"
#include "audiostream.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>
}

#include <memory>
#include <vector>
#include <thread>
#include <condition_variable>

NS_MA_BEGIN

class AudioPlayer;
class AudioTrack;
class AudioLoader;

class FFMpegAudioStream : public AudioStream
{
    friend class AudioPlayer;
public:
    FFMpegAudioStream(AudioLoader *audioLoader);
    ~FFMpegAudioStream();

    bool init();

    bool endOfStream() const;
    Signal<int> statusChanged;
    Signal<const std::string &, const std::string &> metadataChanged;

protected:
    bool onData(Chunk &data);
    void onSeek(const std::chrono::milliseconds &timeOffset);
    void onStatus(Status status);
    virtual int64_t onLoop();

private:
    static int readBuffer(void* opaque, uint8_t* buf, int bufSize);
    static int64_t seekBuffer(void *opaque, int64_t offset, int whence);

    bool decodePacket(AVPacket *packet, std::vector<AVFrame *> &frames);
    void resampleFrame(AVFrame *frame, uint8_t *&outSamples, int &outNbSamples, int &outSamplesLength);

private:
    AudioLoader *m_audioLoader;

    //! ffmpeg
    int64_t m_pos;

    int64_t m_duration;

    unsigned char *m_avioContextBuffer;
    int m_avioContextBufferSize;
    AVIOContext *m_avioContext;

    AVFormatContext *m_audioContext;
    AVCodecContext *m_codecContext;
    AVCodec *m_codec;
    SwrContext *m_swrContext;

    int m_streamId;

    int m_outputSampleRate;
    int m_outputChanels;
    int16_t *m_outputSamplesBuffer;
    int m_outputSamples;
    int m_maxOutputSamples;
    uint8_t **m_outputData;

};

NS_MA_END

#endif // FFMPEGAUDIOSTREAM_H
