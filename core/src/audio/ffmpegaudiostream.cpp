#include "ffmpegaudiostream.h"
#include "audioloaderfactory.h"
#include "audioloader.h"
#include "../log.h"

NS_MA_BEGIN

FFMpegAudioStream::FFMpegAudioStream(AudioLoader *audioLoader) :
    AudioStream(),
    m_audioLoader(audioLoader),
    m_pos(0),
    m_duration(0),
    m_avioContextBuffer(nullptr),
    m_avioContextBufferSize(1024),
    m_avioContext(nullptr),
    m_audioContext(nullptr),
    m_codecContext(nullptr),
    m_codec(nullptr),
    m_swrContext(nullptr),
    m_streamId(-1),
    m_outputSampleRate(44100),
    m_outputChanels(2),
    m_outputSamplesBuffer(nullptr),
    m_outputSamples(1024),
    m_maxOutputSamples(1024),
    m_outputData(nullptr)
{
    av_register_all();
}

FFMpegAudioStream::~FFMpegAudioStream()
{    
    LDEBUG() << "Destroy FFMpegAudioStream";

//    m_audioLoader = nullptr;

    if (status() == FFMpegAudioStream::Playing || status() == FFMpegAudioStream::Paused)
        stop();

    if (m_audioContext) {
        avformat_close_input(&m_audioContext);
        m_audioContext = nullptr;
    }

    if (m_swrContext)
        swr_close(m_swrContext);

    if (m_outputSamplesBuffer)
        av_free(m_outputSamplesBuffer);

    if (m_outputData)
        av_freep(m_outputData);

}

bool FFMpegAudioStream::init()
{
    if (!m_audioLoader)
        return false;

    m_avioContextBuffer = (unsigned char*)av_malloc(m_avioContextBufferSize);
    m_avioContext = avio_alloc_context(m_avioContextBuffer,
                                       m_avioContextBufferSize,
                                       0,
                                       (void *)this,
                                       &FFMpegAudioStream::readBuffer,
                                       0,
                                       &FFMpegAudioStream::seekBuffer);


    m_audioContext = avformat_alloc_context();
    m_audioContext->pb = m_avioContext;

    if (avformat_open_input(&m_audioContext, 0, 0, 0) < 0) {
        LERROR("FFMpegAudioStream") << "Could not open input";
        m_audioContext = nullptr;
        return false;
    }

    if (avformat_find_stream_info(m_audioContext, nullptr) < 0) {
        LERROR("FFMpegAudioStream") << "Could not find input info";
        avformat_close_input(&m_audioContext);
        return false;
    }

    m_streamId = av_find_best_stream(m_audioContext, AVMEDIA_TYPE_AUDIO, -1, -1, &m_codec, 0);
    if (m_streamId < 0) {
        LERROR("FFMpegAudioStream") << "Could not find stream";
        return false;
    }

    if (m_codecContext)
        avcodec_free_context(&m_codecContext);

    m_codecContext = avcodec_alloc_context3(nullptr);
    if (!m_codecContext) {
        LERROR("FFMpegAudioStream") << "Could not create codec context";
        return false;
    }

    avcodec_parameters_to_context(m_codecContext, m_audioContext->streams[m_streamId]->codecpar);

    if (avcodec_parameters_to_context(m_codecContext, m_audioContext->streams[m_streamId]->codecpar) < 0) {
        LERROR("FFMpegAudioStream") << "Unable to avcodec_parameters_to_context";
        return false;
    }

    av_codec_set_pkt_timebase(m_codecContext, m_audioContext->streams[m_streamId]->time_base);
    av_opt_set_int(m_codecContext, "refcounted_frames", 1, 0);

    avcodec_open2(m_codecContext, m_codec, 0);

    if (avcodec_open2(m_codecContext, m_codec, 0) < 0) {
        LERROR("FFMpegAudioStream") << "Could not open codec";
        return false;
    }

    m_swrContext = swr_alloc();
    if (!m_swrContext) {
        LERROR("FFMpegAudioStream") << "Could not allocate swr context";
        return false;
    }

    AVCodecParameters *codecParams = m_audioContext->streams[m_streamId]->codecpar;

    uint64_t inLayout = codecParams->channel_layout;
    if (!inLayout) {
        int channelsCount = codecParams->channels;
        switch (channelsCount) {
        case 1: inLayout = AV_CH_LAYOUT_MONO; break;
        case 2: inLayout = AV_CH_LAYOUT_STEREO; break;
        default: LERROR("FFMpegAudioStream") << "Unknown channel layout"; break;
        }
    }

    AVSampleFormat inFormat = m_codecContext->sample_fmt;

    m_outputSampleRate = 44100;
    m_outputChanels = codecParams->channels;

    av_opt_set_int(m_swrContext, "in_channel_layout", inLayout, 0);
    av_opt_set_int(m_swrContext, "in_sample_rate", codecParams->sample_rate, 0);
    av_opt_set_sample_fmt(m_swrContext, "in_sample_fmt", inFormat, 0);
    av_opt_set_int(m_swrContext, "out_channel_layout", inLayout, 0);
    av_opt_set_int(m_swrContext, "out_sample_rate", m_outputSampleRate, 0);
    av_opt_set_sample_fmt(m_swrContext, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);

    int errc = 0;
    if ((errc = swr_init(m_swrContext)) < 0) {
        char errstr[1024];
        av_make_error_string(errstr, 1024, errc);
        LERROR("FFMpegAudioStream") << "Could not init swr context: " << std::string(errstr, 1024);
        return false;
    }

    m_outputSamplesBuffer = (int16_t *)av_malloc(sizeof(int16_t *) * m_outputChanels * m_outputSampleRate * 2);
    if (av_samples_alloc_array_and_samples(&m_outputData, nullptr, m_outputChanels,
                                           m_outputSamples, AV_SAMPLE_FMT_S16, 0) < 0) {
        LERROR("FFMpegAudioStream") << "Could not allocate samples";
        return false;
    }

    m_audioContext->event_flags = AVFMT_EVENT_FLAG_METADATA_UPDATED;

    // Update metadata
    if (m_audioLoader->getType() == AudioLoader::Stream) {
        metadataChanged("duration", std::to_string(std::numeric_limits<int64_t>::max()));
        m_duration = std::numeric_limits<int64_t>::max();
    } else {
        double tb = av_q2d(m_audioContext->streams[m_streamId]->time_base);
        int64_t duration = floor((double) m_audioContext->streams[m_streamId]->duration * tb);
        metadataChanged("duration", std::to_string(duration * 1000));
        m_duration = duration * 1000;
    }

    // Read audio metadata
    AVDictionaryEntry *titleTag = av_dict_get(m_audioContext->metadata, "title",
                                              nullptr, AV_DICT_IGNORE_SUFFIX);
    if (titleTag)
        metadataChanged("title", titleTag->value);

    AVDictionaryEntry *artistTag = av_dict_get(m_audioContext->metadata, "artist",
                                               nullptr, AV_DICT_IGNORE_SUFFIX);
    if (artistTag)
        metadataChanged("artist", artistTag->value);

    LDEBUG() << "Opened codec: " << m_codec->name;
    initialize(m_outputChanels, m_outputSampleRate);

    return true;
}

bool FFMpegAudioStream::endOfStream() const
{
    return m_pos == m_audioLoader->maxSize();
}

bool FFMpegAudioStream::onData(AudioStream::Chunk &data)
{
    if (!m_audioContext) {
        data.samples = 0;
        data.sampleCount = 0;
        return false;
    }

    data.samples = m_outputSamplesBuffer;

    while (data.sampleCount < m_outputChanels * m_outputSampleRate) {
        AVPacket pkt = {0};
        av_init_packet(&pkt);

        if (av_read_frame(m_audioContext, &pkt) < 0) {
            av_packet_unref(&pkt);
            break;
        }

        std::vector<AVFrame *> frames;
        decodePacket(&pkt, frames);

        // Read audio metadata
        AVDictionaryEntry *titleTag = av_dict_get(m_audioContext->metadata, "title",
                                                  nullptr, AV_DICT_IGNORE_SUFFIX);
        if (titleTag)
            metadataChanged("title", titleTag->value);

        AVDictionaryEntry *artistTag = av_dict_get(m_audioContext->metadata, "artist",
                                                   nullptr, AV_DICT_IGNORE_SUFFIX);
        if (artistTag)
            metadataChanged("artist", artistTag->value);

        if (frames.size() > 0) {
            for (AVFrame *frm : frames) {
                uint8_t* samples = NULL;
                int nbSamples = 0;
                int samplesLength = 0;

                resampleFrame(frm, samples, nbSamples, samplesLength);

                memcpy((void*)(data.samples + data.sampleCount), samples, samplesLength);
                data.sampleCount += nbSamples;

                av_frame_free(&frm);
            }
        }

        av_packet_unref(&pkt);
    }

    return true;
}

void FFMpegAudioStream::onSeek(const std::chrono::milliseconds &timeOffset)
{
    if (!m_audioContext)
        return;

    if (m_audioLoader->seekType() == AVSEEK_FLAG_ANY) {

        int64_t seekTarget = ((double) timeOffset.count() / 1000) * AV_TIME_BASE;
        seekTarget = av_rescale_q(seekTarget,
                                  AV_TIME_BASE_Q,
                                  m_audioContext->streams[m_streamId]->time_base);
        av_seek_frame(m_audioContext, m_streamId, seekTarget, AVSEEK_FLAG_ANY);

    } else if (m_audioLoader->seekType() == AVSEEK_FLAG_BYTE) {
        int64_t seekTarget = m_audioLoader->maxSize() * ((double)timeOffset.count() / m_duration);
        av_seek_frame(m_audioContext, m_streamId, seekTarget, AVSEEK_FLAG_BYTE);

    }
}

void FFMpegAudioStream::onStatus(AudioSource::Status status)
{
    statusChanged((int)status);
}

int64_t FFMpegAudioStream::onLoop()
{
    onSeek(std::chrono::milliseconds(0));
    return 0;
}

int FFMpegAudioStream::readBuffer(void *opaque, uint8_t *buf, int bufSize)
{
    FFMpegAudioStream *audioStream = reinterpret_cast<FFMpegAudioStream*>(opaque);
    if (!audioStream->m_audioLoader)
        return 0;
    int r = audioStream->m_audioLoader->read(buf, bufSize, audioStream->m_pos);
    audioStream->m_pos += r;
    return r;
}

int64_t FFMpegAudioStream::seekBuffer(void *opaque, int64_t offset, int whence)
{
    FFMpegAudioStream *audioStream = reinterpret_cast<FFMpegAudioStream*>(opaque);
    if (!audioStream->m_audioLoader)
        return -1;
    if (whence == AVSEEK_SIZE)
        return audioStream->m_audioLoader->maxSize();
    int64_t newPos_ = audioStream->m_audioLoader->seek(audioStream->m_pos, offset, whence);
    if (newPos_ < 0 || newPos_ > audioStream->m_audioLoader->maxSize())
        return -1;
    audioStream->m_pos = newPos_;
    return audioStream->m_pos;
}

bool FFMpegAudioStream::decodePacket(AVPacket *packet,
                                     std::vector<AVFrame *> &frames)
{
    int ret = avcodec_send_packet(m_codecContext, packet);
    while (ret  >= 0) {
        AVFrame *frm = av_frame_alloc();
        ret = avcodec_receive_frame(m_codecContext, frm);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            av_frame_free(&frm);
            break;
        }
        frames.push_back(frm);
    }
    return false;
}

void FFMpegAudioStream::resampleFrame(AVFrame *frame, uint8_t *&outSamples,
                                      int &outNbSamples, int &outSamplesLength)
{
    int srcRate = frame->sample_rate;
    int outRate = frame->sample_rate;

    m_outputSamples = av_rescale_rnd(swr_get_delay(m_swrContext, srcRate) + frame->nb_samples,
                                     outRate, srcRate, AV_ROUND_UP);

    if(m_outputSamples > m_maxOutputSamples) {
        av_free(m_outputData[0]);
        av_samples_alloc(m_outputData, nullptr, m_outputChanels, m_outputSamples, AV_SAMPLE_FMT_S16, 1);
        m_maxOutputSamples = m_outputSamples;
    }

    int res = swr_convert(m_swrContext, m_outputData, m_outputSamples,
                          (const uint8_t**)frame->extended_data, frame->nb_samples);

    int outputBufferSize = av_samples_get_buffer_size(nullptr, m_outputChanels, res, AV_SAMPLE_FMT_S16, 1);

    outNbSamples = outputBufferSize / av_get_bytes_per_sample(AV_SAMPLE_FMT_S16);
    outSamplesLength = outputBufferSize;
    outSamples = m_outputData[0];
}

NS_MA_END
