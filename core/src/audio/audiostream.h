#ifndef AUDIOSTREAM_H
#define AUDIOSTREAM_H

#include "../core_global.h"
#include "audiosource.h"

#include <thread>
#include <mutex>
#include <chrono>

NS_MA_BEGIN

class AudioStream : public AudioSource
{
public:
    struct Chunk {
        const int16_t *samples;
        std::size_t  sampleCount;
    };

    virtual ~AudioStream();

    void play();
    void pause();
    void stop();

    unsigned int channelCount() const;
    unsigned int sampleRate() const;
    Status status() const;

    void setPlayingOffset(const std::chrono::milliseconds &timeOffset);
    std::chrono::milliseconds playingOffset() const;

    void setLoop(bool loop);
    bool loop() const;

protected:
    enum {
        NoLoop = -1
    };

    AudioStream();

    void initialize(unsigned int channelCount, unsigned int sampleRate);

    virtual bool onData(Chunk &data) = 0;
    virtual void onSeek(const std::chrono::milliseconds &timeOffset) = 0;
    virtual void onStatus(Status status) { (void)status; }
    virtual int64_t onLoop();

private:
    void streamData();
    bool fillAndPushBuffer(unsigned int bufferNum, bool immediateLoop = false);
    bool fillQueue();
    void clearQueue();
    void setThreadStartState(Status status);

private:
    enum  {
        BufferCount = 3/*3*/,
        BufferRetries = 2/*2*/
    };

    std::thread m_thread;
    mutable std::mutex m_threadMutex;
    Status m_threadStartState;
    bool m_isStreaming;
    unsigned int m_buffers[BufferCount];
    unsigned int m_channelCount;
    unsigned int m_sampleRate;
    uint32_t m_format;
    bool m_loop;
    uint64_t m_samplesProcessed;
    int64_t m_bufferSeeks[BufferCount];

};

NS_MA_END

#endif // AUDIOSTREAM_H
