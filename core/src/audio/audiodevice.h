#ifndef AUDIODEVICE_H
#define AUDIODEVICE_H

#include "../core_global.h"
#include "../core_utils.h"

#include <AL/al.h>
#include <AL/alc.h>

NS_MA_BEGIN

#ifdef MACORE_DEBUG
#define alCheck(expr) do { expr; ::ma::alCheckError(__FILE__, __LINE__, #expr); } while (false)
#else
#define alCheck(expr) (expr)
#endif

void alCheckError(const char* file, unsigned int line, const char* expression);

class AudioDevice
{
public:
    static AudioDevice &instance();
    static bool init();

    static bool isExtensionSupported(const std::string& extension);
    static int formatFromChannelCount(unsigned int channelCount);

    static void setGlobalVolume(float volume);
    static float globalVolume();

    static void setPosition(const Vector3f& position);
    static Vector3f position();

    static void setDirection(const Vector3f& direction);
    static Vector3f direction();

    static void setUpVector(const Vector3f& upVector);
    static Vector3f upVector();

private:
    AudioDevice();
    ~AudioDevice();

private:
    static AudioDevice *m_i;
    ALCdevice*  m_audioDevice;
    ALCcontext* m_audioContext;

    float m_listenerVolume = 100.f;
    Vector3f m_listenerPosition;
    Vector3f m_listenerDirection;
    Vector3f m_listenerUpVector;
};

NS_MA_END

#endif // AUDIODEVICE_H
