#ifndef AUDIOPLAYLIST_H
#define AUDIOPLAYLIST_H

#include "../core_global.h"
#include "../sigslot.h"

#include <memory>
#include <vector>

NS_MA_BEGIN

class AudioTrack;

class MACORESHARED_EXPORT AudioPlaylist
{
public:
    AudioPlaylist();
    ~AudioPlaylist();

    //! Add audio track to playlist
    void addAudioTrack(AudioTrack *audioTrack);
    //! Get audio track by index in playlist
    std::shared_ptr<AudioTrack> audioTrack(int index);

    //! Playlist size
    int size() const;

    //! Emit when playlist size changed
    Signal<> changed;

private:
    std::vector<std::shared_ptr<AudioTrack>> m_audioTracks; ///< Audio tracks
};

NS_MA_END

#endif // AUDIOPLAYLIST_H
