#include "audioloader.h"

NS_MA_BEGIN

AudioLoader::AudioLoader(const std::shared_ptr<AudioTrack> &audioTrack) :
    audioTrack(audioTrack)
{

}

AudioLoader::~AudioLoader()
{
}

AudioLoader::Type AudioLoader::getType() const
{
    return type;
}

NS_MA_END
