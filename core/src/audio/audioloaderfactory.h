#ifndef AUDIOLOADERFACTORY_H
#define AUDIOLOADERFACTORY_H

#include "../core_global.h"
#include "audiotrack.h"
#include "../log.h"

#include <memory>
#include <map>

NS_MA_BEGIN

class AudioLoader;

class MACORESHARED_EXPORT AudioLoaderFactory
{
public:
    struct FactoryBase {
        virtual AudioLoader *create(const std::shared_ptr<AudioTrack> &audioTrack) = 0;
    };
    template <typename T>
    struct Factory : public FactoryBase {
        AudioLoader *create(const std::shared_ptr<AudioTrack> &audioTrack) {
            return new T(audioTrack);
        }
    };

    template <typename T>
    bool static registerFactory(const std::string &name) {
        if (instance().m_factories.find(name) != instance().m_factories.end()) {
            LWARNING() << "Audioloader factory <" << name << "> already registred";
            return false;
        }
        instance().m_factories.emplace(name, new Factory<T>());
        LINFO() << "Register audioloader factory <" << name << ">";
        return true;
    }

    static std::shared_ptr<AudioLoader> create(const std::shared_ptr<AudioTrack> &audioTrack);

private:
    static AudioLoaderFactory &instance();

    AudioLoaderFactory();
    ~AudioLoaderFactory();

private:
    std::map<std::string, FactoryBase *> m_factories;


};

NS_MA_END

#endif // AUDIOLOADERFACTORY_H
