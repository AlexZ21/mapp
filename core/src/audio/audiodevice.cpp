#include "audiodevice.h"
#include "../log.h"

NS_MA_BEGIN

void alCheckError(const char *file, unsigned int line, const char *expression)
{
    // Get the last error
    ALenum errorCode = alGetError();

    if (errorCode != AL_NO_ERROR)
    {
        std::string fileString = file;
        std::string error = "Unknown error";
        std::string description = "No description";

        // Decode the error code
        switch (errorCode) {
        case AL_INVALID_NAME: {
            error = "AL_INVALID_NAME";
            description = "A bad name (ID) has been specified.";
            break;
        }

        case AL_INVALID_ENUM: {
            error = "AL_INVALID_ENUM";
            description = "An unacceptable value has been specified for an enumerated argument.";
            break;
        }

        case AL_INVALID_VALUE: {
            error = "AL_INVALID_VALUE";
            description = "A numeric argument is out of range.";
            break;
        }

        case AL_INVALID_OPERATION: {
            error = "AL_INVALID_OPERATION";
            description = "The specified operation is not allowed in the current state.";
            break;
        }

        case AL_OUT_OF_MEMORY: {
            error = "AL_OUT_OF_MEMORY";
            description = "There is not enough memory left to execute the command.";
            break;
        }
        }

        // Log the error
        std::stringstream ss;
        ss << "An internal OpenAL call failed in ";
        ss << fileString.substr(fileString.find_last_of("\\/") + 1) << "(" << line << ").";
        ss << "\nExpression:\n   " << expression;
        ss << "\nError description:\n   " << error << "\n   " << description;
        MA_ASSERT(false, ss.str());
    }
}

AudioDevice *AudioDevice::m_i = nullptr;

AudioDevice &AudioDevice::instance()
{
    static AudioDevice ad;
    return ad;
}

bool AudioDevice::init()
{
    if (m_i) {
        LERROR() << "Audio device already inited";
        return false;
    }
    LINFO() << "Init audio device";
    m_i = &AudioDevice::instance();
    return true;
}

bool AudioDevice::isExtensionSupported(const std::string &extension)
{
    if ((extension.length() > 2) && (extension.substr(0, 3) == "ALC"))
        return alcIsExtensionPresent(instance().m_audioDevice, extension.c_str()) != AL_FALSE;
    else
        return alIsExtensionPresent(extension.c_str()) != AL_FALSE;
}

int AudioDevice::formatFromChannelCount(unsigned int channelCount)
{
    int format = 0;
    switch (channelCount) {
    case 1:  format = AL_FORMAT_MONO16; break;
    case 2:  format = AL_FORMAT_STEREO16; break;
    case 4:  format = alGetEnumValue("AL_FORMAT_QUAD16"); break;
    case 6:  format = alGetEnumValue("AL_FORMAT_51CHN16"); break;
    case 7:  format = alGetEnumValue("AL_FORMAT_61CHN16"); break;
    case 8:  format = alGetEnumValue("AL_FORMAT_71CHN16"); break;
    default: format = 0; break;
    }

    if (format == -1)
        format = 0;

    return format;
}

void AudioDevice::setGlobalVolume(float volume)
{
    if (instance().m_audioContext)
        alCheck(alListenerf(AL_GAIN, volume * 0.01f));
    instance().m_listenerVolume = volume;
}

float AudioDevice::globalVolume()
{
    return instance().m_listenerVolume;
}

void AudioDevice::setPosition(const Vector3f &position)
{
    if (instance().m_audioContext)
        alCheck(alListener3f(AL_POSITION, position.x, position.y, position.z));
    instance().m_listenerPosition = position;
}

Vector3f AudioDevice::position()
{
    return instance().m_listenerPosition;
}

void AudioDevice::setDirection(const Vector3f &direction)
{
    if (instance().m_audioContext) {
        float orientation[] = {direction.x, direction.y, direction.z,
                               instance().m_listenerUpVector.x, instance().m_listenerUpVector.y, instance().m_listenerUpVector.z};
        alCheck(alListenerfv(AL_ORIENTATION, orientation));
    }
    instance().m_listenerDirection = direction;
}

Vector3f AudioDevice::direction()
{
    return instance().m_listenerDirection;
}

AudioDevice::AudioDevice() :
    m_audioDevice(nullptr),
    m_audioContext(nullptr),
    m_listenerVolume(100.f),
    m_listenerPosition{0.f, 0.f, 0.f},
    m_listenerDirection{0.f, 0.f, -1.f},
    m_listenerUpVector{0.f, 1.f, 0.f}
{
    m_audioDevice = alcOpenDevice(NULL);
    MA_ASSERT(m_audioDevice, "Failed to open the audio device");

    m_audioContext = alcCreateContext(m_audioDevice, NULL);
    MA_ASSERT(m_audioContext, "Failed to create the audio context");

    alcMakeContextCurrent(m_audioContext);

    float orientation[] = {m_listenerDirection.x,
                           m_listenerDirection.y,
                           m_listenerDirection.z,
                           m_listenerUpVector.x,
                           m_listenerUpVector.y,
                           m_listenerUpVector.z};
    alCheck(alListenerf(AL_GAIN, m_listenerVolume * 0.01f));
    alCheck(alListener3f(AL_POSITION, m_listenerPosition.x, m_listenerPosition.y, m_listenerPosition.z));
    alCheck(alListenerfv(AL_ORIENTATION, orientation));

}

AudioDevice::~AudioDevice()
{
    alcMakeContextCurrent(NULL);
    if (m_audioContext)
        alcDestroyContext(m_audioContext);

    if (m_audioDevice)
        alcCloseDevice(m_audioDevice);
}

NS_MA_END
