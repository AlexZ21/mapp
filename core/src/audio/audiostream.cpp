#include "audiostream.h"
#include "audiodevice.h"
#include "../log.h"

NS_MA_BEGIN

AudioStream::~AudioStream()
{
    {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        m_isStreaming = false;
    }
    if (m_thread.joinable())
        m_thread.join();
}

void AudioStream::play()
{
    if (m_format == 0) {
        LERROR("AudioStream") << "Failed to play audio stream: sound parameters have not been initialized (call initialize() first)";
        return;
    }

    bool isStreaming = false;
    Status threadStartState = Stopped;

    {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        isStreaming = m_isStreaming;
        threadStartState = m_threadStartState;
    }

    if (isStreaming && (threadStartState == Paused)) {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        setThreadStartState(Playing);
        alCheck(alSourcePlay(m_source));
        return;
    } else if (isStreaming && (threadStartState == Playing)) {
        stop();
    }

    if (m_thread.joinable())
        m_thread.join();

    m_isStreaming = true;
    setThreadStartState(Playing);
    m_thread = std::thread(&AudioStream::streamData, this);
}

void AudioStream::pause()
{
    {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        if (!m_isStreaming)
            return;
        setThreadStartState(Paused);
    }
    alCheck(alSourcePause(m_source));
}

void AudioStream::stop()
{
    {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        m_isStreaming = false;
    }
    if (m_thread.joinable())
        m_thread.join();
    onSeek(std::chrono::milliseconds(0));
}

unsigned int AudioStream::channelCount() const
{
    return m_channelCount;
}

unsigned int AudioStream::sampleRate() const
{
    return m_sampleRate;
}

AudioSource::Status AudioStream::status() const
{
    Status status = AudioSource::status();
    if (status == Stopped) {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        if (m_isStreaming)
            status = m_threadStartState;
    }
    return status;
}

void AudioStream::setPlayingOffset(const std::chrono::milliseconds &timeOffset)
{
    Status oldStatus = status();
    stop();

    onSeek(timeOffset);
    m_samplesProcessed = static_cast<uint64_t>(((double) timeOffset.count() / 1000) *
                                               m_sampleRate * m_channelCount);

    switch (oldStatus) {
    case Stopped: return; break;
    case Paused: pause(); break;
    case Playing: play(); break;
    }
}

std::chrono::milliseconds AudioStream::playingOffset() const
{
    if (m_sampleRate && m_channelCount) {
        ALfloat secs = 0.f;
        alCheck(alGetSourcef(m_source, AL_SEC_OFFSET, &secs));
        return std::chrono::milliseconds((uint64_t)((secs + static_cast<float>(m_samplesProcessed) /
                                                     m_sampleRate / m_channelCount) * 1000));
    }
    return std::chrono::milliseconds(0);
}

void AudioStream::setLoop(bool loop)
{
    m_loop = loop;
}

bool AudioStream::loop() const
{
    return m_loop;
}

AudioStream::AudioStream() :
    m_threadMutex(),
    m_threadStartState(Stopped),
    m_isStreaming(false),
    m_buffers(),
    m_channelCount(0),
    m_sampleRate(0),
    m_format(0),
    m_loop(false),
    m_samplesProcessed(0),
    m_bufferSeeks()
{

}

void AudioStream::initialize(unsigned int channelCount, unsigned int sampleRate)
{
    m_channelCount = channelCount;
    m_sampleRate = sampleRate;
    m_samplesProcessed = 0;
    m_isStreaming = false;

    m_format = AudioDevice::formatFromChannelCount(channelCount);

    if (m_format == 0) {
        m_channelCount = 0;
        m_sampleRate   = 0;
        LERROR("AudioStream") << "Unsupported number of channels (" << m_channelCount << ")";
    }
}

int64_t AudioStream::onLoop()
{
    onSeek(std::chrono::milliseconds(0));
    return 0;
}

void AudioStream::streamData()
{
    bool requestStop = false;

    {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        if (m_threadStartState == Stopped) {
            m_isStreaming = false;
            return;
        }
    }

    alCheck(alGenBuffers(BufferCount, m_buffers));
    for (int i = 0; i < BufferCount; ++i)
        m_bufferSeeks[i] = NoLoop;

    requestStop = fillQueue();

    alCheck(alSourcePlay(m_source));

    {
        std::unique_lock<std::mutex> lock(m_threadMutex);
        if (m_threadStartState == Paused)
            alCheck(alSourcePause(m_source));
    }

    for (;;)
    {
        {
            std::unique_lock<std::mutex> lock(m_threadMutex);
            if (!m_isStreaming)
                break;
        }

        if (AudioSource::status() == Stopped) {
            if (!requestStop) {
                alCheck(alSourcePlay(m_source));
            } else {
                std::unique_lock<std::mutex> lock(m_threadMutex);
                m_isStreaming = false;
            }
        }

        ALint nbProcessed = 0;
        alCheck(alGetSourcei(m_source, AL_BUFFERS_PROCESSED, &nbProcessed));

        while (nbProcessed--)
        {
            ALuint buffer;
            alCheck(alSourceUnqueueBuffers(m_source, 1, &buffer));

            unsigned int bufferNum = 0;
            for (int i = 0; i < BufferCount; ++i) {
                if (m_buffers[i] == buffer) {
                    bufferNum = i;
                    break;
                }
            }

            if (m_bufferSeeks[bufferNum] != NoLoop) {
                m_samplesProcessed = m_bufferSeeks[bufferNum];
                m_bufferSeeks[bufferNum] = NoLoop;
            } else {
                ALint size, bits;
                alCheck(alGetBufferi(buffer, AL_SIZE, &size));
                alCheck(alGetBufferi(buffer, AL_BITS, &bits));

                if (bits == 0)
                {
                    LERROR("AudioStream") << "Bits in sound stream are 0: make sure that the audio format is not corrupt "
                                          << "and initialize() has been called correctly";

                    std::unique_lock<std::mutex> lock(m_threadMutex);
                    m_isStreaming = false;
                    requestStop = true;
                    break;
                } else {
                    m_samplesProcessed += size / (bits / 8);
                }
            }

            if (!requestStop) {
                if (fillAndPushBuffer(bufferNum))
                    requestStop = true;
            }
        }

        if (AudioSource::status() != Stopped)
            std::this_thread::sleep_for(std::chrono::microseconds(10));
    }

    alCheck(alSourceStop(m_source));

    clearQueue();

    m_samplesProcessed = 0;

    alCheck(alSourcei(m_source, AL_BUFFER, 0));
    alCheck(alDeleteBuffers(BufferCount, m_buffers));

    setThreadStartState(Stopped);
}

bool AudioStream::fillAndPushBuffer(unsigned int bufferNum, bool immediateLoop)
{
    bool requestStop = false;

    Chunk data = {NULL, 0};
    for (uint32_t retryCount = 0; !onData(data) && (retryCount < BufferRetries); ++retryCount)
    {
        if (!m_loop) {
            if (data.samples != NULL && data.sampleCount != 0)
                m_bufferSeeks[bufferNum] = 0;
            requestStop = true;
            break;
        }

        m_bufferSeeks[bufferNum] = onLoop();

        if (data.samples != NULL && data.sampleCount != 0)
            break;

        if (immediateLoop && (m_bufferSeeks[bufferNum] != NoLoop)) {
            m_samplesProcessed = m_bufferSeeks[bufferNum];
            m_bufferSeeks[bufferNum] = NoLoop;
        }

    }

    if (data.samples && data.sampleCount) {
        unsigned int buffer = m_buffers[bufferNum];

        ALsizei size = static_cast<ALsizei>(data.sampleCount) * sizeof(int16_t);
        alCheck(alBufferData(buffer, m_format, data.samples, size, m_sampleRate));

        alCheck(alSourceQueueBuffers(m_source, 1, &buffer));
    } else {
        requestStop = true;
    }

    return requestStop;
}

bool AudioStream::fillQueue()
{
    bool requestStop = false;
    for (int i = 0; (i < BufferCount) && !requestStop; ++i)
    {
        if (fillAndPushBuffer(i, (i == 0)))
            requestStop = true;
    }

    return requestStop;
}

void AudioStream::clearQueue()
{
    ALint nbQueued;
    alCheck(alGetSourcei(m_source, AL_BUFFERS_QUEUED, &nbQueued));

    ALuint buffer;
    for (ALint i = 0; i < nbQueued; ++i)
        alCheck(alSourceUnqueueBuffers(m_source, 1, &buffer));
}

void AudioStream::setThreadStartState(AudioSource::Status status)
{
    m_threadStartState = status;
    onStatus(status);
}

NS_MA_END
