#ifndef AUDIOSOURCE_H
#define AUDIOSOURCE_H

#include "../core_global.h"
#include "../core_utils.h"
#include "audiodevice.h"

NS_MA_BEGIN

class AudioSource
{
public:    
    enum Status {
        Stopped,
        Paused,
        Playing
    };

    AudioSource(const AudioSource& copy);
    virtual ~AudioSource();

    void setPitch(float pitch);
    float pitch() const;

    void setVolume(float volume);
    float volume() const;

    void setPosition(float x, float y, float z);
    void setPosition(const Vector3f& position);
    Vector3f position() const;

    void setRelativeToListener(bool relative);
    bool isRelativeToListener() const;

    void setMinDistance(float distance);
    float minDistance() const;

    void setAttenuation(float attenuation);
    float attenuation() const;

    AudioSource &operator =(const AudioSource& right);

protected:
    AudioSource();

    Status status() const;

protected:
    unsigned int m_source; ///< OpenAL source identifier

};

NS_MA_END

#endif // AUDIOSOURCE_H
