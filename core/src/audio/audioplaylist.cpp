#include "audioplaylist.h"
#include "audiotrack.h"

NS_MA_BEGIN

AudioPlaylist::AudioPlaylist()
{

}

AudioPlaylist::~AudioPlaylist()
{
}

void AudioPlaylist::addAudioTrack(AudioTrack *audioTrack)
{
    m_audioTracks.emplace_back(audioTrack);
    changed();
}

std::shared_ptr<AudioTrack> AudioPlaylist::audioTrack(int index)
{
    if (m_audioTracks.size() <= index)
        return nullptr;
    return m_audioTracks.at(index);
}

int AudioPlaylist::size() const
{
    return m_audioTracks.size();
}

NS_MA_END
