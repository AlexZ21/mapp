#ifndef CORE_SIGNAL_H
#define CORE_SIGNAL_H

#include "core_global.h"

#include <unordered_map>
#include <functional>
#include <memory>

NS_MA_BEGIN

//! Простоя реализация сигнал-слот
template <typename ...Attrs>
class MACORESHARED_EXPORT Signal
{
public:
    struct FuncAttrs {
        FuncAttrs(std::function<void(Attrs...)> f) : func(f) {}
        std::function<void(Attrs...)> func;
    };

    template <typename F>
    void connect(F &&functor, void *context = nullptr) {
        std::shared_ptr<FuncAttrs> ptr(new FuncAttrs(std::function<void(Attrs...)>(functor)));
        _slots.insert(std::pair<void *, std::shared_ptr<FuncAttrs>>(context, ptr));
    }

    template <typename O, typename M>
    void connect(O *object, M method) {
        std::shared_ptr<FuncAttrs> ptr(new FuncAttrs(std::function<void(Attrs...)>([object, method](Attrs ...args){
            (object->*method)(std::forward<Attrs>(args)...);
        })));
        _slots.insert(std::pair<void *, std::shared_ptr<FuncAttrs>>(object, ptr));
    }

    void operator()(Attrs ...args) {
        for (auto &it : _slots)
            it.second->func(std::forward<Attrs>(args)...);
    }

    void disconnect() {
        _slots.erase(_slots.begin(), _slots.end());
    }

    void disconnect(void *context) {
        auto it = _slots.find(context);
        if (it != _slots.end())
            _slots.erase(it);
    }

private:
    std::unordered_multimap<void *, std::shared_ptr<FuncAttrs>> _slots;

};

NS_MA_END

#endif // CORE_SIGNAL_H
