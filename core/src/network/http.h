#ifndef HTTP_H
#define HTTP_H

#include "../core_global.h"
#include "ipaddress.h"
#include "tcpsocket.h"
#include "url.h"

#include <map>
#include <string>
#include <chrono>
#include <functional>
#include <thread>
#include <atomic>
#include <tuple>

NS_MA_BEGIN

class MACORESHARED_EXPORT Http
{
public:
    class MACORESHARED_EXPORT Request
    {
    public:
        enum Method {
            Get,
            Post,
            Head,
            Put,
            Delete
        };

        Request(const Url &url, Method method = Get, const std::string& body = "");
        Request(const Request &other);

        Request &operator =(const Request &other);

        void setField(const std::string& field, const std::string& value);
        void setMethod(Method method);
        void setUrl(const Url &url);
        void setHttpVersion(unsigned int major, unsigned int minor);
        void setBody(const std::string& body);

    private:
        friend class Http;
        std::string prepare() const;
        bool hasField(const std::string& field) const;

    private:
        typedef std::map<std::string, std::string> FieldTable;

        FieldTable m_fields;
        Method m_method;
        Url m_url;
        unsigned int m_majorVersion;
        unsigned int m_minorVersion;
        std::string m_body;

    };

    class MACORESHARED_EXPORT Response
    {
    public:
        enum Status {
            Ok             = 200,
            Created        = 201,
            Accepted       = 202,
            NoContent      = 204,
            ResetContent   = 205,
            PartialContent = 206,

            MultipleChoices  = 300,
            MovedPermanently = 301,
            MovedTemporarily = 302,
            NotModified      = 304,

            BadRequest          = 400,
            Unauthorized        = 401,
            Forbidden           = 403,
            NotFound            = 404,
            RangeNotSatisfiable = 407,

            InternalServerError = 500,
            NotImplemented      = 501,
            BadGateway          = 502,
            ServiceNotAvailable = 503,
            GatewayTimeout      = 504,
            VersionNotSupported = 505,

            InvalidResponse  = 1000,
            ConnectionFailed = 1001
        };

        Response();

        const std::string& field(const std::string& field) const;
        Status status() const;
        unsigned int majorHttpVersion() const;
        unsigned int minorHttpVersion() const;
        const std::string& body() const;

    private:
        friend class Http;
        int parseHeader(const std::string &data);
        std::string parseBodyData(const std::string &data, int *pos = nullptr);

    private:
        typedef std::map<std::string, std::string> FieldTable;

        FieldTable m_fields;
        Status m_status;
        unsigned int m_majorVersion;
        unsigned int m_minorVersion;
        std::string m_body;

    };

    Http();
    ~Http();

    void setFollowLocation(bool followLocation);
    void setStoreBody(bool storeBody);
    void setConnectionTimeout(const std::chrono::milliseconds &timeout);
    void setOnHeaderReady(const std::function<void (const Response &)> &onHeaderReady);
    void setOnBodyDataRecieved(const std::function<void (const char *, size_t)> &onBodyDataRecieved);
    void setOnReady(const std::function<void (const Response &)> &onReady);
    void setOnError(const std::function<void ()> &onError);

    Response sendRequest(const Request& request,
                         std::chrono::milliseconds timeout = std::chrono::milliseconds(0));
    void sendRequestAsync(const Request& request,
                          std::chrono::milliseconds timeout = std::chrono::milliseconds(0));

    void stop();
    bool isRunning();

private:
    static void initSsl();

    uint16_t portByProtocol(const std::string &protocol);

    Response sendRequestHelper(const Request& request,
                               std::string &locationUrl,
                               std::chrono::milliseconds timeout = std::chrono::milliseconds(0));
    Response sendSecureRequestHelper(const Request& request,
                                     std::string &locationUrl,
                                     std::chrono::milliseconds timeout = std::chrono::milliseconds(0));
    int processReceiveData(std::string &receivedStr, bool &headerParsed, std::string &locationUrl,
                            Response &response);

private:
    static bool m_sslInited;

    TcpSocket m_connection;
    std::chrono::milliseconds m_connectionTimeout;

    bool m_followLocation;
    bool m_storeBody;

    std::function<void (const Response &)> m_onHeaderReady;
    std::function<void (const char *, size_t)> m_onBodyDataRecieved;
    std::function<void (const Response &)> m_onReady;
    std::function<void ()> m_onError;

    std::thread m_thread;
    std::atomic_bool m_running;

};

NS_MA_END

#endif // HTTP_H

