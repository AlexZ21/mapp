#ifndef SOCKETIMPL_H
#define SOCKETIMPL_H

#include "../../core_global.h"
#include "../socket.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

NS_MA_BEGIN

class MACORESHARED_EXPORT SocketImpl
{
public:
    typedef socklen_t AddrLength;

    static sockaddr_in createAddress(uint32_t address, unsigned short port);
    static SocketHandle invalidSocket();
    static void close(SocketHandle sock);
    static void setBlocking(SocketHandle sock, bool block);
    static Socket::Status getErrorStatus();

};

NS_MA_END

#endif // SOCKETIMPL_H
