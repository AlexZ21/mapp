#ifndef SOCKETHANDLE_H
#define SOCKETHANDLE_H

#include "../core_global.h"

#if defined(SYSTEM_WIN)
    #include <basetsd.h>
#endif

NS_MA_BEGIN

#if defined(SYSTEM_WIN)
    typedef UINT_PTR SocketHandle;
#else
    typedef int SocketHandle;
#endif

NS_MA_END

#endif // SOCKETHANDLE_H
