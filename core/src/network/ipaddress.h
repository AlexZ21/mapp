#ifndef IPADDRESS_H
#define IPADDRESS_H

#include "../core_global.h"

#include <iostream>
#include <chrono>

NS_MA_BEGIN

class MACORESHARED_EXPORT IpAddress
{
public:
    IpAddress();
    IpAddress(const std::string &address);
    IpAddress(const char *address);
    IpAddress(uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3);
    explicit IpAddress(uint32_t address);

    std::string toString() const;
    uint32_t toInteger() const;

    static const IpAddress None;
    static const IpAddress Any;
    static const IpAddress LocalHost;
    static const IpAddress Broadcast;

private:
    friend MACORESHARED_EXPORT bool operator <(const IpAddress &left, const IpAddress &right);

    void resolve(const std::string &address);

private:
    uint32_t m_address;
    bool   m_valid;
};

MACORESHARED_EXPORT bool operator ==(const IpAddress &left, const IpAddress &right);
MACORESHARED_EXPORT bool operator !=(const IpAddress &left, const IpAddress &right);
MACORESHARED_EXPORT bool operator <(const IpAddress &left, const IpAddress &right);
MACORESHARED_EXPORT bool operator >(const IpAddress &left, const IpAddress &right);
MACORESHARED_EXPORT bool operator <=(const IpAddress &left, const IpAddress &right);
MACORESHARED_EXPORT bool operator >=(const IpAddress &left, const IpAddress &right);
MACORESHARED_EXPORT std::istream &operator >>(std::istream &stream, IpAddress &address);
MACORESHARED_EXPORT std::ostream &operator <<(std::ostream &stream, const IpAddress &address);

NS_MA_END

#endif // IPADDRESS_H
