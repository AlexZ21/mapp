#ifndef URL_H
#define URL_H

#include "../core_global.h"

#include <string>

NS_MA_BEGIN

class MACORESHARED_EXPORT Url
{
public:
    Url(const std::string &url = std::string());

    std::string protocol() const;
    std::string host() const;
    std::string port() const;
    std::string path() const;
    std::string query() const;

    std::string fullHost() const;
    std::string fullUri() const;

    bool isValid() const;

private:
    void parse(const std::string &url);

private:
    std::string m_protocol;
    std::string m_host;
    std::string m_port;
    std::string m_path;
    std::string m_query;

};

NS_MA_END

#endif // URL_H
