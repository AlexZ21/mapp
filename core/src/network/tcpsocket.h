#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "../core_global.h"
#include "socket.h"

#include <stdint.h>
#include <chrono>

NS_MA_BEGIN

class TcpListener;
class IpAddress;

class MACORESHARED_EXPORT TcpSocket : public Socket
{
public:
    TcpSocket();

    unsigned short localPort() const;
    IpAddress remoteAddress() const;
    unsigned short remotePort() const;

    Status connect(const IpAddress& remAddr, unsigned short remPort,
                   std::chrono::milliseconds timeout = std::chrono::milliseconds(0));
    void disconnect();

    Status send(const void* data, std::size_t size);
    Status send(const void* data, std::size_t size, std::size_t& sent);

    Status receive(void* data, std::size_t size, std::size_t& received);
    Status receive(void* data, std::size_t size, std::size_t& received,
                   std::chrono::milliseconds timeout);

private:
    friend class TcpListener;

};

NS_MA_END

#endif // TCPSOCKET_H

