#include "url.h"

#include <algorithm>

NS_MA_BEGIN

Url::Url(const std::string &url)
{
    parse(url);
}

std::string Url::protocol() const
{
    return m_protocol;
}

std::string Url::host() const
{
    return m_host;
}

std::string Url::port() const
{
    return m_port;
}

std::string Url::path() const
{
    return m_path;
}

std::string Url::query() const
{
    return m_query;
}

std::string Url::fullHost() const
{
    std::string fh = m_protocol + "://" + m_host;
    if (!m_port.empty())
        fh += ":" + m_port;
    return fh;
}

std::string Url::fullUri() const
{
    return m_path + m_query;
}

bool Url::isValid() const
{
    return (!m_protocol.empty() && !m_host.empty());
}

void Url::parse(const std::string &url)
{
    if (url.size() == 0)
        return;

    using it = std::string::const_iterator;

    it urlEnd = url.end();

    // Get query start
    it queryStart = std::find(url.begin(), urlEnd, L'?');

    // Protocol
    it protocolStart = url.begin();
    it protocolEnd = std::find(protocolStart, urlEnd, L':');

    if (protocolEnd != urlEnd) {
        std::string prot = &*(protocolEnd);
        if ((prot.length() > 3) && (prot.substr(0, 3) == "://")) {
            m_protocol = std::string(protocolStart, protocolEnd);
            protocolEnd += 3;
        } else {
            protocolEnd = url.begin();
        }
    } else {
        protocolEnd = url.begin();
    }

    // Host
    it hostStart = protocolEnd;
    it pathStart = std::find(hostStart, urlEnd, '/');
    it hostEnd = std::find(protocolEnd, (pathStart != urlEnd) ? pathStart : queryStart, ':');

    m_host = std::string(hostStart, hostEnd);

    // port
    if ((hostEnd != urlEnd) && ((&*(hostEnd))[0] == ':')) {
        hostEnd++;
        it portEnd = (pathStart != urlEnd) ? pathStart : queryStart;
        m_port = std::string(hostEnd, portEnd);
    }

    // Path
    if (pathStart != urlEnd)
        m_path = std::string(pathStart, queryStart);

    // Query
    if (queryStart != urlEnd)
        m_query = std::string(queryStart, url.end());
}

NS_MA_END
