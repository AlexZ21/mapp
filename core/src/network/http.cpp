#include "http.h"
#include "socketselector.h"
#include "../log.h"

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>

#include <cctype>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <limits>

namespace
{
std::string toLower(std::string str) {
    for (std::string::iterator i = str.begin(); i != str.end(); ++i)
        *i = static_cast<char>(std::tolower(*i));
    return str;
}
}

NS_MA_BEGIN

Http::Request::Request(const Url &url, Method method, const std::string &body)
{
    setMethod(method);
    setUrl(url);
    setHttpVersion(1, 0);
    setBody(body);
}

Http::Request::Request(const Http::Request &other)
{
    setMethod(other.m_method);
    setUrl(other.m_url);
    setHttpVersion(other.m_majorVersion, other.m_minorVersion);
    setBody(other.m_body);

    for (auto &it : other.m_fields) {
        m_fields.emplace(it.first, it.second);
    }
}

Http::Request &Http::Request::operator =(const Http::Request &other)
{
    setMethod(other.m_method);
    setUrl(other.m_url);
    setHttpVersion(other.m_majorVersion, other.m_minorVersion);
    setBody(other.m_body);

    for (auto &it : other.m_fields) {
        m_fields.emplace(it.first, it.second);
    }

    return *this;
}

void Http::Request::setField(const std::string& field, const std::string& value)
{
    m_fields[toLower(field)] = value;
}

void Http::Request::setMethod(Http::Request::Method method)
{
    m_method = method;
}

void Http::Request::setUrl(const Url &url)
{
    if (url.isValid()) {
        m_url = url;
    } else {
        LERROR() << "Invalid url";
    }
}

void Http::Request::setHttpVersion(unsigned int major, unsigned int minor)
{
    m_majorVersion = major;
    m_minorVersion = minor;
}

void Http::Request::setBody(const std::string& body)
{
    m_body = body;
}

std::string Http::Request::prepare() const
{
    std::ostringstream out;

    std::string method;
    switch (m_method) {
    case Get: method = "GET"; break;
    case Post: method = "POST"; break;
    case Head: method = "HEAD"; break;
    case Put: method = "PUT"; break;
    case Delete: method = "DELETE"; break;
    }

    std::string fullUri = m_url.fullUri();
    if (fullUri.empty())
        fullUri = "/";
    out << method << " " << fullUri << " ";
    out << "HTTP/" << m_majorVersion << "." << m_minorVersion << "\r\n";

    for (FieldTable::const_iterator i = m_fields.begin(); i != m_fields.end(); ++i) {
        out << i->first << ": " << i->second << "\r\n";
    }

    out << "\r\n";
    out << m_body;

    return out.str();
}

bool Http::Request::hasField(const std::string& field) const
{
    return m_fields.find(toLower(field)) != m_fields.end();
}

Http::Response::Response() :
    m_status(ConnectionFailed),
    m_majorVersion(0),
    m_minorVersion(0)
{

}

const std::string& Http::Response::field(const std::string& field) const
{
    FieldTable::const_iterator it = m_fields.find(toLower(field));
    if (it != m_fields.end()) {
        return it->second;
    } else {
        static const std::string empty = "";
        return empty;
    }
}

Http::Response::Status Http::Response::status() const
{
    return m_status;
}

unsigned int Http::Response::majorHttpVersion() const
{
    return m_majorVersion;
}

unsigned int Http::Response::minorHttpVersion() const
{
    return m_minorVersion;
}

const std::string& Http::Response::body() const
{
    return m_body;
}

int Http::Response::parseHeader(const std::string &data)
{
    std::istringstream in(data);

    std::string version;
    if (in >> version) {
        if ((version.size() >= 8) && (version[6] == '.') &&
                (toLower(version.substr(0, 5)) == "http/")   &&
                isdigit(version[5]) && isdigit(version[7])) {
            m_majorVersion = version[5] - '0';
            m_minorVersion = version[7] - '0';
        } else {
            m_status = InvalidResponse;
            return -1;
        }
    }

    int status;
    if (in >> status) {
        m_status = static_cast<Status>(status);
    } else {
        m_status = InvalidResponse;
        return -1;
    }

    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::string line;
    while (std::getline(in, line) && (line.size() > 2)) {
        std::string::size_type pos = line.find(":");
        if (pos != std::string::npos) {
            std::string field = line.substr(0, pos);
            std::string value = line.substr(pos + 1);
            if (value.front() == ' ')
                value.erase(0, 1);

            if (!value.empty() && (*value.rbegin() == '\r'))
                value.erase(value.size() - 1);

            m_fields[toLower(field)] = value;
        }
    }

    return (in.tellg() == in.gcount()) ? -1 : (int)in.tellg();
}

std::string Http::Response::parseBodyData(const std::string &data, int *pos)
{
    if (toLower(field("transfer-encoding")) != "chunked") {
        if (pos)
            *pos = data.size();
        return data;
    } else {
        // TODO: Chunked data
        //        std::istringstream in(data);
        std::string parsedData;
        LDEBUG() << "Chunked";
        //        std::size_t length;

        //        while (in >> std::hex >> length) {
        //            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        //            std::istreambuf_iterator<char> it(in);
        //            std::istreambuf_iterator<char> itEnd;
        //            for (std::size_t i = 0; ((i < length) && (it != itEnd)); i++)
        //                m_body.push_back(*it++);
        //        }

        return parsedData;
    }
}

bool Http::m_sslInited = false;

Http::Http() :
    m_connectionTimeout(0),
    m_followLocation(false),
    m_storeBody(false),
    m_running(false)
{

}

Http::~Http()
{
    if (m_thread.joinable()) {
        m_running.store(false);
        m_thread.join();
    }
}

void Http::setFollowLocation(bool followLocation)
{
    m_followLocation = followLocation;
}

void Http::setStoreBody(bool storeBody)
{
    m_storeBody = storeBody;
}

void Http::setConnectionTimeout(const std::chrono::milliseconds &timeout)
{
    m_connectionTimeout = timeout;
}

Http::Response Http::sendRequest(const Http::Request& request, std::chrono::milliseconds timeout)
{
    std::string locationUrl;
    Http::Request req = request;
    Http::Response response;

    if (!req.m_url.isValid())
        return response;

    m_running.store(true);

    while (m_running.load()) {
        if (req.m_url.protocol() == "https")
            response = sendSecureRequestHelper(req, locationUrl, timeout);
        else
            response = sendRequestHelper(req, locationUrl, timeout);

        if (!locationUrl.empty()) {
            req.setUrl(locationUrl);
            locationUrl.clear();
            continue;
        }

        break;
    }

    m_running.store(false);

    return response;
}

void Http::sendRequestAsync(const Http::Request &request, std::chrono::milliseconds timeout)
{
    if (m_thread.joinable())
        return;
    m_thread = std::thread([this, request, timeout](){
        sendRequest(request, timeout);
    });
}

void Http::setOnReady(const std::function<void (const Response &)> &onReady)
{
    m_onReady = onReady;
}

void Http::setOnError(const std::function<void ()> &onError)
{
    m_onError = onError;
}

void Http::setOnBodyDataRecieved(const std::function<void (const char *, size_t)> &onBodyDataRecieved)
{
    m_onBodyDataRecieved = onBodyDataRecieved;
}

void Http::setOnHeaderReady(const std::function<void (const Response &)> &onHeaderReady)
{
    m_onHeaderReady = onHeaderReady;
}

void Http::initSsl()
{
    if (!m_sslInited) {
        OpenSSL_add_all_algorithms();
        ERR_load_BIO_strings();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        if(SSL_library_init() < 0) {
            LERROR() << "Could not initialize the OpenSSL library";
            return;
        }
        m_sslInited = true;
    }
}

uint16_t Http::portByProtocol(const std::string &protocol)
{
    if (protocol == "http")
        return 80;
    else if (protocol == "https")
        return 443;
    else
        return 80;
}

Http::Response Http::sendRequestHelper(const Request& request,
                                       std::string &locationUrl,
                                       std::chrono::milliseconds timeout)
{
    Request toSend(request);

    IpAddress host(toSend.m_url.host());
    uint16_t port = toSend.m_url.port().empty() ?
                portByProtocol(toSend.m_url.protocol()) :
                std::stoul(toSend.m_url.port());

    if (!toSend.hasField("From"))
        toSend.setField("From", "user@mapp.com");

    if (!toSend.hasField("User-Agent"))
        toSend.setField("User-Agent", "mapp/1.x");

    if (!toSend.hasField("Host"))
        toSend.setField("Host", toSend.m_url.host());

    if (!toSend.hasField("Content-Length")) {
        std::ostringstream out;
        out << toSend.m_body.size();
        toSend.setField("Content-Length", out.str());
    }

    if ((toSend.m_method == Request::Post) && !toSend.hasField("Content-Type"))
        toSend.setField("Content-Type", "application/x-www-form-urlencoded");

    if ((toSend.m_majorVersion * 10 + toSend.m_minorVersion >= 11) && !toSend.hasField("Connection"))
        toSend.setField("Connection", "close");

    Response received;

    if (m_connection.connect(host, port, m_connectionTimeout) == Socket::Done) {
        std::string requestStr = toSend.prepare();

        if (!requestStr.empty()) {
            if (m_connection.send(requestStr.c_str(), requestStr.size()) == Socket::Done) {
                std::string receivedStr;
                std::size_t size = 0;
                char buffer[1024];

                bool headerParsed = false;

                Socket::Status status;

                while (m_running.load()) {
                    status = m_connection.receive(buffer, sizeof(buffer), size, timeout);

                    if (status != Socket::Done && status != Socket::Timeout)
                        break;

                    if (size > 0) {
                        receivedStr.append(buffer, size);
                        int ret = processReceiveData(receivedStr, headerParsed,
                                                     locationUrl, received);
                        if (ret == 1) {
                            m_connection.disconnect();
                            return received;
                        }
                    }
                }

                if (!receivedStr.empty()) {
                    std::string bodyStr = received.parseBodyData(receivedStr);

                    if (m_storeBody)
                        received.m_body.append(bodyStr);

                    if (bodyStr.size() > 0 && m_onBodyDataRecieved)
                        m_onBodyDataRecieved(bodyStr.c_str(), bodyStr.size());
                }
            }
        }

        m_connection.disconnect();
    } else {
        if (m_onError)
            m_onError();
    }

    return received;
}

// OpenSSL read with timeout
int sslRead(SSL *ssl, void *buf, int num,
            TcpSocket &socket, std::chrono::milliseconds timeout)
{
    if (timeout.count() == 0)
        return SSL_read(ssl, buf, num);

    SocketSelector selector;
    selector.add(socket);
    if (selector.wait(timeout))
        return SSL_read(ssl, buf, num);
    else
        return -1;
}

Http::Response Http::sendSecureRequestHelper(const Request& request,
                                             std::string &locationUrl,
                                             std::chrono::milliseconds timeout)
{
    Request toSend(request);

    IpAddress host(toSend.m_url.host());
    uint16_t port = toSend.m_url.port().empty() ?
                portByProtocol(toSend.m_url.protocol()) :
                std::stoul(toSend.m_url.port());

    if (!toSend.hasField("User-Agent"))
        toSend.setField("User-Agent", "mapp/1.0");

    if (!toSend.hasField("Host"))
        toSend.setField("Host", toSend.m_url.host());

    if (!toSend.hasField("Content-Length")) {
        std::ostringstream out;
        out << toSend.m_body.size();
        toSend.setField("Content-Length", out.str());
    }

    if ((toSend.m_method == Request::Post) && !toSend.hasField("Content-Type"))
        toSend.setField("Content-Type", "application/x-www-form-urlencoded");

    if ((toSend.m_majorVersion * 10 + toSend.m_minorVersion >= 11) && !toSend.hasField("Connection"))
        toSend.setField("Connection", "close");

    Response received;

    initSsl();

    if (!m_sslInited) {
        LERROR() << "OpenSSL library not initialized";
        if (m_onError)
            m_onError();
        return received;
    }

    if (m_connection.connect(host, port, m_connectionTimeout) == Socket::Done) {

        const SSL_METHOD *method = SSLv23_method();

        SSL_CTX *ctx = SSL_CTX_new(method);
        if (!ctx) {
            LERROR() << "Unable to create a new SSL context structure.";
            if (m_onError)
                m_onError();
            return received;
        }

        const long flags = SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
        SSL_CTX_set_options(ctx, flags);

        SSL *ssl = SSL_new(ctx);
        SSL_set_fd(ssl, m_connection.handle());

        const char *const PREFERRED_CIPHERS = "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4";
        SSL_set_cipher_list(ssl, PREFERRED_CIPHERS);

        SSL_set_tlsext_host_name(ssl, toSend.m_url.host().c_str());

        int sslc = SSL_connect(ssl);
        if (sslc != 1) {
            long error = ERR_get_error();
            const char* error_str = ERR_error_string(error, NULL);
            LERROR() << "Could not build a SSL session: " << error_str;
            if (m_onError)
                m_onError();
            return received;
        }

        std::string requestStr = toSend.prepare();

        if (!requestStr.empty()) {
            if (SSL_write(ssl, requestStr.c_str(), requestStr.size()) > 0) {
                std::string receivedStr;
                std::size_t size = 0;
                char buffer[1024];

                bool headerParsed = false;

                while (m_running.load() &&
                       ((size = sslRead(ssl, buffer, 1024, m_connection, timeout)) != 0)) {
                    if (size > 0 && size <= 1024) {
                        receivedStr.append(buffer, size);
                        int ret = processReceiveData(receivedStr, headerParsed,
                                                     locationUrl, received);
                        if (ret == 1) {
                            SSL_free(ssl);
                            SSL_CTX_free(ctx);
                            m_connection.disconnect();
                            return received;
                        }
                    }

                }

                if (!receivedStr.empty()) {
                    std::string bodyStr = received.parseBodyData(receivedStr);

                    if (m_storeBody)
                        received.m_body.append(bodyStr);

                    if (bodyStr.size() > 0 && m_onBodyDataRecieved)
                        m_onBodyDataRecieved(bodyStr.c_str(), bodyStr.size());
                }
            }
        }

        SSL_free(ssl);
        SSL_CTX_free(ctx);

        m_connection.disconnect();

        if (m_onReady)
            m_onReady(received);

    } else {
        if (m_onError)
            m_onError();
    }

    return received;
}

void Http::stop()
{
    m_running.store(false);
    if (m_thread.joinable())
        m_thread.join();
}

bool Http::isRunning()
{
    return m_running.load();
}

int Http::processReceiveData(std::string &receivedStr, bool &headerParsed,
                             std::string &locationUrl, Http::Response &response)
{
    if (!headerParsed) {
        int pos = -1;
        if ((pos = response.parseHeader(receivedStr)) > 0) {
            // If redirect
            if (m_followLocation) {
                if (!response.field("location").empty()) {
                    locationUrl = response.field("location");
                    return 1;
                }
            }

            headerParsed = true;

            if (pos > 0)
                receivedStr.erase(0, pos);

            if (m_onHeaderReady)
                m_onHeaderReady(response);
        }

    } else {
        int pos = -1;
        std::string bodyStr = response.parseBodyData(receivedStr, &pos);

        if (pos > 0)
            receivedStr.erase(0, pos);

        if (m_storeBody)
            response.m_body.append(bodyStr);

        if (bodyStr.size() > 0 && m_onBodyDataRecieved)
            m_onBodyDataRecieved(bodyStr.c_str(), bodyStr.size());
    }

    return 0;
}


NS_MA_END
