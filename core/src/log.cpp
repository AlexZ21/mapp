#include "log.h"

#include <chrono>
#include <ctime>
#include <iostream>

NS_MA_BEGIN

LogMsg::~LogMsg()
{
    if (stream->ref == 1) {
        stream->buffer += stream->ss.str();
        if (log)
            log->printMessage(stream->buffer);
    }

    --stream->ref;
    if (stream->ref == 0)
        delete stream;
}

LogTextMsg::~LogTextMsg()
{
    if (stream->ref == 1) {
        std::chrono::time_point<std::chrono::system_clock> now;
        now = std::chrono::system_clock::now();
        std::time_t time = std::chrono::system_clock::to_time_t(now);
        std::string nowTime = std::ctime(&time);
        nowTime.pop_back();

        stream->buffer.append("[").append(nowTime).append("]");

        switch (level) {
        case Debug:
            stream->buffer.append("[DEBUG]");
            break;
        case Info:
            stream->buffer.append("[INFO]");
            break;
        case Warning:
            stream->buffer.append("[WARNING]");
            break;
        case Error:
            stream->buffer.append("[ERROR]");
            break;
        case Fatal:
            stream->buffer.append("[FATAL]");
            break;
        default:
            break;
        }

        if (!context.empty())
            stream->buffer.append("[").append(context).append("]");
        stream->buffer.append(" ");
    }
}

Log &Log::instance()
{
    static Log s;
    return s;
}

LogDebugMsg Log::debug(const std::string &c)
{
    LogDebugMsg m = LogDebugMsg(c);
    m.log = &instance();
    return m;
}

LogInfoMsg Log::info(const std::string &c)
{
    LogInfoMsg m = LogInfoMsg(c);
    m.log = &instance();
    return m;
}

LogWarningMsg Log::warning(const std::string &c)
{
    LogWarningMsg m = LogWarningMsg(c);
    m.log = &instance();
    return m;
}

LogErrorMsg Log::error(const std::string &c)
{
    LogErrorMsg m = LogErrorMsg(c);
    m.log = &instance();
    return m;
}

LogFatalMsg Log::fatal(const std::string &c)
{
    LogFatalMsg m = LogFatalMsg(c);
    m.log = &instance();
    return m;
}

void Log::setOutputFile(const std::string &fileName)
{
    if (instance().m_fileStream.is_open())
        instance().m_fileStream.close();

    instance().m_fileStream.open(fileName, std::ios::app);
}

Log::Log()
{

}

Log::~Log()
{

}

void Log::printMessage(const std::string &msg)
{
    fprintf(stdout, "%s\n", msg.c_str());
    fflush(stdout);

    if (m_fileStream.is_open())
        m_fileStream << msg << "\n";
}

NS_MA_END
