#ifndef FS_H
#define FS_H

#if defined(SYSTEM_WIN)
    #include "win/fsimpl.h"
#else
    #include "unix/fsimpl.h"
#endif

#endif // FS_H
