#include "library.h"

NS_MA_BEGIN

Library::Library()
{

}

bool Library::load(const std::string &libPath)
{
    return m_library.load(libPath);
}

void *Library::resolve(const std::string &libPath)
{
    return m_library.resolve(libPath);
}

NS_MA_END
