#ifndef LIBRARY_H
#define LIBRARY_H

#include "../core_global.h"

#if defined(SYSTEM_WIN)
    #include "win/libraryimpl.h"
#else
    #include "unix/libraryimpl.h"
#endif

NS_MA_BEGIN

class MACORESHARED_EXPORT Library
{
public:
    Library();

    bool load(const std::string &libPath);
    void *resolve(const std::string &libPath);

private:
    LibraryImpl m_library;
};

NS_MA_END

#endif // LIBRARY_H
