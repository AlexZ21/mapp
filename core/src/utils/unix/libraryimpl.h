#ifndef LIBRARYIMPL_H
#define LIBRARYIMPL_H

#include "../../core_global.h"

#include <string>

NS_MA_BEGIN

class MACORESHARED_EXPORT LibraryImpl
{
public:
    LibraryImpl();
    ~LibraryImpl();

    bool load(const std::string &libPath);
    void *resolve(const std::string &funcPath);

private:
    void *m_handle;
};

NS_MA_END

#endif // LIBRARYIMPL_H
