#include "libraryimpl.h"
#include "../../log.h"

#include <dlfcn.h>

NS_MA_BEGIN

LibraryImpl::LibraryImpl() :
    m_handle(nullptr)
{

}

LibraryImpl::~LibraryImpl()
{
    if (m_handle)
        dlclose(m_handle);
}

bool LibraryImpl::load(const std::string &libPath)
{
    m_handle = dlopen(libPath.c_str(), RTLD_LAZY);
    if (!m_handle) {
        LERROR() << dlerror();
        return false;
    }
    return true;
}

void *LibraryImpl::resolve(const std::string &funcPath)
{
    if (!m_handle)
        return nullptr;
    return dlsym(m_handle, funcPath.c_str());
}

NS_MA_END
