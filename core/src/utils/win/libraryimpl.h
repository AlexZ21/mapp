#ifndef LIBRARYIMPL_H
#define LIBRARYIMPL_H

#include "../../core_global.h"

#include <string>
#include <windows.h>

NS_MA_BEGIN

class MACORESHARED_EXPORT LibraryImpl
{
public:
    LibraryImpl();
    ~LibraryImpl();

    bool load(const std::string &libPath);
    void *resolve(const std::string &funcPath);

private:
    HINSTANCE m_handle;
};

NS_MA_END

#endif // LIBRARYIMPL_H
