#ifndef FSIMPL_H
#define FSIMPL_H

#include "../../core_global.h"

#include <string>
#include <vector>

NS_MA_BEGIN

namespace fs {

    std::string currentPath();
    std::vector<std::string> filesList(const std::string &dirPath,
                                       const std::vector<std::string> &filters = {});

}

NS_MA_END

#endif // FSIMPL_H
