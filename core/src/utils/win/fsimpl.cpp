#include "fsimpl.h"
#include "../../log.h"

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

#include <regex>

NS_MA_BEGIN

namespace fs {

    std::string currentPath()
    {
        char *path = getcwd(nullptr, 0);
        if (path == nullptr)
            return std::string();
        std::string pathStr(path);
        free(path);
        return pathStr;
    }

    std::vector<std::string> filesList(const std::string &dirPath,
                                       const std::vector<std::string> &filters)
    {
        std::vector<std::string> files;

        DIR *dir = opendir(dirPath.c_str());
        if (!dir) {
            LERROR() << "Can't open directory <" << dirPath << ">";
            return files;
        }

        struct dirent *ent;
        while ((ent = readdir(dir)) != nullptr) {
            std::string fileName(ent->d_name);

            // Skip directories
            if (fileName == "." || fileName == "..")
                continue;

            bool skip = true;
            for (const std::string &filter : filters) {
                try {
                    if (std::regex_match(fileName, std::regex(filter))) {
                        skip = false;
                        break;
                    }
                } catch (const std::exception &e) {
                    LERROR() << e.what();
                }
            }
            if (skip)
                continue;

            files.push_back(fileName);
        }
        delete ent;

        closedir(dir);
        return files;
    }

}

NS_MA_END
