#include "libraryimpl.h"
#include "../../log.h"

NS_MA_BEGIN

LibraryImpl::LibraryImpl() :
    m_handle(nullptr)
{

}

LibraryImpl::~LibraryImpl()
{
    if (m_handle)
        FreeLibrary(m_handle);
}

bool LibraryImpl::load(const std::string &libPath)
{
    m_handle = LoadLibrary(libPath.c_str());
    if (!m_handle) {
        LERROR() << "Could not load the dynamic library <" << libPath << ">";
        return false;
    }
    return true;
}

void *LibraryImpl::resolve(const std::string &funcPath)
{
    if (!m_handle)
        return nullptr;
    return (void *)GetProcAddress(m_handle, funcPath.c_str());
}

NS_MA_END
