#ifndef CORE_H
#define CORE_H

#include "core_global.h"

#include <vector>
#include <memory>

NS_MA_BEGIN

class Library;
class Plugin;
class AudioPlayer;

class MACORESHARED_EXPORT Core
{
public:
    static Core &instance();
    static void init();

    AudioPlayer *audioPlayer() const;

private:
    Core();
    ~Core();

    void _init();

    void loadPlugins();
    void initPlugins();

private:
    std::vector<std::shared_ptr<Plugin>> m_plugins;
    std::vector<std::shared_ptr<Library>> m_pluginLibs;

    AudioPlayer *m_audioPlayer;

};

NS_MA_END

#endif // CORE_H
