#ifndef PLUGIN_H
#define PLUGIN_H

#include "core_global.h"

#if defined(_MSC_VER)

#if defined(MACORE_LIBRARY)
#  define PLUG_DECL     __declspec(dllexport)
#else
#  define PLUG_DECL     __declspec(dllexport)
#endif

#else

#if defined(MACORE_LIBRARY)
#  define PLUG_DECL     __attribute__((visibility("default")))
#else
#  define PLUG_DECL     __attribute__((visibility("default")))
#endif

#endif

#define MA_PLUGIN(_Class_, _name_, _description_, _version_) \
    extern "C" { \
        PLUG_DECL ma::Plugin *LoadPlugin() { \
            return new _Class_(); \
        } \
        PLUG_DECL const char *PluginName() { \
            return _name_; \
        } \
        PLUG_DECL const char *PluginDescription() { \
            return _description_; \
        } \
        PLUG_DECL const char *PluginVersion() { \
            return _version_; \
        } \
    };

NS_MA_BEGIN

class MACORESHARED_EXPORT Plugin
{
public:
    Plugin();

    virtual void init() = 0;

};

NS_MA_END

#endif // PLUGIN_H
