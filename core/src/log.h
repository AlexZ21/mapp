#ifndef UN_LOG_H
#define UN_LOG_H

#include "core_global.h"

#include <sstream>
#include <fstream>

#define LDEBUG ma::Log::debug
#define LINFO ma::Log::info
#define LWARNING ma::Log::warning
#define LERROR ma::Log::error
#define LFATAL ma::Log::fatal

NS_MA_BEGIN

class Log;

// Сообщение лога
class MACORESHARED_EXPORT LogMsg {
    friend class Log;
public:
    struct Stream {
        Stream() : ref(1) {}
        ~Stream() {}

        std::stringstream ss;
        std::string buffer;
        int ref;
    };

    LogMsg() : log(nullptr), stream(new Stream()) {}
    LogMsg(const LogMsg &logMsg) : log(logMsg.log), stream(logMsg.stream) { ++stream->ref; }
    virtual ~LogMsg();

protected:
    Log *log;
    Stream *stream;
};


class MACORESHARED_EXPORT LogTextMsg : public LogMsg {
public:
    enum Level {
        Debug = 0x0001,
        Info = 0x0002,
        Warning = 0x0004,
        Error = 0x0008,
        Fatal = 0x0010
    };

    LogTextMsg(Level l = Debug, const std::string &c = std::string()) : LogMsg(), context(c), level(l) {}
    ~LogTextMsg();

    LogTextMsg &operator<<(char ch) { stream->ss << ch; return *this; }
    LogTextMsg &operator<<(signed short i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(unsigned short i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(signed int i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(unsigned int i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(signed long i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(unsigned long i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(long long i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(unsigned long long i) { stream->ss << i; return *this; }
    LogTextMsg &operator<<(float f) { stream->ss << f; return *this; }
    LogTextMsg &operator<<(double f) { stream->ss << f; return *this; }
    LogTextMsg &operator<<(const std::string &s) { stream->ss << s; return *this; }
    LogTextMsg &operator<<(const char *c) { stream->ss << c; return *this; }
    LogTextMsg &operator<<(const void *ptr) { stream->ss << ptr; return *this; }

    LogTextMsg &print(char ch) { stream->ss << ch; return *this; }
    LogTextMsg &print(signed short i) { stream->ss << i; return *this; }
    LogTextMsg &print(unsigned short i) { stream->ss << i; return *this; }
    LogTextMsg &print(signed int i) { stream->ss << i; return *this; }
    LogTextMsg &print(unsigned int i) { stream->ss << i; return *this; }
    LogTextMsg &print(signed long i) { stream->ss << i; return *this; }
    LogTextMsg &print(unsigned long i) { stream->ss << i; return *this; }
    LogTextMsg &print(long long i) { stream->ss << i; return *this; }
    LogTextMsg &print(unsigned long long i) { stream->ss << i; return *this; }
    LogTextMsg &print(float f) { stream->ss << f; return *this; }
    LogTextMsg &print(double f) { stream->ss << f; return *this; }
    LogTextMsg &print(const std::string &s) { stream->ss << s; return *this; }
    LogTextMsg &print(const char *c) { stream->ss << c; return *this; }
    LogTextMsg &print(const void *ptr) { stream->ss << ptr; return *this; }

protected:
    std::string context;
    Level level;
};

// Дебаг
class MACORESHARED_EXPORT LogDebugMsg : public LogTextMsg {
public:
    LogDebugMsg(const std::string &c = std::string()) : LogTextMsg(Debug, c) {}
    ~LogDebugMsg() {}
};

// Информация
class MACORESHARED_EXPORT LogInfoMsg : public LogTextMsg {
public:
    LogInfoMsg(const std::string &c = std::string()) : LogTextMsg(Info, c) {}
    ~LogInfoMsg() {}
};

// Предупреждение
class MACORESHARED_EXPORT LogWarningMsg : public LogTextMsg {
public:
    LogWarningMsg(const std::string &c = std::string()) : LogTextMsg(Warning, c) {}
    ~LogWarningMsg() {}
};

// Ошибка
class MACORESHARED_EXPORT LogErrorMsg : public LogTextMsg {
public:
    LogErrorMsg(const std::string &c = std::string()) : LogTextMsg(Error, c) {}
    ~LogErrorMsg() {}
};

// Фатальная ошибка
class MACORESHARED_EXPORT LogFatalMsg : public LogTextMsg {
public:
    LogFatalMsg(const std::string &c = std::string()) : LogTextMsg(Fatal, c) {}
    ~LogFatalMsg() {}
};

// Класс для логирования
class MACORESHARED_EXPORT Log
{
    friend class LogMsg;

public:
    static Log &instance();
    static LogDebugMsg debug(const std::string &c = std::string());
    static LogInfoMsg info(const std::string &c = std::string());
    static LogWarningMsg warning(const std::string &c = std::string());
    static LogErrorMsg error(const std::string &c = std::string());
    static LogFatalMsg fatal(const std::string &c = std::string());

    static void setOutputFile(const std::string &fileName);

private:
    Log();
    ~Log();

    Log(Log const&) = delete;
    Log &operator=(Log const&) = delete;

    void printMessage(const std::string &msg);

private:
    std::ofstream m_fileStream;
};

NS_MA_END

#endif // UN_LOG_H
