#include "button.h"
#include "style.h"
#include "animations.h"
#include "gui_utils.h"

#include <QAction>
#include <QPainter>
#include <QMouseEvent>
#include <QFontMetrics>

#include <QDebug>

USING_MA_NS

Button::Button(QWidget *parent) :
    QWidget(parent),
    m_action(new QAction(this))
{
    init(QString());
}

Button::Button(const QString &text, QWidget *parent) :
    QWidget(parent),
    m_action(new QAction(this))
{
    init(text);
}

Button::~Button()
{
    delete m_enteredAnimation;
}

QAction *Button::action() const
{
    return m_action;
}

void Button::setAction(QAction *action)
{
    m_action = action;
    updateDrawElements();
    repaint();
}

void Button::setText(const QString &text)
{
    m_action->setText(text);
    updateDrawElements();
    repaint();
}

void Button::setIcon(const QIcon &icon)
{
    m_action->setIcon(icon);
    updateDrawElements();
    repaint();
}

void Button::setIconColor(const QColor &color)
{
    m_iconColor = color;
    updateDrawElements();
    repaint();
}

Button::Alignment Button::alignment() const
{
    return m_alignment;
}

void Button::setAlignment(const Button::Alignment &alignment)
{
    m_alignment = alignment;
    updateDrawElements();
    repaint();
}

Button::Shape Button::shape() const
{
    return m_shape;
}

void Button::setShape(const Shape &shape)
{
    m_shape = shape;
    updateDrawElements();
    repaint();
}

int Button::padding() const
{
    return m_padding;
}

void Button::setPadding(int padding)
{
    m_padding = padding;
    updateDrawElements();
    repaint();
}

QSize Button::sizeHint() const
{
    QFontMetrics fm(font());
    int fHeight = fm.height();
    int cWidth = fm.width(m_action->text());
    if (!m_action->icon().isNull()){
        int iconSize = 0;
        if (height() >= width()) {
            iconSize = width() - m_padding * 2;
        } else {
            iconSize = height() - m_padding * 2;
        }
        cWidth += iconSize;
    }
    return QSize(m_padding * 2 + cWidth, m_padding * 2 + fHeight);
}

QSize Button::minimumSizeHint() const
{
    QFontMetrics fm(font());
    int fHeight = fm.height();
    int cWidth = fm.width(m_action->text());
    if (!m_action->icon().isNull()){
        int iconSize = 0;
        if (height() >= width()) {
            iconSize = width() - m_padding * 2;
        } else {
            iconSize = height() - m_padding * 2;
        }
        cWidth += iconSize;
    }
    return QSize(m_padding * 2 + cWidth, m_padding * 2 + fHeight);
}

void Button::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);

    QPainterPath bgPath;
    int bgCornerRadius = m_padding;
    QRect bgRect = rect();
    int shapeWidth = width();

    if (m_shape == Circle) {
        if (height() >= width()) {
            bgCornerRadius = width() / 2;
        } else {
            bgCornerRadius = height() / 2;
        }
        bgRect.setX(width() / 2 - bgCornerRadius);
        bgRect.setY(height() / 2 - bgCornerRadius);
        bgRect.setWidth(bgCornerRadius * 2);
        bgRect.setHeight(bgCornerRadius * 2);
        shapeWidth = bgCornerRadius * 2;
    }
    if (m_shape == Rect) {
        bgCornerRadius = 0;
    }

    bgPath.addRoundedRect(bgRect, bgCornerRadius, bgCornerRadius);

    p.setClipPath(bgPath);

    p.fillRect(rect(), QColor(0, 0, 0, m_enetredBgOpacity));

    if (m_pressedRadius > 0) {
        p.setPen(Qt::transparent);
        p.setBrush(QColor(0, 0, 0, m_enetredBgOpacity * 1.5));
        p.drawEllipse(m_pressPoint, m_pressedRadius, m_pressedRadius);
    }

    p.setPen(QColor(Style::get("button_text_color").toString()));
    p.setFont(font());

    int iconSize = 0;
    if (height() >= width()) {
        iconSize = width() - m_padding * 2.4;
    } else {
        iconSize = height() - m_padding * 2.4;
    }

    if (m_action->isCheckable()) {
        if (m_action->isChecked())
            p.setOpacity(1);
        else
            p.setOpacity(0.5);
    }

    if (!m_action->icon().isNull())
        p.drawPixmap(m_iconDrawRect, m_icon);

    if (!m_action->text().isEmpty())
        p.drawText(m_textDrawRect, Qt::AlignVCenter | Qt::AlignLeft, m_action->text());
}

void Button::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_pressed = true;
        m_pressPoint = event->pos();
        m_pressedAnimation->start(0, qMax(width(), height()), 500,
                                  [this](float v){
            m_pressedRadius = v;
            repaint();
        }, [this](float v){
        }, TransitionFuncs::EaseOutCubic);
    }
    setFocus();
}

void Button::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        emit clicked();
        m_pressed = false;
        m_pressedAnimation->stop();
        m_pressedRadius = 0;
        repaint();
    }
}

void Button::enterEvent(QEvent *event)
{
    Q_UNUSED(event);
    m_entered = true;

    m_enteredAnimation->stop();
    m_enteredAnimation->start(0, 10, 600,
                              [this](float v){
        m_enetredBgOpacity = v;
        repaint();
    }, [this](float v){
    }, TransitionFuncs::EaseOutCubic);

}

void Button::leaveEvent(QEvent *event)
{
    Q_UNUSED(event);
    m_entered = false;

    m_enteredAnimation->stop();
    m_enteredAnimation->start(10, 0, 600,
                              [this](float v){
        m_enetredBgOpacity = v;
        repaint();
    }, [this](float v){
    }, TransitionFuncs::EaseOutCubic);
}

void Button::resizeEvent(QResizeEvent *)
{
    updateDrawElements();
}

void Button::init(const QString &text)
{
    m_action->setText(text);

    connect(this, &Button::clicked, [this](){
        m_action->trigger();
    });

    m_shape = RoundRect;
    m_alignment = Center;
    m_entered = false;
    m_enetredBgOpacity = 0;
    m_enteredAnimation = new Animation();
    m_pressed = false;
    m_pressedRadius = 0;
    m_pressedAnimation = new Animation();
    m_padding = Style::get("button_padding").toInt();

    QFont f = font();
    f.setBold(true);
    f.setPixelSize(12);
    setFont(f);

    setMouseTracking(true);
}

void Button::updateDrawElements()
{
    int iconSize = 16;//qMin(width(), height()) - m_padding * 2.4;

    QFontMetrics fm(font());
    int textWidth = fm.width(m_action->text());

    int contentWidth = iconSize + m_padding + textWidth;

    if (!m_action->icon().isNull()) {
        m_icon = m_action->icon().pixmap(iconSize, iconSize);
        if (m_iconColor.isValid())
            setPixmapColor(m_icon, m_iconColor);

        if (m_alignment == Center) {
            m_iconDrawRect = QRect(((width() - (textWidth == 0 ? iconSize : contentWidth)) / 2),
                                   (height() - iconSize) / 2,
                                   iconSize, iconSize);

        } else if (m_alignment == Left) {
            m_iconDrawRect = QRect(m_padding,
                                   (height() - iconSize) / 2,
                                   iconSize, iconSize);
        }

    } else {
        m_iconDrawRect = QRect();
    }

    int textRectX = m_iconDrawRect.x() + m_iconDrawRect.width() + m_padding;
    m_textDrawRect = QRect(textRectX, 0,
                           width() - (textRectX + m_padding), height());

}

bool Button::isCheckable() const
{
    return m_action->isCheckable();
}

void Button::setCheckable(bool checkable)
{
    m_action->setCheckable(checkable);
}

bool Button::isChecked() const
{
    return m_action->isChecked();
}



