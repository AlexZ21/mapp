#ifndef GUI_UTILS_H
#define GUI_UTILS_H

#include "gui_global.h"
#include <core_global.h>

#include <QImage>
#include <QPixmap>
#include <QColor>

class QWidget;

NS_MA_BEGIN

QWidget *createHorizontalSpacer(int width);

void setImageColor(QImage &img, const QColor &color);
void setPixmapColor(QPixmap &img, const QColor &color);

NS_MA_END

#endif // GUI_UTILS_H
