#ifndef LINEEDIT_H
#define LINEEDIT_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>
#include <QLineEdit>

NS_MA_BEGIN

class ILineEdit;
class Animation;

class LineEdit : public QWidget
{
    friend class ILineEdit;
    Q_OBJECT
public:
    enum Background {
        Flat,
        Fill
    };

    enum Border {
        AllSide,
        OnlyBottom
    };

    explicit LineEdit(QWidget *parent = 0);
    ~LineEdit();

    QSize sizeHint() const;
    QSize minimumSizeHint() const;

    Background background() const;
    void setBackground(const Background &background);

    QString label() const;
    void setLabel(const QString &label);

protected:
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    void setFocus(bool state);

private:
    class ILineEdit : public QLineEdit {
    public:
        explicit ILineEdit(LineEdit *parent = 0);
    protected:
        void focusInEvent(QFocusEvent *event);
        void focusOutEvent(QFocusEvent *event);
    private:
        LineEdit *parentLineEdit;
    };

    ILineEdit *m_lineEdit;
    Background m_background;
    Border m_border;
    int m_borderOpacity;
    int m_borderWidth;
    Animation *m_borderAnimation;
    QString m_label;

};

NS_MA_END

#endif // LINEEDIT_H
