#include "audioplayercontrols.h"
#include "button.h"
#include "style.h"
#include "imagecontainer.h"
#include "gui_utils.h"
#include "animations.h"
#include "progressicon.h"

#include <core.h>
#include <audio/audioplayer.h>
#include <audio/audiotrack.h>
#include <log.h>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPainter>
#include <QLabel>
#include <QMouseEvent>
#include <QFontMetrics>
#include <QAction>

USING_MA_NS

AudioPlayerProgress::AudioPlayerProgress(QWidget *parent) :
    QWidget(parent),
    m_duration(0),
    m_currentDuration(0),
    m_pressed(false),
    m_sliderHgOpacity(0),
    m_moveAnimation(new Animation()),
    m_enteredAnimation(new Animation())
{
    setMouseTracking(true);

    m_durationUpdate.setInterval(30);

    connect(&m_durationUpdate, &QTimer::timeout,
            [this](){
        if (!m_pressed) {
            m_currentDuration = Core::instance().audioPlayer()->playingOffset();
            repaint();
        }
//            setCurrentDurationHelper(Core::instance().audioPlayer()->playingOffset());
    });

    connect(this, &AudioPlayerProgress::durationUpdateStarted, this,
            [this](){
        m_durationUpdate.start();
    }, Qt::QueuedConnection);

    connect(this, &AudioPlayerProgress::durationUpdateStopped, this,
            [this](){
        m_durationUpdate.stop();
    }, Qt::QueuedConnection);

    QIcon infIcon(":/res/icons/infinite.svg");
    m_infPixmap = infIcon.pixmap(14, 14);
}

AudioPlayerProgress::~AudioPlayerProgress()
{
    delete m_moveAnimation;
    delete m_enteredAnimation;
}

void AudioPlayerProgress::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);

    // Background
    drawLine(&p, width(), QColor(0, 0, 0, 10));

    // Current duration
    if (m_duration != std::chrono::milliseconds(std::numeric_limits<int64_t>::max())) {
        QColor baseDurationLineColor("#BDBDBD");
        baseDurationLineColor.setAlpha(255 - m_sliderHgOpacity);
        drawLine(&p, durationToWidth(m_currentDuration), baseDurationLineColor);

        QColor hgDurationLineColor(Style::get("hg_color").toString());
        hgDurationLineColor.setAlpha(m_sliderHgOpacity);
        drawLine(&p, durationToWidth(m_currentDuration), hgDurationLineColor);
    }

    p.setPen(QColor(Style::get("text_color").toString()));

    // Current time
    if (m_duration != std::chrono::milliseconds(std::numeric_limits<int64_t>::max())) {
        drawTimeBlock(&p, m_currentDuration, QPoint(durationToWidth(m_currentDuration), height() - 4),
                      [this](int blockWidth, QPoint &point) -> bool {
            if (durationToWidth(m_currentDuration) < blockWidth)
                point.setX(blockWidth);
            return true;
        });
    } else {
        drawTimeBlock(&p, m_currentDuration, QPoint(0, height() - 4),
                      [this](int blockWidth, QPoint &point) -> bool {
            point.setX(blockWidth);
            return true;
        });
    }

    // Track time
    if (m_duration != std::chrono::milliseconds(std::numeric_limits<int64_t>::max())) {
        drawTimeBlock(&p, m_duration, QPoint(width(), height() - 4),
                      [this](int blockWidth, QPoint &) -> bool {
            return durationToWidth(m_currentDuration) < (width() - blockWidth);
        });
    } else {
        // Infinity icon
        setPixmapColor(m_infPixmap, QColor(Style::get("text_color").toString()).light(180));
        p.drawPixmap(QRect(width() - m_infPixmap.width(), - 3,
                           m_infPixmap.width(), m_infPixmap.height()), m_infPixmap);
    }

}

void AudioPlayerProgress::mousePressEvent(QMouseEvent *event)
{
    if (m_duration != std::chrono::milliseconds(std::numeric_limits<int64_t>::max())) {
        m_pressed = true;
        setCurrentDurationHelper(widthToDuration(event->pos().x()));
        repaint();
    }
}

void AudioPlayerProgress::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_pressed) {
        m_pressed = false;
        setCurrentDurationHelper(widthToDuration(event->pos().x()));
        emit sliderMoved(widthToDuration(event->pos().x()));
    }
}

void AudioPlayerProgress::mouseMoveEvent(QMouseEvent *event)
{
    if (m_pressed) {
        setCurrentDurationHelper(widthToDuration(event->pos().x()));
        repaint();
    }
}

void AudioPlayerProgress::enterEvent(QEvent *event)
{
    Q_UNUSED(event);
    m_enteredAnimation->start(0, 255, 300,
                              [this](float v){
        m_sliderHgOpacity = v;
        repaint();
    }, [this](float v){
    }, TransitionFuncs::EaseOutCubic);
}

void AudioPlayerProgress::leaveEvent(QEvent *event)
{
    Q_UNUSED(event);
    m_enteredAnimation->start(255, 0, 300,
                              [this](float v){
        m_sliderHgOpacity = v;
        repaint();
    }, [this](float v){
    }, TransitionFuncs::EaseOutCubic);
}

void AudioPlayerProgress::drawLine(QPainter *p, int width, const QColor &color)
{
    QPainterPath bgLinePath;
    QRect bgLineRect(0, height() - 2, width, 2);
    bgLinePath.addRoundedRect(bgLineRect, 2, 2);
    p->fillPath(bgLinePath, color);
}

void AudioPlayerProgress::drawTimeBlock(QPainter *p,
                                        const std::chrono::milliseconds &duration,
                                        QPoint point, const std::function<bool (int, QPoint &)> &hideIf)
{
    int64_t m = std::chrono::duration_cast<std::chrono::minutes>(duration).count();
    int64_t s = std::chrono::duration_cast<std::chrono::seconds>(duration).count() % 60;

    QFontMetrics fm(font());
    QString timeStr = QString("%1:%2").arg(m).arg(s < 10 ? ("0" + QString::number(s)) : QString::number(s));
    int timeStrWidth = fm.width(timeStr);

    if (hideIf && !hideIf(timeStrWidth + 4, point))
        return;

    QPainterPath bgPath;
    QRect bgRect((point.x() - (4 + timeStrWidth)),
                 (point.y() - fm.height()),
                 (4 + timeStrWidth), fm.height());
    bgPath.addRoundedRect(bgRect, 2, 2);
    p->fillPath(bgPath, QColor(0, 0, 0, 10));
    p->drawText(bgRect, Qt::AlignCenter, timeStr);
}

int AudioPlayerProgress::durationToWidth(const std::chrono::milliseconds &duration)
{
    return width() * ((double) duration.count() / m_duration.count());
}

std::chrono::milliseconds AudioPlayerProgress::widthToDuration(int width)
{
    return std::chrono::milliseconds((int64_t)(m_duration.count() * ((float) width / this->width())));
}

void AudioPlayerProgress::setCurrentDurationHelper(const std::chrono::milliseconds &duration)
{
    if (duration.count() > m_duration.count() || duration.count() < 0)
        return;
    if (m_moveAnimation->isRunning())
        m_moveAnimation->stop();
    int ms = 100;
    auto f = TransitionFuncs::Linear;
    m_moveAnimation->start(m_currentDuration.count(), duration.count(), ms,
                           [this](float v){
        m_currentDuration = std::chrono::milliseconds((int64_t)v);
        repaint();
    }, [this](float v){
    }, f);
}

void AudioPlayerProgress::startDurationUpdate()
{
    emit durationUpdateStarted();
}

void AudioPlayerProgress::stopDurationUpdate()
{
    emit durationUpdateStopped();
}

std::chrono::milliseconds AudioPlayerProgress::currentDuration() const
{
    return m_currentDuration;
}

void AudioPlayerProgress::setCurrentDuration(const std::chrono::milliseconds &currentDuration)
{
    if (!m_pressed)
        setCurrentDurationHelper(std::chrono::milliseconds(currentDuration));
}

std::chrono::milliseconds AudioPlayerProgress::duration() const
{
    return m_duration;
}

void AudioPlayerProgress::setDuration(const std::chrono::milliseconds &duration)
{
    m_duration = duration;
}

AudioPlayerControls::AudioPlayerControls(QWidget *parent) :
    QWidget(parent),
    m_audioPlayerProgress(new AudioPlayerProgress(this)),
    m_prevAction(new QAction(this)),
    m_playPauseAction(new QAction(this)),
    m_nextAction(new QAction(this)),
    m_playlistAction(new QAction(this)),
    m_coverImg(new ImageContainer(this)),
    m_titleLabel(new QLabel(this)),
    m_artistLabel(new QLabel(this)),
    m_shuffleAction(new QAction(this)),
    m_repeatAction(new QAction(this)),
    m_bufferingIcon(new ProgressIcon(":/res/icons/spinner.svg", this))
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(6);
    setLayout(mainLayout);

    QHBoxLayout *progressLayout = new QHBoxLayout();
    progressLayout->setContentsMargins(10, 0, 10, 0);
    progressLayout->setSpacing(0);
    mainLayout->addLayout(progressLayout);

    QFont appf = font();
    appf.setPixelSize(10);
    m_audioPlayerProgress->setFont(appf);

    connect(m_audioPlayerProgress, &AudioPlayerProgress::sliderMoved,
            [this](const std::chrono::milliseconds &duration){
        Core::instance().audioPlayer()->seek(duration);
    });

    progressLayout->addWidget(m_audioPlayerProgress);

    QHBoxLayout *controlsLayout = new QHBoxLayout();
    controlsLayout->setMargin(0);
    controlsLayout->setSpacing(0);
    mainLayout->addLayout(controlsLayout);

    Button *prevBtn = new Button(this);
    prevBtn->setAction(m_prevAction);
    prevBtn->setIcon(QIcon(":/res/icons/prev.svg"));
    prevBtn->setShape(Button::Circle);
    prevBtn->setIconColor(QColor(Style::get("text_color").toString()));
    prevBtn->setFixedSize(Style::get("apc_controls_height").toInt(),
                          Style::get("apc_controls_height").toInt());

    connect(m_prevAction, &QAction::triggered,
            [this](){
        reset();
        Core::instance().audioPlayer()->prev();
    });

    controlsLayout->addWidget(prevBtn, 0, Qt::AlignLeft);

    Button *playPauseBtn = new Button(this);
    playPauseBtn->setAction(m_playPauseAction);
    playPauseBtn->setShape(Button::Circle);
    playPauseBtn->setIconColor(QColor(Style::get("text_color").toString()));
    playPauseBtn->setFixedSize(Style::get("apc_controls_height").toInt(),
                               Style::get("apc_controls_height").toInt());

    m_playPauseAction->setIcon(QIcon(":/res/icons/play.svg"));
    connect(m_playPauseAction, &QAction::triggered,
            [this](){
        if (Core::instance().audioPlayer()->status() != AudioPlayer::Playing)
            Core::instance().audioPlayer()->play();
        else
            Core::instance().audioPlayer()->pause();
    });

    connect(this, &AudioPlayerControls::audioPlayerStatusChanged, this,
            [this, playPauseBtn](int status){
        if ((AudioPlayer::Status)status != AudioPlayer::Playing) {
            playPauseBtn->setIcon(QIcon(":/res/icons/play.svg"));
            m_audioPlayerProgress->stopDurationUpdate();
        } else {
            playPauseBtn->setIcon(QIcon(":/res/icons/pause.svg"));
            m_audioPlayerProgress->startDurationUpdate();
        }
    }, Qt::QueuedConnection);

    Core::instance().audioPlayer()->statusChanged.connect([this](AudioPlayer::Status status){
        emit audioPlayerStatusChanged((int)status);
    });

    controlsLayout->addWidget(playPauseBtn, 0, Qt::AlignLeft);

    Button *nextBtn = new Button(this);
    nextBtn->setAction(m_nextAction);
    nextBtn->setIcon(QIcon(":/res/icons/next.svg"));
    nextBtn->setShape(Button::Circle);
    nextBtn->setIconColor(QColor(Style::get("text_color").toString()));
    nextBtn->setFixedSize(Style::get("apc_controls_height").toInt(),
                          Style::get("apc_controls_height").toInt());

    connect(m_nextAction, &QAction::triggered,
            [this](){
        reset();
        Core::instance().audioPlayer()->next();
    });

    controlsLayout->addWidget(nextBtn, 0, Qt::AlignLeft);

    Button *playlistBtn = new Button(this);
    playlistBtn->setAction(m_playlistAction);
    playlistBtn->setIcon(QIcon(":/res/icons/list.svg"));
    playlistBtn->setShape(Button::Circle);
    playlistBtn->setIconColor(QColor(Style::get("text_color").toString()));
    playlistBtn->setFixedSize(Style::get("apc_controls_height").toInt(),
                              Style::get("apc_controls_height").toInt());
    controlsLayout->addWidget(playlistBtn, 0, Qt::AlignLeft);

    controlsLayout->addWidget(createHorizontalSpacer(20), 0);

    m_coverImg->fromResource(":/res/icons/cover.png");
    m_coverImg->setFixedSize(40, 40);
    controlsLayout->addWidget(m_coverImg, 0, Qt::AlignLeft);

    controlsLayout->addWidget(createHorizontalSpacer(20), 0);

    QVBoxLayout *songMetadataLayout = new QVBoxLayout();
    songMetadataLayout->setContentsMargins(0, 6, 0, 6);

    QFont f = font();
    f.setBold(true);
    f.setPixelSize(12);

    QPalette titlePalette = m_titleLabel->palette();
    titlePalette.setColor(m_titleLabel->foregroundRole(), QColor(Style::get("text_color").toString()));

    m_titleLabel->setFont(f);
    m_titleLabel->setPalette(titlePalette);
    m_titleLabel->setText("Song title");

    songMetadataLayout->addWidget(m_titleLabel);

    f.setBold(false);
    m_artistLabel->setFont(f);
    m_artistLabel->setPalette(titlePalette);
    m_artistLabel->setText("song artist");

    songMetadataLayout->addWidget(m_artistLabel);

    controlsLayout->addLayout(songMetadataLayout, 1);

    // Buffering icon
    m_bufferingIcon->setFixedSize(Style::get("apc_controls_height").toInt(),
                                  Style::get("apc_controls_height").toInt());
    m_bufferingIcon->setIconColor(QColor(Style::get("text_color").toString()));
    m_bufferingIcon->hide();

    connect(this, &AudioPlayerControls::audioPlayerStartBuffering, this,
            [this](){
        m_bufferingIcon->show();
        m_bufferingIcon->start();
    }, Qt::QueuedConnection);

    Core::instance().audioPlayer()->
            startBuffering.connect([this](){
        emit audioPlayerStartBuffering();
    });

    connect(this, &AudioPlayerControls::audioPlayerEndBuffering, this,
            [this](){
        m_bufferingIcon->hide();
        m_bufferingIcon->stop();
    }, Qt::QueuedConnection);

    Core::instance().audioPlayer()->
            endBuffering.connect([this](){
        emit audioPlayerEndBuffering();
    });

    controlsLayout->addWidget(m_bufferingIcon, 0, Qt::AlignLeft);

    Button *repeatBtn = new Button(this);
    repeatBtn->setAction(m_repeatAction);
    repeatBtn->setCheckable(true);
    repeatBtn->setIcon(QIcon(":/res/icons/repeat.svg"));
    repeatBtn->setShape(Button::Circle);
    repeatBtn->setIconColor(QColor(Style::get("text_color").toString()));
    repeatBtn->setFixedSize(Style::get("apc_controls_height").toInt(),
                            Style::get("apc_controls_height").toInt());

    connect(m_repeatAction, &QAction::triggered,
            [this](){
        Core::instance().audioPlayer()->setRepeat(!Core::instance().audioPlayer()->isRepeat());
    });

    controlsLayout->addWidget(repeatBtn, 0, Qt::AlignLeft);

    Button *shuffleBtn = new Button(this);
    shuffleBtn->setAction(m_shuffleAction);
    shuffleBtn->setCheckable(true);
    shuffleBtn->setIcon(QIcon(":/res/icons/shuffle.svg"));
    shuffleBtn->setShape(Button::Circle);
    shuffleBtn->setIconColor(QColor(Style::get("text_color").toString()));
    shuffleBtn->setFixedSize(Style::get("apc_controls_height").toInt(),
                             Style::get("apc_controls_height").toInt());

    connect(m_shuffleAction, &QAction::triggered,
            [this](){
        Core::instance().audioPlayer()->setShuffle(!Core::instance().audioPlayer()->isShuffle());
    });

    controlsLayout->addWidget(shuffleBtn, 0, Qt::AlignLeft);

    Core::instance().audioPlayer()->
            metadataChanged.connect([this](const AudioTrack &audioTrack){
        m_audioPlayerProgress->setDuration(audioTrack.duration());
        m_titleLabel->setText(audioTrack.title().data());
        m_artistLabel->setText(audioTrack.artist().data());
    });

    setMouseTracking(true);
}

AudioPlayerControls::~AudioPlayerControls()
{
    Core::instance().audioPlayer()->statusChanged.disconnect(this);
}

void AudioPlayerControls::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.fillRect(rect(), Qt::white);
}

void AudioPlayerControls::reset()
{
    m_audioPlayerProgress->setCurrentDuration(std::chrono::milliseconds(0));
    m_audioPlayerProgress->setDuration(std::chrono::milliseconds(0));
    m_titleLabel->setText("Song title");
    m_artistLabel->setText("Song artist");
}


