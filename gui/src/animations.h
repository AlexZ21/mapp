#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#include "gui_global.h"
#include <core_global.h>

#include <QObject>
#include <QTimer>
#include <QSet>

#include <functional>
#include <math.h>

#if defined(_MSC_VER)
#define M_PI 3.14
#endif

NS_MA_BEGIN

using TransitionFunc = std::function<float (float delta, float dt)>;

class TransitionFuncs
{
public:
    static float Linear(const float &delta, const float &dt) {
        return delta * dt;
    }

    static float SineInOut(const float &delta, const float &dt) {
        return -(delta / 2) * (cos(M_PI * dt) - 1);
    }

    static float HalfSine(const float &delta, const float &dt) {
        return delta * sin(M_PI * dt / 2);
    }

    static float EaseOutBack(const float &delta, const float &dt) {
        static constexpr auto s = 1.70158;
        const float t = dt - 1;
        return delta * (t * t * ((s + 1) * t + s) + 1);
    }

    static float EaseInCirc(const float &delta, const float &dt) {
        return -delta * (sqrt(1 - dt * dt) - 1);
    }

    static float EaseOutCirc(const float &delta, const float &dt) {
        const float t = dt - 1;
        return delta * sqrt(1 - t * t);
    }

    static float EaseInCubic(const float &delta, const float &dt) {
        return delta * dt * dt * dt;
    }

    static float EaseOutCubic(const float &delta, const float &dt) {
        const float t = dt - 1;
        return delta * (t * t * t + 1);
    }

    static float EaseInQuint(const float &delta, const float &dt) {
        const float t2 = dt * dt;
        return delta * t2 * t2 * dt;
    }

    static float EaseOutQuint(const float &delta, const float &dt) {
        const float t = dt - 1, t2 = t * t;
        return delta * (t2 * t2 * t + 1);
    }
};

class Animations;

class AnimationValue
{
public:
    AnimationValue() = default;
    AnimationValue(float from) : m_cur(from), m_from(from) { }
    AnimationValue(float from, float to) : m_cur(from), m_from(from), m_delta(to - from) { }

    void start(float to) {
        m_from = m_cur;
        m_delta = to - m_from;
    }

    void restart() {
        m_delta = m_from + m_delta - m_cur;
        m_from = m_cur;
    }

    float from() const {
        return m_from;
    }

    float current() const {
        return m_cur;
    }

    float to() const {
        return m_from + m_delta;
    }

    void add(float delta) {
        m_from += delta;
        m_cur += delta;
    }

    AnimationValue &update(float dt, TransitionFunc func) {
        m_cur = m_from + func(m_delta, dt);
        return *this;
    }

    void finish() {
        m_cur = m_from + m_delta;
        m_from = m_cur;
        m_delta = 0;
    }

private:
    float m_cur;
    float m_from;
    float m_delta;

};

class MAGUISHARED_EXPORT Animation
{
    friend class Animations;
public:
    Animation();

    void setRepeat(bool repeat);

    void start(float from, float to, qint64 duration,
               std::function<void (float)> stepCallback,
               std::function<void (float)> finishCallback,
               TransitionFunc trasition = TransitionFuncs::Linear);
    void stop();
    bool isRunning() const;

    void finish();

protected:
    void step(qint64 ms);

private:
    bool m_running;
    qint64 m_duration;
    qint64 m_fduration;
    bool m_repeat;
    AnimationValue m_value;
    std::function<void (float)> m_stepCallback;
    std::function<void (float)> m_finishCallback;
    TransitionFunc m_trasition;

};

class MAGUISHARED_EXPORT Animations : public QObject
{
    Q_OBJECT
public:
    static Animations &instance();

    void start(Animation *animation);
    void stop(Animation *animation);

private:
    Animations();

private:
    QTimer m_timer;
    QSet<Animation *> m_animations;
    QSet<Animation *> m_sanimations;
    QSet<Animation *> m_fanimations;
    qint64 m_lastMs;
    qint64 m_currentMs;
    bool m_updating;
};

NS_MA_END

#endif // ANIMATIONS_H
