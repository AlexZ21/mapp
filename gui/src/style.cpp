#include "style.h"


USING_MA_NS

Style &Style::instance()
{
    static Style inst;
    return inst;
}

QVariant Style::get(const QString &key)
{
    return instance().m_values.value(key);
}

float Style::pr()
{

    return 1;
}

Style::Style()
{
    initDefaultValues();
}

Style::~Style()
{
}

void Style::initDefaultValues()
{
    // For all
    m_values["bg_color"] = "#FFFFFF";
    m_values["hg_color"] = "#F64747";

    m_values["text_color"] = "#424242";
    m_values["sub_text_color"] = "#616161";

    m_values["corner_radius"] = 5;
    m_values["sm_corner_radius"] = 3;

    // Buttons
    m_values["button_padding"] = 8;
    m_values["button_text_color"] = "#6C7A89";

    // Toolbar
    m_values["toolbar_height"] = 48;
    m_values["toolbar_padding"] = 6;
    m_values["toolbar_spacing"] = 6;
    m_values["toolbar_controls_height"] = 36;

    // LineEdit
    m_values["line_edit_bg_color"] = "#ECECEC";
    m_values["line_edit_border_color"] = "#DADFE1";
    m_values["line_edit_border_radius"] = 3;
    m_values["line_edit_border_width"] = 2;
    m_values["line_edit_hg_color"] = m_values["hg_color"];
    m_values["line_edit_font_color"] = "#6C7A89";
    m_values["line_edit_font_size"] = 12;
    m_values["line_edit_font_bold"] = false;
    m_values["line_edit_font_italic"] = false;
    m_values["line_edit_placeholder_font_color"] = "#ABB7B7";
    m_values["line_edit_placeholder_font_size"] = 12;
    m_values["line_edit_placeholder_font_bold"] = false;
    m_values["line_edit_placeholder_font_italic"] = false;

    // AudioPlayerControls
    m_values["apc_height"] = 76;
    m_values["apc_controls_height"] = 36;
}
