#include "scrollbar.h"
#include "animations.h"

#include <QPainter>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QDebug>

NS_MA_BEGIN

ScrollBar::ScrollBar(Qt::Orientation orientation, QWidget *parent) :
    QWidget(parent),
    m_value(0),
    m_maximum(100),
    m_pageStep(20),
    m_singleStep(1),
    m_orientation(orientation),
    m_opacity(0),
    m_opacityAnimation(new Animation()),
    m_sliderPosition(0),
    m_pressed(false),
    m_pressedPosition(0)
{
    setMouseTracking(true);
}

ScrollBar::~ScrollBar()
{
    delete m_opacityAnimation;
}

int ScrollBar::maximum() const
{
    return m_maximum;
}

void ScrollBar::setMaximum(int maxValue)
{
    m_maximum = maxValue;
}

int ScrollBar::value() const
{
    return m_value;
}

void ScrollBar::setValue(int value)
{
    m_value = value;
    if (m_value < 0)
        m_value = 0;
    else if (m_value > m_maximum - m_pageStep)
        m_value = m_maximum - m_pageStep;
    m_sliderPosition = ((float)m_value/m_maximum) * lineWidth();
    repaint();
}

int ScrollBar::pageStep() const
{
    return m_pageStep;
}

void ScrollBar::setPageStep(int sliderWidth)
{
    m_pageStep = sliderWidth;
    updateSliderSize();

}

int ScrollBar::singleStep() const
{
    return m_singleStep;
}

void ScrollBar::setSingleStep(int singleStep)
{
    m_singleStep = singleStep;
}

void ScrollBar::fadeIn()
{
    show();
    m_opacityAnimation->start(m_opacity, 1, 500,
                              [this](float v){
        m_opacity = v;
        repaint();
    }, [this](float v){
    }, TransitionFuncs::EaseOutCubic);
}

void ScrollBar::fadeOut()
{
    m_opacityAnimation->start(m_opacity, 0, 500,
                              [this](float v){
        m_opacity = v;
        repaint();
    }, [this](float v){
        hide();
    }, TransitionFuncs::EaseOutCubic);
}

void ScrollBar::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.setOpacity(m_opacity);

    if (m_orientation == Qt::Vertical) {
        QPainterPath bgShape;
        bgShape.addRoundedRect(QRect(2, 2, 4, height() - 4), 2, 2);
        p.fillPath(bgShape, QColor(0, 0, 0, 20));

        QPainterPath sliderShape;
        sliderShape.addRoundedRect(QRect(2, m_sliderPosition + 2, 4, m_sliderSize - 4), 2, 2);
        p.fillPath(sliderShape, QColor(0, 0, 0, 80));

    } else {
        QPainterPath bgShape;
        bgShape.addRoundedRect(QRect(2, 2, width() - 4, 4), 2, 2);
        p.fillPath(bgShape, QColor(0, 0, 0, 20));

        QPainterPath sliderShape;
        sliderShape.addRoundedRect(QRect(m_sliderPosition + 2, 2, m_sliderSize - 4, 4), 2, 2);
        p.fillPath(sliderShape, QColor(0, 0, 0, 80));
    }

}

void ScrollBar::mousePressEvent(QMouseEvent *event)
{
    if (m_orientation == Qt::Vertical) {
        if (event->y() > m_sliderPosition && event->y() < (m_sliderPosition + m_sliderSize)) {
            m_pressed = true;
            m_pressedPosition = event->y();
        } else {
            setValue((float)event->y()/height() * (m_maximum - m_pageStep));
            emit sliderMoved(m_value);
        }
    } else {
        if (event->x() > m_sliderPosition && event->x() < (m_sliderPosition + m_sliderSize)) {
            m_pressed = true;
            m_pressedPosition = event->x();
        } else {
            setValue((float)event->x()/width() * (m_maximum - m_pageStep));
            emit sliderMoved(m_value);
        }
    }
}

void ScrollBar::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    m_pressed = false;
}

void ScrollBar::mouseMoveEvent(QMouseEvent *event)
{
    if (m_pressed) {
        int delta = 0;

        if (m_orientation == Qt::Vertical) {
            delta = event->y() - m_pressedPosition;
            m_pressedPosition = event->y();

        } else {
            delta = event->x() - m_pressedPosition;
            m_pressedPosition = event->x();
        }

        float nextSliderPosition = m_sliderPosition + delta;
        if (nextSliderPosition > 0 && nextSliderPosition < (lineWidth() - m_sliderSize)) {
            m_sliderPosition = nextSliderPosition;
            m_value = (nextSliderPosition / lineWidth()) * m_maximum;
            repaint();
            emit sliderMoved(m_value);
        }
    }
}

void ScrollBar::wheelEvent(QWheelEvent *event)
{
    if (event->delta() < 0) {
        int value = m_value + m_singleStep * 3;
        if (value > (m_maximum - m_pageStep))
            value = (m_maximum - m_pageStep);
        setValue(value);
        repaint();
        emit sliderMoved(m_value);
    } else if (event->delta() > 0) {
        int value = m_value - m_singleStep * 3;
        if (value < 0)
            value = 0;
        setValue(value);
        repaint();
        emit sliderMoved(m_value);
    }
}

void ScrollBar::resizeEvent(QResizeEvent *event)
{
    updateSliderSize();
}

void ScrollBar::updateSliderSize()
{
    m_sliderSize = ((float)m_pageStep / m_maximum) * lineWidth();
    repaint();
}

int ScrollBar::lineWidth() const
{
    if (m_orientation == Qt::Vertical)
        return height();
    else
        return width();
}

int ScrollBar::lineHeight() const
{
    if (m_orientation == Qt::Vertical)
        return width();
    else
        return height();
}

NS_MA_END
