#ifndef SCROLLAREA_H
#define SCROLLAREA_H

#include "gui_global.h"
#include <core_global.h>

#include <QScrollArea>

NS_MA_BEGIN

class ScrollBar;

class ScrollArea : public QScrollArea
{
    Q_OBJECT
public:
    ScrollArea(QWidget *parent = nullptr);
    ~ScrollArea();

    void setWidget(QWidget *widget);

    void setVerticalScrollBarPolicy(Qt::ScrollBarPolicy policy);
    Qt::ScrollBarPolicy verticalScrollBarPolicy() const;
    ScrollBar *verticalScrollBar() const;

    void setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy policy);
    Qt::ScrollBarPolicy horizontalScrollBarPolicy() const;
    ScrollBar *horizontalScrollBar() const;

protected:
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);
    void resizeEvent(QResizeEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    void updateScrollBars();
    void updateScrollBarsGeometry();

private:
    ScrollBar *m_verticalScrollBar;
    Qt::ScrollBarPolicy m_verticalScrollBarPolicy;

    ScrollBar *m_horizontalScrollBar;
    Qt::ScrollBarPolicy m_horizontalScrollBarPolicy;

};

NS_MA_END

#endif // SCROLLAREA_H
