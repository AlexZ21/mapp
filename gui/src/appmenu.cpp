#include "appmenu.h"
#include "animations.h"
#include "scrollarea.h"
#include "button.h"

#include <log.h>

#include <QEvent>
#include <QVariant>
#include <QPainter>
#include <QVBoxLayout>
#include <QLabel>

USING_MA_NS

AppMenu::AppMenu(QWidget *parent) :
    QWidget(parent),
    m_menuShowed(false),
    m_menuBgOpacity(0),
    m_menuAnimation(new Animation()),
    m_sideMenuWidget(new QWidget(this)),
    m_sideMenuScrollArea(new ScrollArea(m_sideMenuWidget))
{
    if (parent)
        parent->installEventFilter(this);

    m_sideMenuWidget->resize(250, height());
    m_sideMenuWidget->move(-250, 0);
    m_sideMenuWidget->setStyleSheet(".QWidget { background: white; }");

    m_sideMenuWidget->setLayout(new QVBoxLayout());
    m_sideMenuWidget->layout()->setMargin(0);

    initHeader();

    m_sideMenuScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_sideMenuScrollArea->setWidgetResizable(true);

    QWidget *appMenuItemsWidget = new QWidget();
    m_itemsLayout = new QVBoxLayout();
    m_itemsLayout->setSpacing(0);
    m_itemsLayout->setMargin(0);
    appMenuItemsWidget->setLayout(m_itemsLayout);
    m_sideMenuScrollArea->setWidget(appMenuItemsWidget);

    m_sideMenuWidget->layout()->addWidget(m_sideMenuScrollArea);

    setAttribute(Qt::WA_TransparentForMouseEvents, true);
}

AppMenu::~AppMenu()
{
    delete m_menuAnimation;
}

void AppMenu::showMenu()
{
    if (m_menuAnimation->isRunning())
        return;

    setAttribute(Qt::WA_TransparentForMouseEvents, false);

    m_menuAnimation->start(0, 250, 200,
                           [this](float v){
        m_menuBgOpacity = v;
//        m_sideMenuWidget->resize(250 * ((float)v/250), height());
        m_sideMenuWidget->move(-250 + 250 * ((float)v/250), 0);
        repaint();
    }, [this](float v){
        m_menuShowed = !m_menuShowed;
        if (m_menuShowed) {
            setAttribute(Qt::WA_TransparentForMouseEvents, false);
        } else {
            setAttribute(Qt::WA_TransparentForMouseEvents, true);
        }
    }, TransitionFuncs::EaseOutCubic);
}

void AppMenu::hideMenu()
{
    if (m_menuAnimation->isRunning())
        return;

    m_menuAnimation->start(250, 0, 200,
                           [this](float v){
        m_menuBgOpacity = v;
//        m_sideMenuWidget->resize(250 * ((float)v/250), height());
        m_sideMenuWidget->move(-250 + 250 * ((float)v/250), 0);
        repaint();
    }, [this](float v){
        m_menuShowed = !m_menuShowed;
        if (m_menuShowed) {
            setAttribute(Qt::WA_TransparentForMouseEvents, false);
        } else {
            setAttribute(Qt::WA_TransparentForMouseEvents, true);
        }
    }, TransitionFuncs::EaseOutCubic);
}

void AppMenu::addItem(QAction *action)
{
    Button *bt = new Button();
    bt->setAction(action);
    bt->setShape(Button::Rect);
    bt->setAlignment(Button::Left);
    bt->setPadding(20);
    bt->setFixedHeight(40);

    connect(bt, &Button::clicked, this, [this](){
        hideMenu();
    });

    if (m_itemsLayout > 0)
        m_itemsLayout->setStretch(m_itemsLayout->count() - 1, 0);
    m_itemsLayout->addWidget(bt, 1, Qt::AlignTop);

    m_items << bt;
}

void AppMenu::addItem(QWidget *widget)
{

}

void AppMenu::insertItem(int index, QAction *action)
{

}

void AppMenu::insertItem(int index, QWidget *widget)
{

}

void AppMenu::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);

    QBrush bgBrush(QColor(0, 0, 0, 150 * (m_menuBgOpacity/250)));
    p.fillRect(rect(), bgBrush);

}

void AppMenu::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    hideMenu();
}

bool AppMenu::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::Resize) {
        resize(parentWidget()->size());
        m_sideMenuWidget->resize(m_sideMenuWidget->width(), height());
        return QObject::eventFilter(obj, event);
    } else {
        return QObject::eventFilter(obj, event);
    }
}

void AppMenu::initHeader()
{
    QWidget *headerWidget = new QWidget();

}
