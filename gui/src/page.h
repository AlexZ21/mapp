#ifndef PAGE_H
#define PAGE_H

#include "gui_global.h"
#include <core_global.h>

#include <QString>

class QWidget;

NS_MA_BEGIN

class MAGUISHARED_EXPORT Page
{
public:
    Page(const QString &title, const QString &subTitle = QString());
    ~Page();

    virtual QWidget *widget();

    QString title() const;
    QString subTitle() const;

private:
    QString m_title;
    QString m_subTitle;
};

NS_MA_END

#endif // PAGE_H
