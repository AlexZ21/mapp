#include "animations.h"

#include <QDateTime>
#include <QDebug>

USING_MA_NS

Animation::Animation() :
    m_running(false),
    m_repeat(false)
{

}

void Animation::setRepeat(bool repeat)
{
    m_repeat = repeat;
}

void Animation::start(float from, float to, qint64 duration,
                      std::function<void (float)> stepCallback,
                      std::function<void (float)> finishCallback,
                      TransitionFunc trasition)
{
    m_running = true;
    m_duration = duration;
    m_fduration = 0;
    m_repeat = false;
    m_value = AnimationValue(from, to);
    m_stepCallback = stepCallback;
    m_finishCallback = finishCallback;
    m_trasition = trasition;

    Animations::instance().start(this);
}

void Animation::stop()
{
    m_running = false;
    Animations::instance().stop(this);
}

void Animation::finish()
{
    m_running = false;
    m_value.finish();
    m_stepCallback(m_value.current());
    m_finishCallback(m_value.current());
}

void Animation::step(qint64 ms)
{
    m_fduration += ms;
    float dt = m_fduration >= m_duration ? 1. : (m_fduration/(float)m_duration);

    if (dt >= 1) {
        if (m_repeat) {
            m_fduration = 0;
            return;
        }
        finish();
        stop();
    } else {
        m_value.update(dt, m_trasition);
        m_stepCallback(m_value.current());
    }
}

bool Animation::isRunning() const
{
    return m_running;
}

Animations &Animations::instance()
{
    static Animations inst;
    return inst;
}

Animations::Animations() :
    QObject(),
    m_updating(false)
{
    m_timer.setSingleShot(false);
    m_currentMs = QDateTime::currentMSecsSinceEpoch();
    m_lastMs = m_currentMs;
    connect(&m_timer, &QTimer::timeout,
            [this](){

        m_currentMs = QDateTime::currentMSecsSinceEpoch();
        m_updating = true;
        for (Animation *animation : m_animations) {
            animation->step(m_currentMs - m_lastMs/*m_msAfterStart*/);
        }

        m_lastMs = m_currentMs;
        m_updating = false;

        if (!m_sanimations.isEmpty()) {
            for (Animation *animation : m_sanimations)
                m_animations.insert(animation);
            m_sanimations.clear();
        }

        if (!m_fanimations.isEmpty()) {
            for (Animation *animation : m_fanimations)
                m_animations.remove(animation);
            m_fanimations.clear();
        }

        if (m_animations.empty())
            m_timer.stop();

    });
}

void Animations::start(Animation *animation)
{
    if (m_updating) {
        m_sanimations.insert(animation);
        if (!m_fanimations.isEmpty()) {
            m_fanimations.remove(animation);
        }
    } else {
        if (m_animations.isEmpty()) {
            m_currentMs = QDateTime::currentMSecsSinceEpoch();
            m_lastMs = m_currentMs;
            m_timer.start(5);
        }
        m_animations.insert(animation);
    }
}

void Animations::stop(Animation *animation)
{
    if (m_updating) {
        m_fanimations.insert(animation);
        if (!m_sanimations.isEmpty()) {
            m_sanimations.remove(animation);
        }
    } else {
        auto i = m_animations.find(animation);
        if (i != m_animations.cend()) {
            m_animations.erase(i);
            if (m_animations.empty())
                m_timer.stop();
        }
    }
}

