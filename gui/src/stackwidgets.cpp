#include "stackwidgets.h"
#include "animations.h"
#include "style.h"

#include <QPainter>
#include <QDebug>

USING_MA_NS

StackWidgetsOverlay::StackWidgetsOverlay(QWidget *parent) :
    QWidget(parent),
    m_opacity(0)
{
    setAttribute(Qt::WA_TransparentForMouseEvents, true);
    hide();
}

void StackWidgetsOverlay::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    p.fillRect(rect(), QColor(0, 0, 0, m_opacity));

    QLinearGradient shadowGradient(QPoint(width(), 0),
                                   QPoint(width() - 15, 0));
    shadowGradient.setColorAt(0, QColor(0, 0, 0, 70));
    shadowGradient.setColorAt(1, Qt::transparent);
    p.fillRect(QRect(width() - 15, 0, 15, height()), shadowGradient);
}

int StackWidgetsOverlay::opacity() const
{
    return m_opacity;
}

void StackWidgetsOverlay::setOpacity(int opacity)
{
    m_opacity = opacity;
    repaint();
}

StackWidgets::StackWidgets(QWidget *parent) :
    QWidget(parent),
    m_overlay(new StackWidgetsOverlay(this)),
    m_pushAnimation(new Animation()),
    m_popAnimation(new Animation()),
    m_prevTop(nullptr),
    m_top(nullptr)
{

}

StackWidgets::~StackWidgets()
{
    delete m_pushAnimation;
    delete m_popAnimation;
    qDeleteAll(m_widgets);
}

void StackWidgets::push(QWidget *widget)
{
    if (widget == nullptr)
        return;

    if (m_pushAnimation->isRunning()) {
        m_pushAnimation->finish();
        m_pushAnimation->stop();
    }

    if (m_widgets.size() > 0)
        m_prevTop = m_widgets.back();

    m_top = widget;

    m_top->move(width(), 0);
    m_top->setParent(this);
    m_top->resize(size());
    m_top->show();

    m_overlay->move(0, 0);
    m_overlay->setOpacity(0);
    m_overlay->show();
    m_overlay->raise();
    m_overlay->resize(size());

    m_top->raise();

    m_widgets.push_back(m_top);

    m_pushAnimation->start(width(), 0, 300,
                           [this](float v){
        m_top->move(v, 0);
        m_overlay->move(v - width(), 0);
        m_overlay->setOpacity(150 * ((float)(width() - v)/width()));

    }, [this](float v){
        if (m_prevTop)
            m_prevTop->hide();

        m_overlay->hide();

    }, TransitionFuncs::EaseOutCubic);

}

QWidget *StackWidgets::pop(bool del)
{
    if (m_widgets.size() == 0)
        return nullptr;

    if (m_pushAnimation->isRunning()) {
        m_pushAnimation->finish();
        m_pushAnimation->stop();
    }

    if (m_prevTop)
        m_prevTop->show();

    QWidget *top = m_top;

    m_overlay->move(-width(), 0);
    m_overlay->show();

    m_pushAnimation->start(0, width(), 300,
                           [this](float v){
        m_top->move(v, 0);
        m_overlay->move(v - width(), 0);
        m_overlay->setOpacity(150 * ((float)(width() - v)/width()));

    }, [this, del](float v){
        if (del)
            delete m_top;

        m_widgets.removeLast();
        m_overlay->hide();

        if (m_widgets.size() > 1) {
            m_top = m_prevTop;
            m_prevTop = m_widgets.at(m_widgets.size() - 2);
        } else if (m_widgets.size() == 1) {
            m_top = m_prevTop;
            m_prevTop = nullptr;
        } else {
            m_top = nullptr;
            m_prevTop = nullptr;
        }

    }, TransitionFuncs::EaseOutCubic);

    return top;
}

void StackWidgets::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    p.fillRect(rect(), QColor(Style::get("bg_color").toString()));
}

void StackWidgets::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    for (QWidget *widget : m_widgets)
        widget->resize(size());
}


