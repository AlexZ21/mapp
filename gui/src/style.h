#ifndef STYLE_H
#define STYLE_H

#include "gui_global.h"
#include <core_global.h>

#include <QVariant>
#include <QMap>

NS_MA_BEGIN

class Style
{
public:
    static Style &instance();
    static QVariant get(const QString &key);
    static float pr();

private:
    Style();
    ~Style();
    void initDefaultValues();

private:
    QMap<QString, QVariant> m_values;
};

NS_MA_END

#endif // STYLE_H
