#include "progressicon.h"
#include "animations.h"
#include "gui_utils.h"

#include <QPainter>
#include <QIcon>

USING_MA_NS

ProgressIcon::ProgressIcon(const QString &iconPath, QWidget *parent) :
    QWidget(parent),
    m_iconPath(iconPath),
    m_iconSize(16, 16),
    m_rotate(0),
    m_rotatingAnimation(new Animation())
{
    QIcon progressIcon(m_iconPath);
    m_spinnerPixmap = progressIcon.pixmap(m_iconSize);
    if (m_iconColor.isValid())
        setPixmapColor(m_spinnerPixmap, m_iconColor);
}

ProgressIcon::~ProgressIcon()
{
    delete m_rotatingAnimation;
}

void ProgressIcon::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    p.setRenderHint(QPainter::SmoothPixmapTransform);
    p.translate(width() / 2, height() / 2);
    p.rotate(m_rotate);
    p.drawPixmap((0 - m_spinnerPixmap.width()) / 2,
                 (0 - m_spinnerPixmap.height()) / 2, m_spinnerPixmap);
}

QSize ProgressIcon::iconSize() const
{
    return m_iconSize;
}

void ProgressIcon::setIconSize(const QSize &iconSize)
{
    m_iconSize = iconSize;
    QIcon progressIcon(m_iconPath);
    m_spinnerPixmap = progressIcon.pixmap(m_iconSize);
    if (m_iconColor.isValid())
        setPixmapColor(m_spinnerPixmap, m_iconColor);
}

QColor ProgressIcon::iconColor() const
{
    return m_iconColor;
}

void ProgressIcon::setIconColor(const QColor &iconColor)
{
    m_iconColor = iconColor;
}

void ProgressIcon::start()
{
    if (m_rotatingAnimation->isRunning())
        return;
    m_rotatingAnimation->start(0, 360, 2000,
                              [this](float v){
        ++m_rotate;
        repaint();
    }, [this](float v){
    }, TransitionFuncs::Linear);
    m_rotatingAnimation->setRepeat(true);
}

void ProgressIcon::stop()
{
    m_rotatingAnimation->stop();
}

QSize ProgressIcon::sizeHint() const
{
    return QSize(16, 16);
}

QSize ProgressIcon::minimumSizeHint() const
{
    return QSize(16, 16);
}
