#ifndef APPMENU_H
#define APPMENU_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>

class QVBoxLayout;

NS_MA_BEGIN

class Animation;
class ScrollArea;

class AppMenu : public QWidget
{
    Q_OBJECT
public:
    explicit AppMenu(QWidget *parent = nullptr);
    ~AppMenu();

    void showMenu();
    void hideMenu();

    void addItem(QAction *action);
    void addItem(QWidget *widget);
    void insertItem(int index, QAction *action);
    void insertItem(int index, QWidget *widget);
    void removeItem(int index);

protected:
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    bool eventFilter(QObject *obj, QEvent *event);

private:
    void initHeader();

private:
    bool m_menuShowed;
    float m_menuBgOpacity;
    Animation *m_menuAnimation;

    QWidget *m_sideMenuWidget;
    ScrollArea *m_sideMenuScrollArea;

    QList<QWidget *> m_items;
    QVBoxLayout *m_itemsLayout;

};

NS_MA_END

#endif // APPMENU_H
