#include "mainwindow.h"
#include "appmenu.h"
#include "toolbar.h"
#include "stackwidgets.h"
#include "style.h"
#include "button.h"
#include "page.h"
#include "lineedit.h"
#include "audioplayercontrols.h"

#include <log.h>

#include <QVBoxLayout>
#include <QAction>
#include <QLabel>
#include <QPalette>
#include <QDateTime>

USING_MA_NS

class TestPage : public Page
{
public:
    TestPage() : Page(p_title(), p_subTitle()) {

    }

    QWidget *widget() {
        QWidget *w = new QWidget();

        w->setAutoFillBackground(true);

        int rn = 255;//qrand() % 255;
        int gn = 255;//qrand() % 255;
        int bn = 255;//qrand() % 255;

        QPalette bgPalette = w->palette();
        bgPalette.setColor(w->backgroundRole(), QColor(rn, gn, bn));
        w->setPalette(bgPalette);

        LineEdit *testLineEdit = new LineEdit(w);
        testLineEdit->setLabel("First Name:");
        testLineEdit->setBackground(LineEdit::Flat);

        return w;
    }

    QString p_title() {
        qsrand(QDateTime::currentMSecsSinceEpoch());
        int rn = qrand() % 1000;
        return QString::number(rn);
    }

    QString p_subTitle() {
        qsrand(QDateTime::currentMSecsSinceEpoch());
        int rn = qrand() % 1000;
        return QString::number(rn);
    }

};

ToolbarTitle::ToolbarTitle(QWidget *parent) :
    QWidget(parent),
    m_title(new QLabel(this)),
    m_subTitle(new QLabel(this))
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(2);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);

    QFont f = m_title->font();
    f.setBold(true);
    m_title->setFont(f);

    QPalette titlePalette = m_title->palette();
    titlePalette.setColor(m_title->foregroundRole(), QColor(Style::get("text_color").toString()));
    m_title->setPalette(titlePalette);
    m_title->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    m_title->hide();
    mainLayout->addWidget(m_title);

    QPalette subTitlePalette = m_subTitle->palette();
    subTitlePalette.setColor(m_subTitle->foregroundRole(), QColor(Style::get("sub_text_color").toString()));
    m_subTitle->setPalette(subTitlePalette);
    m_subTitle->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    m_subTitle->hide();
    mainLayout->addWidget(m_subTitle);
}

void ToolbarTitle::setText(const QString &title, const QString &subTitle)
{
    m_title->setText(title);
    m_title->setVisible(!title.isEmpty());
    m_subTitle->setText(subTitle);
    m_subTitle->setVisible(!subTitle.isEmpty());
}

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    m_appMenu(new AppMenu(this)),
    m_toolbar(new Toolbar(this)),
    m_backBtn(new Button(this)),
    m_pagesWidgets(new StackWidgets(this)),
    m_searchLineEdit(new LineEdit(this)),
    m_audioPlayerControls(new AudioPlayerControls(this))
{
    init();

    setWindowFlags(Qt::Window);
    resize(640, 480);
}

MainWindow::~MainWindow()
{
    qDeleteAll(m_pages);
}

void MainWindow::pushPage(Page *page)
{
    QWidget *pageWidget = page->widget();

    if (!pageWidget)
        return;

    m_toolbarTitle->setText(page->title(), page->subTitle());
    m_pages << page;
    m_pagesWidgets->push(pageWidget);

    m_backBtn->setVisible(!(m_pages.size() == 1));
}

void MainWindow::popPage()
{
    if (m_pages.size() > 1) {
        m_pagesWidgets->pop();

        Page *prevPage = m_pages.at(m_pages.size() - 2);
        m_toolbarTitle->setText(prevPage->title(), prevPage->subTitle());

        delete m_pages.last();
        m_pages.removeLast();

        m_backBtn->setVisible(!(m_pages.size() == 1));
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    m_audioPlayerControls->setGeometry(QRect(0, height() - Style::get("apc_height").toInt(),
                                             width(), Style::get("apc_height").toInt()));
}

void MainWindow::init()
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);

    initToolbar();
    mainLayout->addWidget(m_toolbar, 0, Qt::AlignTop);

    mainLayout->addWidget(m_pagesWidgets, 1);

    m_audioPlayerControls->setGeometry(QRect(0, height() - Style::get("apc_height").toInt(),
                                             width(), Style::get("apc_height").toInt()));
    m_audioPlayerControls->raise();

    m_appMenu->raise();

    for (int i = 0; i < 5; ++i) {
        QAction *action = new QAction(this);
        action->setIcon(QIcon(":/res/icons/menu.svg"));
        action->setText("Test");
        m_appMenu->addItem(action);
    }
}

#include <core.h>
#include <audio/audioplayer.h>
#include <audio/audiotrack.h>
#include <audio/audioplaylist.h>
#include <QFileDialog>

void MainWindow::initToolbar()
{
    // Menu
    m_showMenuAction = new QAction(this);
    m_showMenuAction->setIcon(QIcon(":/res/icons/menu.svg"));
    connect(m_showMenuAction, &QAction::triggered, [this](){
        m_appMenu->showMenu();
    });
    Button *showMenuBtn = new Button(this);
    showMenuBtn->setAction(m_showMenuAction);
    showMenuBtn->setShape(Button::Circle);
    showMenuBtn->setFixedSize(Style::get("toolbar_controls_height").toInt(),
                              Style::get("toolbar_controls_height").toInt());
    m_toolbar->addWidget(showMenuBtn);

    // Previous page
    m_backAction = new QAction(this);
    m_backAction->setIcon(QIcon(":/res/icons/back.svg"));
    connect(m_backAction, &QAction::triggered, [this](){
        popPage();
    });
    m_backBtn->setAction(m_backAction);
    m_backBtn->setShape(Button::Circle);
    m_backBtn->setFixedSize(Style::get("toolbar_controls_height").toInt(),
                            Style::get("toolbar_controls_height").toInt());
    m_backBtn->hide();
    m_toolbar->addWidget(m_backBtn);

    // Toolbar title
    m_toolbarTitle = new ToolbarTitle(this);
    m_toolbarTitle->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_toolbar->addWidget(m_toolbarTitle);
    m_toolbar->setStrech(2, 1);
    m_toolbarTitle->setText("Test", "Sub test");

    m_searchLineEdit->setLabel("Search");
    m_toolbar->addWidget(m_searchLineEdit);

    Button *addPageBtn = new Button("Open audiofile", this);
    addPageBtn->setFixedHeight(Style::get("toolbar_controls_height").toInt());
    connect(addPageBtn, &Button::clicked, [this](){
        //        AudioPlaylist *pl = new AudioPlaylist();

        //        QString dirPath = QFileDialog::getExistingDirectory(this, "PIDR BLYA");

        //        QDir d(dirPath.isEmpty() ? "/home/alex/Загрузки" : dirPath);
        //        QStringList fil = d.entryList({"*.mp3", "*.flac", "*.ogg"});
        //        for (QString audioFile : fil) {
        //            LDEBUG() << audioFile.toStdString();
        //            AudioTrack *t = new AudioTrack();
        //            t->setSource(d.absolutePath().toStdString() + "/" + audioFile.toStdString());
        //            t->setLoader("local_file");
        //            pl->addAudioTrack(t);
        //        }
        //        Core::instance().audioPlayer()->setpPlaylist(pl);

        AudioPlaylist *pl = new AudioPlaylist();

        AudioTrack *t5 = new AudioTrack();
        t5->setSource("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-13.mp3");
        t5->setLoader("http_file");
        pl->addAudioTrack(t5);



        AudioTrack *t = new AudioTrack();
        t->setSource("https://cs1-35v4.vk-cdn.net/p5/64511b7bc29e57.mp3?extra=POI-ptNGCWffAqQkbelUHYRps9D7d4tuFr83j1DsqzgriIGasSBVVyFQF7QY2FZmFEghRQStMW-JtxjhgqwFN4hR3q0P-Bcovb80yUwBwygJK137waJX4FNr3HSlMTSl9F-gGScvJQ");
        //        t->setSource("http://194.58.97.117:8000/coreradio");
        //        t->setSource("https://cs9-18v4.userapi.com/p7/47eac3d9b26dcb.mp3?extra=VI8c2aJUhyPG3LWl5uT1wVpXQCry85FUXqH5uda3BzFVHaG0P19vfjfsJVGGWPqVknPZcbIYaueF6iODE8ZWG1_dSZJHa6N-cMFbpbFcATQgxVQUxFmwELxQyp1PsxHdHCEgoKpMUw");
        t->setLoader("http_file");
        pl->addAudioTrack(t);


        AudioTrack *t6 = new AudioTrack();
        t6->setSource("https://api.soundcloud.com/tracks/348657558/stream?client_id=nrjnWkkZmPPcl1XxQvg8ssO1w8KTZ3o8");
        t6->setLoader("http_file");
        pl->addAudioTrack(t6);

        AudioTrack *t2 = new AudioTrack();
        t2->setSource("http://194.58.97.117:8000/coreradio");
        t2->setLoader("http_file");
        pl->addAudioTrack(t2);

        //http://uk1.internet-radio.com:8355/

        AudioTrack *t3 = new AudioTrack();
        t3->setSource("http://uk1.internet-radio.com:8355/");
        t3->setLoader("http_file");
        pl->addAudioTrack(t3);

        Core::instance().audioPlayer()->setpPlaylist(pl);
//                Core::instance().audioPlayer()->play(t2);

        //        pushPage(new TestPage());
        //        QString path = QFileDialog::getOpenFileName(this, "Select audiofile", QDir::homePath(), "All (*.*)");
        //        if (!path.isEmpty()) {
        //            AudioTrack *t = new AudioTrack();
        //            t->setSource(path.toStdString());
        //            t->setLoader("local_file");
        //            Core::instance().audioPlayer()->play(t);
        //        }
    });
    m_toolbar->addWidget(addPageBtn);

    Button *addPage2Btn = new Button("Open folder", this);
    addPage2Btn->setFixedHeight(Style::get("toolbar_controls_height").toInt());
    connect(addPage2Btn, &Button::clicked, [this](){

                AudioPlaylist *pl = new AudioPlaylist();

                QString dirPath = QFileDialog::getExistingDirectory(this, "PIDR BLYA");

                QDir d(dirPath.isEmpty() ? "/home/alex/Загрузки" : dirPath);
                QStringList fil = d.entryList({"*.mp3", "*.flac", "*.ogg"});
                for (QString audioFile : fil) {
                    LDEBUG() << audioFile.toStdString();
                    AudioTrack *t = new AudioTrack();
                    t->setSource(d.absolutePath().toStdString() + "/" + audioFile.toStdString());
                    t->setLoader("local_file");
                    pl->addAudioTrack(t);
                }
                Core::instance().audioPlayer()->setpPlaylist(pl);

    });
    m_toolbar->addWidget(addPage2Btn);

    Button *stopBtn = new Button("Stop", this);
    stopBtn->setFixedHeight(Style::get("toolbar_controls_height").toInt());
    connect(stopBtn, &Button::clicked, [this](){
                Core::instance().audioPlayer()->stop();

    });
    m_toolbar->addWidget(stopBtn);
}

