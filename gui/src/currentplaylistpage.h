#ifndef CURRENTPLAYLISTPAGE_H
#define CURRENTPLAYLISTPAGE_H

#include "gui_global.h"
#include "page.h"
#include <core_global.h>

NS_MA_BEGIN

class MAGUISHARED_EXPORT CurrentPlaylistPage : public Page
{
public:
    CurrentPlaylistPage();
    ~CurrentPlaylistPage();

};

NS_MA_END

#endif // CURRENTPLAYLISTPAGE_H
