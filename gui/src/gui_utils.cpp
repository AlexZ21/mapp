#include "gui_utils.h"

#include <QWidget>
#include <QPainter>

QWidget *ma::createHorizontalSpacer(int width)
{
    QWidget *w = new QWidget();
    w->setFixedWidth(width);
    return w;
}

void ma::setImageColor(QImage &img, const QColor &color)
{
    QPainter p(&img);
    p.setCompositionMode(QPainter::CompositionMode_SourceIn);
    p.fillRect(img.rect(), color);
}

void ma::setPixmapColor(QPixmap &img, const QColor &color)
{
    QPainter p(&img);
    p.setCompositionMode(QPainter::CompositionMode_SourceIn);
    p.fillRect(img.rect(), color);
}
