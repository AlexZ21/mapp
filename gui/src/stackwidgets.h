#ifndef STACKWIDGETS_H
#define STACKWIDGETS_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>
#include <QVector>

NS_MA_BEGIN

class Animation;

class MAGUISHARED_EXPORT StackWidgetsOverlay : public QWidget
{
    Q_OBJECT
public:
    explicit StackWidgetsOverlay(QWidget *parent = 0);

    int opacity() const;
    void setOpacity(int opacity);

protected:
    void paintEvent(QPaintEvent *event);

private:
    int m_opacity;

};

class MAGUISHARED_EXPORT StackWidgets : public QWidget
{
    Q_OBJECT
public:
    explicit StackWidgets(QWidget *parent = 0);
    ~StackWidgets();

    void push(QWidget *widget);
    QWidget *pop(bool del = true);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    QVector<QWidget *> m_widgets;

    StackWidgetsOverlay *m_overlay;

    Animation *m_pushAnimation;
    Animation *m_popAnimation;

    QWidget *m_prevTop;
    QWidget *m_top;

};

NS_MA_END

#endif // STACKWIDGETS_H
