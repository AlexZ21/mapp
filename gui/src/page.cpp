#include "page.h"

USING_MA_NS

Page::Page(const QString &title, const QString &subTitle) :
    m_title(title),
    m_subTitle(subTitle)
{

}

Page::~Page()
{

}

QWidget *Page::widget()
{
    return nullptr;
}

QString Page::title() const
{
    return m_title;
}

QString Page::subTitle() const
{
    return m_subTitle;
}
