#ifndef SCROLLBAR_H
#define SCROLLBAR_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>

NS_MA_BEGIN

class Animation;

class ScrollBar : public QWidget
{
    Q_OBJECT
public:
    ScrollBar(Qt::Orientation orientation, QWidget *parent = nullptr);
    ~ScrollBar();

    int value() const;
    void setValue(int value);

    int maximum() const;
    void setMaximum(int maximum);

    int pageStep() const;
    void setPageStep(int pageStep);

    int singleStep() const;
    void setSingleStep(int singleStep);

    int lineWidth() const;
    int lineHeight() const;

    void fadeIn();
    void fadeOut();

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    void updateSliderSize();

signals:
    void sliderMoved(int value);

private:
    int m_value;
    int m_maximum;
    int m_pageStep;
    int m_singleStep;

    Qt::Orientation m_orientation;

    float m_opacity;
    Animation *m_opacityAnimation;

    int m_sliderSize;
    int m_sliderPosition;

    bool m_pressed;
    int m_pressedPosition;

};

NS_MA_END

#endif // SCROLLBAR_H
