#ifndef GUI_GLOBAL_H
#define GUI_GLOBAL_H

#if defined(_MSC_VER)
#  define DECL_EXPORT     __declspec(dllexport)
#  define DECL_IMPORT     __declspec(dllimport)
#elif defined(__GNUC__)
#  if  defined(__WIN32__)
#    define DECL_EXPORT     __declspec(dllexport)
#    define DECL_IMPORT     __declspec(dllimport)
#  elif defined(__linux__)
#    define DECL_EXPORT     __attribute__((visibility("default")))
#    define DECL_IMPORT     __attribute__((visibility("default")))
#    define DECL_HIDDEN     __attribute__((visibility("hidden")))
#  endif
#endif

#if defined(MAGUI_LIBRARY)
#  define MAGUISHARED_EXPORT DECL_EXPORT
#else
#  define MAGUISHARED_EXPORT DECL_IMPORT
#endif

#endif // GUI_GLOBAL_H
