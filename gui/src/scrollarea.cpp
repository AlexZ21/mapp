#include "scrollarea.h"
#include "scrollbar.h"

#include <QScrollBar>
#include <QDebug>

NS_MA_BEGIN

ScrollArea::ScrollArea(QWidget *parent) :
    QScrollArea(parent),
    m_verticalScrollBar(new ScrollBar(Qt::Vertical, this)),
    m_horizontalScrollBar(new ScrollBar(Qt::Horizontal, this))
{
    setFrameStyle(QFrame::NoFrame | QFrame::Plain);
    viewport()->setAutoFillBackground(false);

    QScrollArea::setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QScrollArea::setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    m_verticalScrollBar->setFixedWidth(8);
    m_horizontalScrollBar->setFixedHeight(8);

    connect(m_verticalScrollBar, &ScrollBar::sliderMoved, this,
            [this](int value){
        QScrollArea::verticalScrollBar()->setSliderPosition(value);
    });

    connect(m_horizontalScrollBar, &ScrollBar::sliderMoved, this,
            [this](int value){
        QScrollArea::horizontalScrollBar()->setSliderPosition(value);
    });

    updateScrollBars();
}

ScrollArea::~ScrollArea()
{

}

void ScrollArea::setWidget(QWidget *widget)
{
    QScrollArea::setWidget(widget);
    updateScrollBars();
}

void ScrollArea::setVerticalScrollBarPolicy(Qt::ScrollBarPolicy policy)
{
    QScrollArea::setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_verticalScrollBarPolicy = policy;
}

Qt::ScrollBarPolicy ScrollArea::verticalScrollBarPolicy() const
{
    return m_verticalScrollBarPolicy;
}

ScrollBar *ScrollArea::verticalScrollBar() const
{
    return m_verticalScrollBar;
}

void ScrollArea::setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy policy)
{
    QScrollArea::setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_horizontalScrollBarPolicy = policy;
}

Qt::ScrollBarPolicy ScrollArea::horizontalScrollBarPolicy() const
{
    return m_horizontalScrollBarPolicy;
}

ScrollBar *ScrollArea::horizontalScrollBar() const
{
    return m_horizontalScrollBar;
}

void ScrollArea::enterEvent(QEvent *event)
{
    Q_UNUSED(event);
    if (verticalScrollBarPolicy() == Qt::ScrollBarAlwaysOn)
        m_verticalScrollBar->fadeIn();
    if (horizontalScrollBarPolicy() == Qt::ScrollBarAlwaysOn)
        m_horizontalScrollBar->fadeIn();
}

void ScrollArea::leaveEvent(QEvent *event)
{
    Q_UNUSED(event);
    if (verticalScrollBarPolicy() == Qt::ScrollBarAlwaysOn)
        m_verticalScrollBar->fadeOut();
    if (horizontalScrollBarPolicy() == Qt::ScrollBarAlwaysOn)
        m_horizontalScrollBar->fadeOut();
}

void ScrollArea::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    QScrollArea::resizeEvent(event);
    updateScrollBarsGeometry();
}

void ScrollArea::wheelEvent(QWheelEvent *event)
{
    QScrollArea::wheelEvent(event);
    m_verticalScrollBar->setValue(QScrollArea::verticalScrollBar()->sliderPosition());
    m_horizontalScrollBar->setValue(QScrollArea::horizontalScrollBar()->sliderPosition());
}

void ScrollArea::updateScrollBars()
{
    m_verticalScrollBar->setMaximum(QScrollArea::verticalScrollBar()->maximum());
    m_verticalScrollBar->setPageStep(QScrollArea::verticalScrollBar()->pageStep());
    m_verticalScrollBar->setSingleStep(QScrollArea::verticalScrollBar()->singleStep());

    m_horizontalScrollBar->setMaximum(QScrollArea::horizontalScrollBar()->maximum());
    m_horizontalScrollBar->setPageStep(QScrollArea::horizontalScrollBar()->pageStep());
    m_horizontalScrollBar->setSingleStep(QScrollArea::horizontalScrollBar()->singleStep());

    updateScrollBarsGeometry();
}

void ScrollArea::updateScrollBarsGeometry()
{
    m_verticalScrollBar->setGeometry(width() - m_verticalScrollBar->lineHeight(),
                                     0, m_verticalScrollBar->lineHeight(),
                                     height());

    m_verticalScrollBar->setPageStep(QScrollArea::verticalScrollBar()->pageStep());

    m_horizontalScrollBar->setGeometry(0, height() - m_horizontalScrollBar->lineHeight(),
                                       width() - m_verticalScrollBar->lineHeight(),
                                       m_horizontalScrollBar->lineHeight());

    m_horizontalScrollBar->setPageStep(QScrollArea::horizontalScrollBar()->pageStep());

}

NS_MA_END
