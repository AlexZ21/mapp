#include "imagecontainer.h"
#include <log.h>

#include <QPainter>
#include <QByteArray>

USING_MA_NS

ImageContainer::ImageContainer(QWidget *parent) :
    QWidget(parent),
    m_corderRadius(3)
{
    connect(this, &ImageContainer::finished, this,
            [this](){
        repaint();
    }, Qt::QueuedConnection);

    m_http.setFollowLocation(true);
    m_http.setConnectionTimeout(std::chrono::milliseconds(1000));
    m_http.setStoreBody(true);
    m_http.setOnReady([this](const Http::Response &response){
        if (response.body().size())
            m_img = QPixmap::fromImage(QImage::fromData((const uchar *)response.body().data(), response.body().size()));
        emit finished();
    });
}

void ImageContainer::load(const QUrl &path)
{
    if (path.isLocalFile()) {
        m_img = QPixmap::fromImage(QImage(path.toLocalFile()));
        repaint();
    } else {
        m_http.sendRequestAsync(Url(path.toString().toStdString()),
                                std::chrono::milliseconds(1000));
    }
}

void ImageContainer::fromData(const QByteArray &data)
{
    m_img = QPixmap::fromImage(QImage::fromData(data));
    repaint();
}

void ImageContainer::fromResource(const QString &path)
{
    m_img = QPixmap::fromImage(QImage(path));
    repaint();
}

void ImageContainer::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    QPainterPath bgPath;
    QRect bgRect = rect();
    bgPath.addRoundedRect(bgRect, m_corderRadius, m_corderRadius);
    p.setClipPath(bgPath);

    if (!m_img.isNull()) {
        QPixmap sp = m_img.scaled(rect().size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        int size = qMin(sp.width(), sp.height());
        p.drawPixmap(rect(), sp,
                     QRect((sp.width() - size)/2, (sp.height() - size)/2, size, size));
    }

}

int ImageContainer::corderRadius() const
{
    return m_corderRadius;
}

void ImageContainer::setCorderRadius(int corderRadius)
{
    m_corderRadius = corderRadius;
}
