#include "toolbar.h"
#include "style.h"

#include <QHBoxLayout>
#include <QPainter>

USING_MA_NS

Toolbar::Toolbar(QWidget *parent) :
    QWidget(parent),
    m_mainLayout(new QHBoxLayout())
{
    m_mainLayout->setMargin(Style::get("toolbar_padding").toInt());
    m_mainLayout->setSpacing(Style::get("toolbar_spacing").toInt());
    setLayout(m_mainLayout);

    setFixedHeight(Style::get("toolbar_height").toInt());
}

void Toolbar::addWidget(QWidget *w)
{
    if (!w)
        return;

    if (m_mainLayout->count() > 0 && m_strechedIndexes.isEmpty())
        m_mainLayout->setStretch(m_mainLayout->count() - 1, 0);

    m_mainLayout->addWidget(w, m_strechedIndexes.isEmpty() ? 1 : 0, Qt::AlignLeft);
}

void Toolbar::setStrech(int index, int s)
{
    if (s > 0) {
        if (m_strechedIndexes.contains(index))
            m_strechedIndexes.removeOne(index);
        m_strechedIndexes << index;
        m_mainLayout->setStretch(index, s);
    }

    if (s == 0 && m_strechedIndexes.contains(index)) {
        m_strechedIndexes.removeOne(index);
        m_mainLayout->setStretch(index, 0);
    }

    if (!m_strechedIndexes.isEmpty() && m_mainLayout->count() > 0 && index != (m_mainLayout->count() - 1))
        m_mainLayout->setStretch(m_mainLayout->count() - 1, 0);

    if (m_strechedIndexes.isEmpty() && m_mainLayout->count() > 0 && index != (m_mainLayout->count() - 1))
        m_mainLayout->setStretch(m_mainLayout->count() - 1, 1);
}

void Toolbar::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.fillRect(rect(), QColor(Style::get("bg_color").toString()));
}
