#include "gui.h"
#include "mainwindow.h"

#include <core_utils.h>
#include <core.h>

USING_MA_NS

Gui &Gui::instance()
{
    static Gui gui;
    return gui;
}

void Gui::init()
{
    instance()._init();
}

Gui::Gui() :
    m_window(nullptr)
{

}

Gui::~Gui()
{
    if (m_window)
        delete m_window;
}

void Gui::_init()
{
    if (!m_window)
        m_window = new MainWindow();
    m_window->show();
}


