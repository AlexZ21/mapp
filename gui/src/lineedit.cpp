#include "lineedit.h"
#include "style.h"
#include "animations.h"

#include <QPainter>
#include <QPen>

#include <QDebug>

USING_MA_NS

LineEdit::LineEdit(QWidget *parent) :
    QWidget(parent),
    m_lineEdit(new ILineEdit(this)),
    m_background(Fill),
    m_border(AllSide),
    m_borderOpacity(0),
    m_borderWidth(0),
    m_borderAnimation(new Animation())
{

}

LineEdit::~LineEdit()
{
    delete m_borderAnimation;
}

QSize LineEdit::sizeHint() const
{
    QSize size = m_lineEdit->sizeHint();
    if (m_background == Fill) {
        size.setWidth(size.width() + 4);
        size.setHeight(size.height() + 4);
    } else {
        size.setWidth(size.width() + 4);
        size.setHeight(size.height() + 24);
    }
    return size;
}

QSize LineEdit::minimumSizeHint() const
{
    QSize size = m_lineEdit->minimumSizeHint();
    if (m_background == Fill) {
        size.setWidth(size.width() + 4);
        size.setHeight(size.height() + 4);
    } else {
        int phFontHeight = Style::get("line_edit_placeholder_font_size").toInt();
        size.setWidth(size.width() + 4);
        size.setHeight(size.height() + 24);
    }
    return size;
}

void LineEdit::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    if (m_background == Fill) {
        m_lineEdit->setGeometry(QRect(8, 2, width() - 16, height() - 4));
    } else {
        int phFontHeight = Style::get("line_edit_placeholder_font_size").toInt();
        m_lineEdit->setGeometry(QRect(2, 4 + phFontHeight, width() - 4, height() - 8 - phFontHeight));
    }
}

void LineEdit::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QPainterPath bgPath;
    int bgCornerRadius = Style::get("line_edit_border_radius").toInt();
    QRect bgRect = QRect(2, 2, width() - 4, height() - 4);
    bgPath.addRoundedRect(bgRect, bgCornerRadius, bgCornerRadius);

    if (m_background == Fill) {
        p.fillPath(bgPath, QColor(Style::get("line_edit_bg_color").toString()));

        if (m_lineEdit->text().isEmpty()) {
            QFont phFont = font();
            phFont.setPixelSize(Style::get("line_edit_placeholder_font_size").toInt());
            phFont.setBold(Style::get("line_edit_placeholder_font_bold").toBool());
            phFont.setItalic(Style::get("line_edit_placeholder_font_italic").toBool());
            p.setFont(phFont);

            p.setPen(QPen(Style::get("line_edit_placeholder_font_color").toString()));
            QRect phTextRect(12, 2, width() - 20, height() - 4);
            p.drawText(phTextRect, Qt::AlignLeft | Qt::AlignVCenter, m_label);
        }

        if (m_borderOpacity > 0) {
            QPen borderPen;
            borderPen.setStyle(Qt::SolidLine);
            borderPen.setCapStyle(Qt::RoundCap);
            borderPen.setJoinStyle(Qt::RoundJoin);
            borderPen.setWidth(2);
            QColor borderColor(Style::get("line_edit_hg_color").toString());
            borderColor.setAlpha(m_borderOpacity);
            borderPen.setColor(borderColor);
            p.setPen(borderPen);
            p.drawPath(bgPath);
        }

    } else {
        QPen borderPen;
        borderPen.setStyle(Qt::SolidLine);
        borderPen.setCapStyle(Qt::RoundCap);
        borderPen.setJoinStyle(Qt::RoundJoin);
        borderPen.setWidth(0.1);
        QColor borderColor(Style::get("line_edit_border_color").toString());
        borderPen.setColor(borderColor);
        p.setPen(borderPen);
        p.setRenderHint(QPainter::Antialiasing, false);
        p.drawLine(QPoint(2, height() - 5), QPoint(width() - 4, height() - 5));

        p.setRenderHint(QPainter::Antialiasing);

        QPen borderHPen;
        borderHPen.setStyle(Qt::SolidLine);
        borderHPen.setCapStyle(Qt::RoundCap);
        borderHPen.setJoinStyle(Qt::RoundJoin);
        borderHPen.setWidth(2);
        QColor borderHColor(Style::get("line_edit_hg_color").toString());
        borderHColor.setAlpha(255);
        borderHPen.setColor(borderHColor);
        p.setPen(borderHPen);
        p.drawLine(QPoint(width()/2 - m_borderWidth/2, height() - 4), QPoint(width()/2 + m_borderWidth/2, height() - 4));

        if (m_lineEdit->text().isEmpty()) {
            QFont phFont = font();
            phFont.setPixelSize(Style::get("line_edit_placeholder_font_size").toInt());
            phFont.setBold(Style::get("line_edit_placeholder_font_bold").toBool());
            phFont.setItalic(Style::get("line_edit_placeholder_font_italic").toBool());
            p.setFont(phFont);

            if (m_borderWidth == 0)
                p.setPen(QPen(Style::get("line_edit_placeholder_font_color").toString()));
            else
                p.setPen(borderHPen);

            QFontMetrics fm(phFont);
            QRect phTextRect(2, 2, width() - 4, fm.height());
            p.drawText(phTextRect, Qt::AlignLeft | Qt::AlignVCenter, m_label);
        }


    }
}

void LineEdit::setFocus(bool state)
{
    if (state) {
        if (m_background == Fill) {
            m_borderAnimation->start(0, 255, 100,
                                     [this](float v){
                m_borderOpacity = v;
                repaint();
            }, [this](float v){
            }, TransitionFuncs::EaseOutCubic);
        } else {
            m_borderAnimation->start(0, (width() - 4), 100,
                                     [this](float v){
                m_borderWidth = v;
                repaint();
            }, [this](float v){
            }, TransitionFuncs::EaseOutCubic);
        }
    } else {
        if (m_background == Fill) {
            m_borderAnimation->start(255, 0, 200,
                                     [this](float v){
                m_borderOpacity = v;
                repaint();
            }, [this](float v){
            }, TransitionFuncs::EaseOutCubic);
        } else {
            m_borderAnimation->start((width() - 4), 0, 200,
                                     [this](float v){
                m_borderWidth = v;
                repaint();
            }, [this](float v){
            }, TransitionFuncs::EaseOutCubic);
        }
    }
}

QString LineEdit::label() const
{
    return m_label;
}

void LineEdit::setLabel(const QString &placeHolder)
{
    m_label = placeHolder;
}

LineEdit::Background LineEdit::background() const
{
    return m_background;
}

void LineEdit::setBackground(const Background &background)
{
    m_background = background;
    if (m_background == Flat)
        m_border = OnlyBottom;
    else
        m_border = AllSide;
}

LineEdit::ILineEdit::ILineEdit(LineEdit *parent) :
    QLineEdit(parent)
{
    parentLineEdit = parent;
    setFrame(false);

    QPalette lineEditPalette = palette();
    lineEditPalette.setColor(backgroundRole(), Qt::transparent);
    lineEditPalette.setColor(foregroundRole(), QColor(Style::get("line_edit_font_color").toString()));
    setPalette(lineEditPalette);

    QFont f = font();
    f.setPixelSize(Style::get("line_edit_font_size").toInt());
    f.setBold(Style::get("line_edit_font_bold").toBool());
    f.setItalic(Style::get("line_edit_font_italic").toBool());
    setFont(f);
}

void LineEdit::ILineEdit::focusInEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    parentLineEdit->setFocus(true);
    QLineEdit::focusInEvent(event);
}

void LineEdit::ILineEdit::focusOutEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
    parentLineEdit->setFocus(false);
    QLineEdit::focusOutEvent(event);
}

