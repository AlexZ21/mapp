#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>
#include <QList>

class QAction;
class QLabel;

NS_MA_BEGIN

class AppMenu;
class Toolbar;
class StackWidgets;
class Page;
class Button;
class LineEdit;
class AudioPlayerControls;

class MAGUISHARED_EXPORT ToolbarTitle : public QWidget
{
    Q_OBJECT
public:
    explicit ToolbarTitle(QWidget *parent = 0);

    void setText(const QString &title, const QString &subTitle);

private:
    QLabel *m_title;
    QLabel *m_subTitle;
};

class MAGUISHARED_EXPORT MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void pushPage(Page *page);
    void popPage();

protected:
    void resizeEvent(QResizeEvent *event);

private:
    void init();
    void initToolbar();

private:
    AppMenu *m_appMenu;

    Toolbar *m_toolbar;
    QAction *m_showMenuAction;
    QAction *m_backAction;
    Button *m_backBtn;
    ToolbarTitle *m_toolbarTitle;
    LineEdit *m_searchLineEdit;

    QList<Page *> m_pages;
    StackWidgets *m_pagesWidgets;

    AudioPlayerControls *m_audioPlayerControls;

};

NS_MA_END

#endif // MAINWINDOW_H
