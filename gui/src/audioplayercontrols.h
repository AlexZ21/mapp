#ifndef AUDIOPLAYERCONTROLS_H
#define AUDIOPLAYERCONTROLS_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>
#include <QTimer>

#include <chrono>
#include <functional>

class QLabel;
class QAction;
class QPainter;

NS_MA_BEGIN

class Button;
class ImageContainer;
class Animation;
class ProgressIcon;

class AudioPlayerProgress : public QWidget
{
    Q_OBJECT
public:
    explicit AudioPlayerProgress(QWidget *parent = nullptr);
    ~AudioPlayerProgress();

    std::chrono::milliseconds duration() const;
    void setDuration(const std::chrono::milliseconds &duration);

    std::chrono::milliseconds currentDuration() const;
    void setCurrentDuration(const std::chrono::milliseconds &currentDuration);

    void startDurationUpdate();
    void stopDurationUpdate();

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

private:
    void drawLine(QPainter *p, int width, const QColor &color);
    void drawTimeBlock(QPainter *p, const std::chrono::milliseconds &duration,
                       QPoint point, const std::function<bool (int, QPoint &)> &hideIf = {});
    int durationToWidth(const std::chrono::milliseconds &duration);
    std::chrono::milliseconds widthToDuration(int width);
    void setCurrentDurationHelper(const std::chrono::milliseconds &duration);

signals:
    void sliderMoved(const std::chrono::milliseconds &duration);
    void durationUpdateStarted();
    void durationUpdateStopped();

protected:
    std::chrono::milliseconds m_duration;
    std::chrono::milliseconds m_currentDuration;

    QTimer m_durationUpdate;

    QPixmap m_infPixmap;

    bool m_pressed;
    int m_sliderHgOpacity;

    Animation *m_moveAnimation;
    Animation *m_enteredAnimation;

};

class AudioPlayerControls : public QWidget
{
    Q_OBJECT
public:
    explicit AudioPlayerControls(QWidget *parent = nullptr);
    ~AudioPlayerControls();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void reset();

signals:
    void audioPlayerStatusChanged(int status);
    void audioPlayerStartBuffering();
    void audioPlayerEndBuffering();

private:
    AudioPlayerProgress *m_audioPlayerProgress;

    QAction *m_prevAction;
    QAction *m_playPauseAction;
    QAction *m_nextAction;
    QAction *m_playlistAction;

    ImageContainer *m_coverImg;

    QLabel *m_titleLabel;
    QLabel *m_artistLabel;

    QAction *m_shuffleAction;
    QAction *m_repeatAction;

    ProgressIcon *m_bufferingIcon;

};

NS_MA_END

#endif // AUDIOPLAYERCONTROLS_H
