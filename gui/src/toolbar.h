#ifndef TOOLBAR_H
#define TOOLBAR_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>
#include <QList>

class QHBoxLayout;

NS_MA_BEGIN

class MAGUISHARED_EXPORT Toolbar : public QWidget
{
    Q_OBJECT
public:
    explicit Toolbar(QWidget *parent = nullptr);

    void addWidget(QWidget *w);
    void setStrech(int index, int s);

protected:
    void paintEvent(QPaintEvent *event);

private:
    QHBoxLayout *m_mainLayout;
    QList<int> m_strechedIndexes;

};

NS_MA_END

#endif // TOOLBAR_H
