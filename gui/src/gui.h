#ifndef GUI_H
#define GUI_H

#include "gui_global.h"
#include <core_global.h>

#include <QString>

NS_MA_BEGIN

class MainWindow;

class MAGUISHARED_EXPORT Gui
{
public:
    static Gui &instance();
    static void init();


private:
    Gui();
    ~Gui();

    void _init();

private:
    MainWindow *m_window;

};

NS_MA_END

#endif // GUI_H
