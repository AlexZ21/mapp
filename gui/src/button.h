#ifndef BUTTON_H
#define BUTTON_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>
#include <QIcon>

class QAction;

NS_MA_BEGIN

class Animation;

class Button : public QWidget
{
    Q_OBJECT
public:
    enum Shape {
        RoundRect,
        Circle,
        Rect
    };

    enum Alignment {
        Center,
        Left
    };

    explicit Button(QWidget *parent = 0);
    explicit Button(const QString &text, QWidget *parent = 0);
    ~Button();

    QAction *action() const;
    void setAction(QAction *action);

    bool isCheckable() const;
    void setCheckable(bool checkable);

    bool isChecked() const;

    void setText(const QString &text);
    void setIcon(const QIcon &icon);
    void setIconColor(const QColor &color);

    Alignment alignment() const;
    void setAlignment(const Alignment &alignment);

    Shape shape() const;
    void setShape(const Shape &shape);

    int padding() const;
    void setPadding(int padding);

    QSize sizeHint() const;
    QSize minimumSizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);
    void resizeEvent(QResizeEvent *);

private:
    void init(const QString &text);
    void updateDrawElements();

signals:
    void clicked();

private:
    QAction *m_action;
    Shape m_shape;
    Alignment m_alignment;
    QColor m_iconColor;

    bool m_entered;
    int m_enetredBgOpacity;
    Animation *m_enteredAnimation;

    bool m_pressed;
    QPoint m_pressPoint;
    int m_pressedRadius;
    Animation *m_pressedAnimation;

    QPixmap m_icon;
    QRect m_iconDrawRect;
    QRect m_textDrawRect;
    int m_padding;

};

NS_MA_END

#endif // BUTTON_H
