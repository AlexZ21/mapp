#ifndef PROGRESSICON_H
#define PROGRESSICON_H

#include "gui_global.h"
#include <core_global.h>

#include <QWidget>

NS_MA_BEGIN

class Animation;

class ProgressIcon : public QWidget
{
    Q_OBJECT
public:
    explicit ProgressIcon(const QString &iconPath, QWidget *parent = nullptr);
    ~ProgressIcon();

    QColor iconColor() const;
    void setIconColor(const QColor &iconColor);

    QSize iconSize() const;
    void setIconSize(const QSize &iconSize);

    void start();
    void stop();

    QSize sizeHint() const;
    QSize minimumSizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);

private:
    QString m_iconPath;
    QPixmap m_spinnerPixmap;
    QColor m_iconColor;
    QSize m_iconSize;
    int m_rotate;
    Animation *m_rotatingAnimation;

};

NS_MA_END

#endif // PROGRESSICON_H
