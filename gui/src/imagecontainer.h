#ifndef IMAGECONTAINER_H
#define IMAGECONTAINER_H

#include "gui_global.h"
#include <core_global.h>
#include <network/http.h>

#include <QWidget>
#include <QUrl>
#include <QPixmap>

NS_MA_BEGIN

class ImageContainer : public QWidget
{
    Q_OBJECT
public:
    explicit ImageContainer(QWidget *parent = nullptr);

    void load(const QUrl &path);
    void fromData(const QByteArray &data);
    void fromResource(const QString &path);

    int corderRadius() const;
    void setCorderRadius(int corderRadius);

protected:
    void paintEvent(QPaintEvent *event);

signals:
    void finished();

private:
    QPixmap m_img;
    int m_corderRadius;
    Http m_http;

};

NS_MA_END

#endif // IMAGECONTAINER_H
